﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DistribucionSofia.Utils
{
    public class RemoteAreaAttribute : RemoteAttribute
    {
        public RemoteAreaAttribute(string action, string controller, string area)
            : base(action, controller, area)
        {
            this.RouteData["area"] = area;
        }
    }
}