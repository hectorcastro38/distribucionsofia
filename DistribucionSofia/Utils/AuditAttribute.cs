﻿using DistribucionSofia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DistribucionSofia.Utils
{
    public class AuditAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                // Stores the Request in an Accessible object
                var request = filterContext.HttpContext.Request;
                var pathArray = request.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                bool lastUrlParamIsId = false;
                string id = string.Empty;
                Guid newGuid;
                int newInt;
                string server = request.Url.Host;

                if (Guid.TryParse(pathArray.Last(), out newGuid) || int.TryParse(pathArray.Last(), out newInt))
                {
                    //Ultimo param es id, quitar
                    id = pathArray.Last();
                    pathArray.Remove(pathArray.Last());
                    lastUrlParamIsId = true;
                }

                // Generate an audit
                Audit audit = new Audit()
                {
                    // Your Audit Identifier     
                    Id = Guid.NewGuid(),
                    IdUser = (request.IsAuthenticated) ? (Guid?)Membership.GetUser().ProviderUserKey : (Guid?)null,
                    // Our Username (if available)
                    Username = (request.IsAuthenticated) ? filterContext.HttpContext.User.Identity.Name : "Anonymous",
                    // The IP Address of the Request
                    IPAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.UserHostAddress,
                    // The URL that was accessed
                    URL = request.RawUrl,
                    // Creates our Timestamp
                    Fecha = DateTime.UtcNow,
                    Area = (pathArray.Count == 3) ? pathArray[0] : string.Empty,
                    Controller = (pathArray.Count == 3) ? pathArray[1] : pathArray[0],
                    Action = (pathArray.Count == 3) ? pathArray[2] : (pathArray.Count == 2) ? pathArray[1] : string.Empty,
                    Params = (!lastUrlParamIsId) ? request.QueryString.ToString() : id,
                    Method = request.HttpMethod,
                    Host = server,
                    //ClientComputerName = request.UserHostName
                };

                if (string.IsNullOrEmpty(audit.ClientComputerName))
                    audit.ClientComputerName = DetermineCompName(request.UserHostAddress);

                // Stores the Audit in the Database
                Entities db = new Entities();
                db.Audits.Add(audit);
                db.SaveChanges();
                //db.Dispose();
            }
            catch (Exception err)
            {

            }
            // Finishes executing the Action as normal 
            base.OnActionExecuting(filterContext);
        }

        public static string DetermineCompName(string IP)
        {
            try
            {
                IPAddress myIP = IPAddress.Parse(IP);
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
                return string.Join(".", compName);
                //return compName.FirstOrDefault();
            }
            catch (Exception err)
            {
                return "ERROR";
            }
        }
    }
}