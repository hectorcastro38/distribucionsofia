﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.IO;
using System.Web;
using System.Drawing;
using DistribucionSofia.Models;
using System.Drawing.Drawing2D;
using System.Net;
using System.Data;

namespace DistribucionSofia.Utils
{
    public class FileUploadModel
    {
        public Guid UserId { get; set; }
        //public string FileName { get; set; }
        public string FileData { get; set; }
        public string FileUrl { get; set; }

    }

    public class FileUploadResultModel
    {
        public bool IsFileUploaded { get; set; }
        public Guid ImgId { get; set; }
        public string Error { get; set; }
    }

    public class FileUtils
    {

        //Image to string
        private static string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        //String to image
        public static System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        //Url to image
        public static System.Drawing.Image UrlToImage(string url)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);

            using (HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                using (Stream stream = httpWebReponse.GetResponseStream())
                {
                    return System.Drawing.Image.FromStream(stream);
                }
            }
        }

        //
        public static string GetDir()
        {
            return HttpContext.Current.Server.MapPath("~");
            //return Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
        }

        //Image resize
        private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }
        

        //Upload file
        public static FileUploadResultModel FileUpload(FileUploadModel model, HttpPostedFileBase file, Guid? userId)
        {

            System.Drawing.Image img = null;
            bool isUserImage = false;
            string fullFileName = string.Empty;
            DistribucionSofia.Models.Imagene imgdb;
            string fullFilePath = string.Empty;
            string thumbFilePath = string.Empty;
            var newTempImgId = Guid.NewGuid();

            try
            {
                //Si tiene modelo, utilizar. Si no, como archivo
                
                if (model != null)
                {
                    if (model.FileUrl == null && model.FileData.Length > 0)
                        img = Base64ToImage(model.FileData);

                    if (model.FileData == null && model.FileUrl.Length > 0)
                        img = UrlToImage(model.FileUrl);

                    isUserImage = true;
                    fullFileName = string.Format("{0}.jpg", newTempImgId);
                }
                else
                {
                    img = System.Drawing.Image.FromStream(file.InputStream);
                    fullFileName = string.Format("{0}.{1}", newTempImgId, file.FileName.Split('.').Last());
                }

                //Si no se genero imagen
                if (img.Size.IsEmpty || img == null)
                {
                    return new FileUploadResultModel() { IsFileUploaded = false, Error = "Imagen nula o vacia: FileModel no tiene FileData o FileURL ó archivo no es imagen" };
                }

                fullFilePath = Path.Combine( GetDir() + "\\Content\\img\\Full\\", Path.GetFileName(fullFileName));
                thumbFilePath = Path.Combine(GetDir() + "\\Content\\img\\Thumb\\", Path.GetFileName(fullFileName));

                //string fullFilePath = Path.Combine(HttpContext.Server.MapPath("../Content/Uploads/full/"), Path.GetFileName(fullFileName));
                //string thumbFilePath = Path.Combine(HttpContext.Server.MapPath("../Content/Uploads/thumb/"), Path.GetFileName(fullFileName));

                //if (model != null)
                    img.Save(fullFilePath);
                //else
                //    file.SaveAs(fullFilePath);

                System.Drawing.Image imgThumb = resizeImage(img, new Size(256,256));

                imgThumb.Save(thumbFilePath);

                imgdb = new Imagene()
                {
                    /*ImageURL = "/content/uploads/full/" + fullFileName,
                    ImageURLThumb = "/content/uploads/thumb/" + fullFileName,
                    IsActive = true,
                    IsUserImage = isUserImage,
                    Filename = fullFileName*/
                    fullurl = @"\..\..\Content\img\Full\" + fullFileName,
                    thumburl = @"\..\..\Content\img\Thumb\" + fullFileName,
                    filename = fullFileName,
                    isShared = true,
                    UserID = (model != null) ? model.UserId : userId.Value,
                    Description = fullFileName,
                    imageID = newTempImgId
                };

                Entities db = new Entities();
                db.Imagenes.Add(imgdb);
                db.SaveChanges();

                Guid ImgId = db.Imagenes.First(x => x.fullurl == imgdb.fullurl).imageID;

                return new FileUploadResultModel() { ImgId = ImgId, IsFileUploaded = true };

            }
            catch (Exception err)
            {
               
                //Eliminar fisicamente
                if (File.Exists(fullFilePath))
                    File.Delete(fullFilePath);

                if(File.Exists(thumbFilePath))
                    File.Delete(thumbFilePath);

                return new FileUploadResultModel() { IsFileUploaded = false, Error = err.Message };
            }


        }

        //Download file
        public static Tuple<Guid, bool, string> DownloadFile(DistribucionSofia.Models.Imagene img)
        {
            try
            {
                var fullDownloaded = false;
                var thumbDownloaded = false;

                var fullFilePath = Path.Combine(GetDir() + @"Content\Img\Full\", Path.GetFileName(img.filename));
                var thumbFilePath = Path.Combine(GetDir() + @"Content\Img\Thumb\", Path.GetFileName(img.filename));

                if (!File.Exists(fullFilePath))
                {
                    System.Drawing.Image imgFull = UrlToImage("http://www.sofiaxt.com/content/img/Full/" + img.filename);
                    imgFull.Save(fullFilePath);
                    fullDownloaded = true;
                }

                if (!File.Exists(thumbFilePath))
                {
                    System.Drawing.Image imgThumb = UrlToImage("http://www.sofiaxt.com/content/img/Thumb/" + img.filename);
                    imgThumb.Save(thumbFilePath);
                    thumbDownloaded = true;
                }

                return new Tuple<Guid, bool, string>(img.imageID, true, string.Format("Descargado Full: {0} - Thumb: {1}", fullDownloaded, thumbDownloaded) );
            }
            catch(Exception er)
            {
                return new Tuple<Guid, bool, string>(img.imageID, false, er.Message);
            }
        }
    }
}