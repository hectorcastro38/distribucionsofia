﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DistribucionSofia.Models;
using System.Web.Security;
using System.Diagnostics;

namespace DistribucionSofia.Controllers
{
    public class HomeController : Controller
    {
        private aspnet_Users user = new aspnet_Users();

        public ActionResult Index()
        {
            Entities db = new Entities();
            //var usuario = db.aspnet_Users.Where(u => u.UserName == User.Identity.Name).First();

            if (User.Identity.IsAuthenticated)
            {

                //MembershipUser u = Membership.GetUser(User.Identity.Name, false);
                MembershipUser u = Membership.GetUser();

                if (u == null)
                {
                    Session.Clear();
                    FormsAuthentication.SignOut();
                    return View();
                }

                Guid userid = (Guid)u.ProviderUserKey;
                user = db.aspnet_Users.Find(userid);

                //Verifica contraseña
                if (User.IsInRole("Distribuidor"))
                    return RedirectToAction("Index", "Distribuidor");
                else if (User.IsInRole("Licencia"))
                    return RedirectToAction("Index", "LicenciaManager");
                else if (User.IsInRole("Asesor"))
                    return RedirectToAction("Index", "Asesor");
                
            }

            return View();
        }

        [Authorize(Roles = "Distribuidor, Licencia, Asesor")]
        public JsonResult _GetUser()
        {
            Entities db = new Entities();

            MembershipUser u = Membership.GetUser(User.Identity.Name, false);

            Guid userid = (Guid)u.ProviderUserKey;
            user = db.aspnet_Users.Find(userid);
            

            var id = Guid.Empty;
            if (user.Distribuidor.Id != null)
            {
                id = user.Distribuidor.Id;
            }
            else if (user.Distribuidor.Distribuidor2.Id != null)
            {
                id = user.Distribuidor.Distribuidor2.Id;
            }
            else
            {
                id = user.LicenciasManager.IdLicenciasManager;
            }
            var nameUser = user.UserExtra;

            return Json(new { Name = nameUser.Name, ApellidoP = nameUser.LastNameP, Id = id }, JsonRequestBehavior.AllowGet);
        }
    }
}
