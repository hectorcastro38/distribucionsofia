﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DistribucionSofia.Models;
using System.Web;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net.Mail;
using System.Data.Entity.Validation;
using System.Text;
using System.Globalization;
using PagedList;
using DistribucionSofia.Models.ViewModels;
using System.Web.Routing;

namespace DistribucionSofia.Controllers
{
    public class LicenciaManagerController : Controller
    {
        private Entities db = new Entities();
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {

            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult Index()
        {

            int mes = ((DateTime.Now.Month - 1) == 0 ? 12 : (DateTime.Now.Month - 1));
            int dia = (mes == 2 ? 28 : (DateTime.Now.Day == 31 ? 30 : DateTime.Now.Day));
            int anio = (mes == 12 ? (DateTime.Now.Year - 1) : DateTime.Now.Year);
            DateTime unmes = new DateTime(anio, mes, dia, 0, 0, 0);

            ViewBag.EscuelasExpira = db.Schools.Where(x => x.DistribuidorID != null).SelectMany(y => y.Licencias).Where(r => r.FechaFinal.Value.Month == (DateTime.Now.Month + 1) && r.FechaFinal.Value.Year == DateTime.Now.Year).Select(u => u.IdEscuela).Distinct().Count();
            ViewBag.OrdenUltimoMes = db.OrdenCompras.Where(x => x.IdDistribuidor != null && x.FechaRegistro > unmes && x.FechaRegistro < DateTime.Now && !x.OrdenCompraProductoes.FirstOrDefault().Unidad.Contains("Prueba")).Count();
            ViewBag.OrdenesPrueba = db.OrdenCompras.Where(x => x.IdDistribuidor != null && x.FechaRegistro > unmes && x.FechaRegistro < DateTime.Now && x.OrdenCompraProductoes.FirstOrDefault().Unidad.Contains("Prueba")).Count();
            ViewBag.Aprobadas = db.OrdenCompras.Where(x => x.IdDistribuidor != null && x.Estatus == 1).Count();
            ViewBag.NoAprobadas = db.OrdenCompras.Where(x => x.IdDistribuidor != null && x.Estatus == 0).Count();

            return View();
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult Ordenes()
        {
            var ordenes = db.OrdenCompras.ToList();
            return View();
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult SinAprobar()
        {
            var ordenes = db.OrdenCompras.Where(x => x.IdDistribuidor != null).ToList();
            return View(ordenes);
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult Aprobadas()
        {
            var ordenes = db.OrdenCompras.Where(x => x.IdDistribuidor != null).ToList();
            return View(ordenes);
        }

        public JsonResult _AprobarPrueba(Guid idOrden)
        {
            //lic fechainicio, fechaaprobacion isactivo isaprobado upodate fechafinal
            //ordencompra estatus = 1 --total pagado facturado (si solicito fac)
            //ordencompago new... 
            var orden = db.OrdenCompras.Find(idOrden);
            var lic = orden.Licencia;

            lic.FechaInicio = DateTime.UtcNow;
            lic.FechaAprobacion = DateTime.UtcNow;
            lic.IsActivo = true;
            lic.IsAprobado = true;
            lic.FechaFinal = DateTime.UtcNow.AddMonths(6);

            orden.Estatus = 1;
            orden.TotalPagado = 0;

            try
            {

                db.SaveChanges();

                //enviar correo
                Email.EnviarNotificacionUsuarioAprobada(orden.IdDistribuidor.Value, orden.IdEscuela.Value);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RegistrarPago(Guid idOrden)
        {

            return PartialView();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult ExcelEscuelas()
        {
            ViewBag.Paises = db.Pais.ToList();
            ViewBag.Estados = db.Estadoes.OrderBy(x => x.Codigo).ToList();
            ViewBag.Distribuidores = db.Distribuidors.OrderBy(x => x.Estado1.Codigo).ToList();
            return View();
        }

        [Authorize(Roles = "Licencia")]
        public ActionResult ExcelMaestros()
        {

            return View();
        }

        public ActionResult ExcelAlumnos()
        {

            return View();
        }


        [Authorize(Roles = "Licencia")]
        public JsonResult _ProcesarExcelEscuelas(FormCollection fc, HttpPostedFileBase file, Guid IdDistribuidor, Guid IdPais, Guid IdEstado)
        {
            var listaRetorno = new List<ViewModel_excel_EscuelaNueva>();
            var listaProcesa = new List<ViewModel_excel_EscuelaNueva>();
            var Estado = db.Estadoes.Find(IdEstado);
            var Pais = db.Pais.Find(IdPais);
            var Distribuidor = db.Distribuidors.Find(IdDistribuidor);

            if (!file.ContentType.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            //Guardar achivo en tempfiles
            var tempFilename = Guid.NewGuid().ToString() + ".xlsx";
            string filenamePath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\ImportedFiles\\" + tempFilename;
            file.SaveAs(filenamePath);

            var existingFile = new FileInfo(filenamePath);
            using (var package = new ExcelPackage(existingFile))
            {
                ExcelWorkbook workBook = package.Workbook;
                if (workBook != null)
                {
                    if (workBook.Worksheets.Count > 0)
                    {
                        // Get the first worksheet
                        ExcelWorksheet currentWorksheet = workBook.Worksheets.First();

                        int totalRows = currentWorksheet.Dimension.End.Row;

                        for (int i = 3; i <= totalRows; i++)
                        {

                            var newescuela = new ViewModel_excel_EscuelaNueva();
                            newescuela.Name = (currentWorksheet.Cells[i, 1].Value != null) ? currentWorksheet.Cells[i, 1].Value.ToString() : "";
                            newescuela.ClaveOficial = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 2].Value.ToString() : "";
                            newescuela.Dependencia = (currentWorksheet.Cells[i, 3].Value != null) ? Convert.ToInt32(currentWorksheet.Cells[i, 3].Value.ToString()) : 1;
                            newescuela.Scholar_Level = (currentWorksheet.Cells[i, 4].Value != null) ? Convert.ToInt32(currentWorksheet.Cells[i, 4].Value.ToString()) : 2;
                            newescuela.Address = (currentWorksheet.Cells[i, 5].Value != null) ? currentWorksheet.Cells[i, 5].Value.ToString() : "Escuela";
                            newescuela.Telephone = (currentWorksheet.Cells[i, 6].Value != null) ? currentWorksheet.Cells[i, 6].Value.ToString() : "123456";
                            newescuela.Turno = (currentWorksheet.Cells[i, 7].Value != null) ? Convert.ToInt32(currentWorksheet.Cells[i, 7].Value.ToString()) : 1;
                            newescuela.City = (currentWorksheet.Cells[i, 8].Value != null) ? currentWorksheet.Cells[i, 8].Value.ToString() : "";
                            newescuela.IdEstado = Estado.ID;
                            newescuela.IdPais = Pais.Id;
                            newescuela.EstadoCode = Estado.Codigo;
                            newescuela.DistribuidorID = Distribuidor.Id;
                            newescuela.Cantidad = (currentWorksheet.Cells[i, 9].Value != null) ? Convert.ToInt32(currentWorksheet.Cells[i, 9].Value.ToString()) : 1;
                            newescuela.NombreDir = (currentWorksheet.Cells[i, 10].Value != null) ? currentWorksheet.Cells[i, 10].Value.ToString() : "";
                            newescuela.ApellidoP = (currentWorksheet.Cells[i, 11].Value != null) ? currentWorksheet.Cells[i, 11].Value.ToString() : "";
                            newescuela.ApellidoM = (currentWorksheet.Cells[i, 12].Value != null) ? currentWorksheet.Cells[i, 12].Value.ToString() : "";
                            newescuela.Dia = 1;
                            newescuela.Mes = 10;
                            newescuela.Ano = 1991;
                            newescuela.CorreoDir = (currentWorksheet.Cells[i, 13].Value != null) ? currentWorksheet.Cells[i, 13].Value.ToString() : "";
                            newescuela.Sexo = (currentWorksheet.Cells[i, 14].Value != null) ? Convert.ToInt32(currentWorksheet.Cells[i, 14].Value.ToString()) : 1;
                            newescuela.UsuarioDirector = (currentWorksheet.Cells[i, 15].Value != null) ? currentWorksheet.Cells[i, 15].Value.ToString() : "";
                            newescuela.ContraseñaDirector = (currentWorksheet.Cells[i, 16].Value != null) ? currentWorksheet.Cells[i, 16].Value.ToString() : "";

                            listaProcesa.Add(newescuela);
                        }
                    }
                }
            }
            listaRetorno = ProcesarExcel(db, listaProcesa);

            return Json(listaRetorno.Select(x => new { Numero = x.Conteo, Ciudad = x.City, Name = x.Name, UsuarioDirector = x.UsuarioDirector, ContraseñaDirector = x.ContraseñaDirector }).OrderBy(x => x.Numero).ThenBy(x => x.Name), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Licencia")]
        public JsonResult _ProcesarExcelMaestro(FormCollection fc, HttpPostedFileBase file)
        {
            //Registro masivo drector
            List<ViewModel_excel_Maestros> Maestros = new List<ViewModel_excel_Maestros>();
            List<ViewModel_excel_Maestros> MaestrosRegistrados = new List<ViewModel_excel_Maestros>();


            if (!file.ContentType.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            //Guardar achivo en tempfiles
            var tempFilename = Guid.NewGuid().ToString() + ".xlsx";
            string filenamePath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\ImportedFiles\\" + tempFilename;
            file.SaveAs(filenamePath);
            var existingFile = new FileInfo(filenamePath);
            using (var package = new ExcelPackage(existingFile))
            {
                ExcelWorkbook workBook = package.Workbook;
                if (workBook != null)
                {
                    if (workBook.Worksheets.Count > 0)
                    {
                        // Get the first worksheet
                        ExcelWorksheet currentWorksheet = workBook.Worksheets.First();
                        int totalRows = currentWorksheet.Dimension.End.Row;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            var nombre = (currentWorksheet.Cells[i, 3].Value != null) ? currentWorksheet.Cells[i, 3].Value.ToString() : "";
                            var apellidoP = (currentWorksheet.Cells[i, 1].Value != null) ? currentWorksheet.Cells[i, 1].Value.ToString() : "";
                            var apellidoM = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 2].Value.ToString() : "";
                            var grado = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 4].Value.ToString() : "";
                            var grupo = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 5].Value.ToString() : "";
                            var nombreEscuela = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 6].Value.ToString() : "";
                            var claveoficial = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 7].Value.ToString() : "";

                            if (!string.IsNullOrEmpty(nombre))
                            {

                                Maestros.Add(
                                         new ViewModel_excel_Maestros
                                         {
                                             Nombre = nombre,
                                             ApellidoP = apellidoP,
                                             ApellidoM = apellidoM,
                                             Grado = grado,
                                             Grupo = grupo,
                                             NombreEscuela = nombreEscuela,
                                             ClaveOficial = claveoficial
                                         });


                            }
                        }
                    }
                }
            }



            #region Registro

            foreach (var Maestro in Maestros)
            {
                Models.Student Student = new Models.Student();
                Models.Group Group1 = new Models.Group();
                Guid idlicenciausuario = Guid.Empty;
                Guid teacherid;
                Guid idlicencia = Guid.Empty;
                int cantidadAlumnos = 1;
                int levelID = 0;
                int lvl = Int32.Parse(Maestro.Grado);
                Models.Group groupnew;
                String Teachernip = "";
                var nivel = db.NivelEscolars.FirstOrDefault(x => x.Grado == lvl);
                levelID = nivel.LevelID;

                #region getSchoolID
                string Name = Maestro.NombreEscuela;
                string clave = Maestro.ClaveOficial;
                Models.School SchoolID = db.Schools.Where(x => x.ClaveOficial == clave).FirstOrDefault();
                #endregion


                if (true)
                {
                    IMembershipService MembershipService = new AccountMembershipService();
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    bool uservalido = false;
                    string nuevousername = "";
                    string contrasena = "";
                    var numeros = "0123456789";
                    var random = new Random();
                    var result = new string(
                    Enumerable.Repeat(numeros, 3)
                                         .Select(r => r[random.Next(r.Length)])
                                         .ToArray());

                    #region CleanNeame
                    var n = Maestro.Nombre.Split(' ');
                    var name = "";
                    foreach (var n1 in n)
                    {
                        if (n1 != "")
                        {
                            name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }

                    }
                    var ap = Maestro.ApellidoP.Split(' ');
                    var app = "";
                    foreach (var ap1 in ap)
                    {
                        if (ap1 != "")
                        {
                            app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }
                    }

                    var am = Maestro.ApellidoM.Split(' ');
                    var amm = "";
                    foreach (var am1 in am)
                    {
                        if (am1 != "")
                        {
                            amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }
                    }
                    #endregion


                    var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                    var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                    var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;

                    while (!uservalido)
                    {
                        result = new string(
                        Enumerable.Repeat(numeros, 4)
                                  .Select(r => r[random.Next(r.Length)])
                                  .ToArray());

                        nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                        nuevousername = nuevousername.RemoveDiacritics();
                        if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                        {
                            uservalido = true;
                            contrasena = nuevousername;
                        }

                    }

                    MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                    createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@sofiaxt.com");

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        try
                        {
                            #region Teacher
                            MembershipUser user = Membership.GetUser(nuevousername, false);
                            Guid userid = (Guid)user.ProviderUserKey;
                            aspnet_Users us = db.aspnet_Users.Find(userid);

                            try
                            {
                                UserExtra Teacherextra = new UserExtra();
                                Teacherextra.UserID = userid;
                                Teacherextra.Name = Maestro.Nombre;
                                Teacherextra.LastNameP = Maestro.ApellidoP;
                                Teacherextra.LastNameM = Maestro.ApellidoM;
                                Teacherextra.Password = contrasena;
                                Teacherextra.SexoCode = 1;
                                Teacherextra.Correo = "general@sofiaxt.com";
                                Teacherextra.EmailPapa = "general@sofiaxt.com";
                                Teacherextra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Profesor;
                                Teacherextra.IdEstado = SchoolID.IdEstado;
                                Teacherextra.IdPais = SchoolID.IdPais;

                                db.UserExtras.Add(Teacherextra);
                                result = new string(
                                    Enumerable.Repeat(chars, 8)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                if (true)
                                {
                                    // prefijo = "P";
                                    Teachernip = "P" + result;
                                    if (db.Teachers.Where(x => x.UserId == userid).Count() == 0)
                                    {
                                        Teacher teacher = new Teacher()
                                        {
                                            TeacherID = Guid.NewGuid(),
                                            UserId = userid,
                                            Nip = Teachernip
                                        };
                                        var School = db.Schools.Where(x => x.SchoolID == SchoolID.SchoolID).FirstOrDefault();

                                        teacher.Schools.Add(School);
                                        teacherid = teacher.TeacherID;

                                        db.Teachers.Add(teacher);
                                    }

                                    if (db.SaveChanges() > 0)
                                    {
                                        if (true) Roles.AddUserToRole(nuevousername, "Profesor");

                                        Maestro.Usuario = nuevousername;
                                        Maestro.Contraseña = nuevousername;
                                        FormsAuthentication.SetAuthCookie(us.UserName, true /* createPersistentCookie */);
                                        //Email.EnviarEmailConfirmacion(user, nuevousername, us.UserExtra);

                                        var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == SchoolID.SchoolID).ToList();

                                        var licvigente = licenciasescuelas.Where(x => x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                        if (licvigente != null)
                                        {
                                            LicenciaUsuario newlic = new LicenciaUsuario()
                                            {
                                                Id = Guid.NewGuid(),
                                                IdLicencia = licvigente.Id,
                                                IdUsuario = userid,
                                                FechaRegistro = DateTime.Now,
                                                IsActivo = true,
                                                IsPrueba = licvigente.IsPrueba
                                            };

                                            db.LicenciaUsuarios.Add(newlic);

                                        }
                                        db.SaveChanges();
                                        MaestrosRegistrados.Add(Maestro);
                                        //
                                    }
                                    #endregion
                                    #region Grupo
                                    var errores = new List<string>();
                                    if (db.Teachers.Where(x => x.UserId == userid).Count() != 0)
                                    {
                                        //group.NumPariales = 0;
                                        var Teacher = db.Teachers.Where(x => x.UserId == userid).FirstOrDefault();
                                        try
                                        {
                                            var School = db.Schools.Where(x => x.SchoolID == SchoolID.SchoolID).FirstOrDefault();

                                            groupnew = new Models.Group()
                                            {
                                                GroupID = Guid.NewGuid(),
                                                NivelEscolarID = levelID,
                                                SchoolID = School.SchoolID,
                                                Description = Maestro.Grupo,
                                                Alias = "",
                                                Generacion = DateTime.Now.Year,
                                                Timezone = db.Configuracions.First().Timezone,
                                                TimezoneOffset = db.Configuracions.First().TimezoneOffset,
                                                IsActivo = true,
                                                Activo = true,
                                                IsCerrado = true,
                                                StartDate = DateTime.UtcNow,
                                                TeacherID = Teacher.TeacherID,
                                                NumParciales = 0,
                                                FechaRegistro = DateTime.UtcNow,
                                                ViewList = true,
                                            };

                                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                            var random2 = new Random();
                                            bool nipvalido = false;
                                            String nip2 = "G";
                                            var length = 5;
                                            var loop = 100000;
                                            var cont = 0;
                                            var result2 = new string(
                                                Enumerable.Repeat(chars2, length)
                                                          .Select(s => s[random2.Next(s.Length)])
                                                          .ToArray());
                                            while (!nipvalido)
                                            {
                                                cont++;
                                                if (cont > loop)
                                                {
                                                    result2 = new string(
                                                    Enumerable.Repeat(chars2, length + (cont / loop))
                                                          .Select(s => s[random2.Next(s.Length)])
                                                          .ToArray());
                                                }
                                                nip2 = "G" + result2;
                                                if (db.Groups.Count(x => x.Nip == nip2) == 0)
                                                    nipvalido = true;
                                            }
                                            groupnew.Nip = result2;

                                            foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                            {
                                                groupnew.GroupMaterias.Add(new GroupMateria()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    Materia = materia,
                                                    NumParciales = 0,
                                                    //Validado = 1, ///validacion extra metadata
                                                });

                                            }
                                            foreach (var libro in db.Libroes.Where(x => x.IdNivelEscolar == groupnew.NivelEscolarID))
                                            {
                                                groupnew.Libroes.Add(libro);
                                            }

                                            db.Groups.Add(groupnew);
                                            db.SaveChanges();
                                            Group1 = groupnew;
                                        }
                                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                                        {
                                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                                            {
                                                foreach (var validationError in validationErrors.ValidationErrors)
                                                {
                                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                                }
                                            }
                                        }
                                    }                                  //{



                                    #endregion
                                    #region Alumnos
                                    //    try
                                    //    {
                                    //        var School1 = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();
                                    //        List<Models.Student> Alumnos = new List<Models.Student>();
                                    //        do
                                    //        {
                                    //            //COMO DARLE USUARIO FACIL AL ALUMNO
                                    //            var userAlumno = Teachernip +"AlumnoT" + Alumnos.Count() + 1+ "";
                                    //            var NombreAlumno = "AlumnoT" + Alumnos.Count() + 1 + "";
                                    //             var chars3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //            var random3 = new Random();
                                    //            var numeros = "0123456789";

                                    //            var result3 = new string(
                                    //                Enumerable.Repeat(chars3, 10)
                                    //                          .Select(s => s[random3.Next(s.Length)])
                                    //                          .ToArray());
                                    //            var contrasena = "123456";
                                    //            if (true)
                                    //            {
                                    //                var usuariotemporal = userAlumno;//"usuariotemporal" + result3;
                                    //                MembershipCreateStatus createStatus3 = MembershipService.CreateUser(usuariotemporal, "123456", "prueba@prueba.com");

                                    //                if (createStatus3 == MembershipCreateStatus.Success)
                                    //                {

                                    //                    //FormsService.SignIn(model.UserName, false /* createPersistentCookie */);

                                    //                    MembershipUser user3 = Membership.GetUser(usuariotemporal, false);
                                    //                    Guid userid3 = (Guid)user3.ProviderUserKey;
                                    //                    Roles.AddUserToRole(usuariotemporal, "Alumno");
                                    //                    aspnet_Users us3 = db.aspnet_Users.Find(userid3);
                                    //                    us3.UserExtra = new UserExtra();

                                    //                    var School = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();
                                    //                    us3.UserExtra.Name = NombreAlumno;
                                    //                    us3.UserExtra.Password = contrasena;
                                    //                    us3.UserExtra.SexoCode = 1;
                                    //                    us3.UserExtra.TipoUsuario = (int)EnumTipoUsuario.Alumno;
                                    //                    us3.UserExtra.Ciudad = School.City;
                                    //                    if (Group1.SchoolID.HasValue)
                                    //                    {
                                    //                    us3.UserExtra.Estado = School.EstadoCode;
                                    //                    us3.UserExtra.IdEstado = School.IdEstado;
                                    //                    us3.UserExtra.IdPais = School.IdPais;
                                    //                    //var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == Group1.SchoolID && x.IsAprobado == true).ToList();
                                    //                    //var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == Group1.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                    //                    //if (licvigente != null)
                                    //                    //{
                                    //                    //    LicenciaUsuario newlic = new LicenciaUsuario()
                                    //                    //    {
                                    //                    //        Id = Guid.NewGuid(),
                                    //                    //        IdLicencia = licvigente.Id,
                                    //                    //        IdUsuario = userid,
                                    //                    //        FechaRegistro = DateTime.Now,
                                    //                    //        IsActivo = true,
                                    //                    //        IsPrueba = licvigente.IsPrueba
                                    //                    //    };

                                    //                    //    db.LicenciaUsuarios.Add(newlic);
                                    //                    //    idlicencia = licvigente.Id;
                                    //                    //    idlicenciausuario = newlic.IdUsuario;
                                    //                    //}
                                    //}
                                    //else if (Group1.TeacherID.HasValue)
                                    //{
                                    //    var licenciasescuelas = db.Licencias.Where(x => x.IdProfesor == Group1.TeacherID).ToList();
                                    //    var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == Group1.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                    //    if (licvigente != null)
                                    //    {
                                    //        LicenciaUsuario newlic = new LicenciaUsuario()
                                    //        {
                                    //            Id = Guid.NewGuid(),
                                    //            IdLicencia = licvigente.Id,
                                    //            IdUsuario = userid,
                                    //            FechaRegistro = DateTime.Now,
                                    //            IsActivo = true,
                                    //            IsPrueba = licvigente.IsPrueba
                                    //        };

                                    //        db.LicenciaUsuarios.Add(newlic);
                                    //        idlicencia = licvigente.Id;
                                    //        idlicenciausuario = newlic.IdUsuario;
                                    //    }
                                    //}

                                    //                    result3 = new string(
                                    //                        Enumerable.Repeat(chars3, 8)
                                    //                                  .Select(s => s[random3.Next(s.Length)])
                                    //                                  .ToArray());

                                    //                    String nip3 = "A" + result3;
                                    //                    Student nuevoEstudiante = new Student()
                                    //                    {
                                    //                        StudentID = Guid.NewGuid(),
                                    //                        UserID = userid3,
                                    //                        EjerciciosCorrectosConsecutivos = 0,
                                    //                        EjerciciosCompletados = 0,
                                    //                        EjerciciosCorrectos = 0,
                                    //                        EjerciciosTiempoMenor = 0,
                                    //                        TiempoReal = 0,
                                    //                        TiempoPromedio = 0,
                                    //                        PuntosTotales = 0,
                                    //                        Creditos = 0,
                                    //                        NivelPuntos = 0,
                                    //                        Nip = nip3,
                                    //                        GroupMateriaAlumnoes = new List<GroupMateriaAlumno>(),
                                    //                        IdUserCreado = (Guid)Membership.GetUser().ProviderUserKey,
                                    //                    };

                                    //                    nuevoEstudiante.Groups.Add(Group1);

                                    //                    ////agregar las materias de ese grupo al Alumno
                                    //                    foreach (var materia in Group1.GroupMaterias)
                                    //                    {
                                    //                        var materiaAlumno = new GroupMateriaAlumno()
                                    //                        {
                                    //                            Id = Guid.NewGuid(),
                                    //                            IdGroupMateria = materia.Id,
                                    //                            GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                    //                            IsCerrada = false
                                    //                        };
                                    //                        /////agregrar los parciales de esa materias a la materia del alumno
                                    //                        foreach (var parcialmateria in materia.GroupMateriaParciales)
                                    //                        {
                                    //                            var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                    //                            {
                                    //                                Id = Guid.NewGuid(),
                                    //                                IdGroupMateriaParcial = parcialmateria.Id,
                                    //                                GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()
                                    //                            };
                                    //                            ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                    //                            foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                    //                            {
                                    //                                var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                    //                                {
                                    //                                    Id = Guid.NewGuid(),
                                    //                                    Porcentaje = evaluacionparcialmateria.Porcentaje,
                                    //                                    IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                    //                                    GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                    //                                };
                                    //                                ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                    //                                ///a la evaluacion del parcial de la materia al alumno
                                    //                                foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                    //                                {
                                    //                                    var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                    //                                    {
                                    //                                        Id = Guid.NewGuid(),
                                    //                                        IsCheckBox = true,
                                    //                                        Cumplio = true,
                                    //                                        IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                    //                                    };
                                    //                                    evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);
                                    //                                }
                                    //                                parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                    //                            }
                                    //                            materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                    //                        }
                                    //                        nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                    //                    }

                                    //                    Alumnos.Add(nuevoEstudiante);
                                    //                    db.Students.Add(nuevoEstudiante);
                                    //                    db.SaveChanges();
                                    //                }

                                    //            }
                                    //        } while (Alumnos.Count < cantidadAlumnos);
                                    //    }
                                    //    catch (Exception e)
                                    //    {
                                    //        string ex = e.Message; 

                                    //    }
                                    #endregion
                                }
                                else
                                {
                                    Models.Error er = new Models.Error()
                                    {
                                        Id = Guid.NewGuid(),
                                        Descripcion = "No se guardo UserAndExtra, ni Student, ni Teacher ni Tutor",
                                        // Detalle = model.TipoUsuario + ,
                                    };
                                    db.Errors.Add(er);
                                    db.SaveChanges();
                                }

                                #region coment
                                //if (model.TipoUsuario == Models.Enum.EnumTipoUsuario.Tutor)
                                //{
                                //    prefijo = "T";
                                //    Tutor tutor = new Tutor()
                                //    {
                                //        TutorID = Guid.NewGuid(),
                                //        UserID = userid
                                //    };
                                //    db.Tutors.Add(tutor);

                                //}
                                #endregion
                            }

                            catch (SmtpException smtpe)
                            {
                                Models.Error e = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    // Detalle = model.Usuario,
                                    Descripcion = "Envio de correos"
                                };
                                return Json(false, JsonRequestBehavior.AllowGet);
                            }
                            catch (DbEntityValidationException dbEx)
                            {
                                var err = "";
                                foreach (var validationErrors in dbEx.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        err += "Property: " + validationError.PropertyName + " - Error: " + validationError.ErrorMessage + ". ";
                                    }
                                }
                                Models.Error er = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    Descripcion = err,
                                    // Detalle = model.Usuario,
                                };
                                db.Errors.Add(er);
                                db.SaveChanges();

                            }
                            catch (Exception e)
                            {
                                var err = e.Message;
                                Models.Error er = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    Descripcion = e.InnerException.Message,
                                    //Detalle = model.Usuario,
                                };
                                db.Errors.Add(er);
                                db.SaveChanges();
                            }
                        }

                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                }
                            }
                        }
                    }

                }



            }
            #endregion


            #region DescargaAltas


            //var fi = new FileInfo(@"C:\ListasMaestrosT\ListaProfesores.xlsx");
            //using (var pi = new ExcelPackage(fi))
            //{
            //    var ws = pi.Workbook.Worksheets["Lista"];
            //    //////.Cells[2, 10] = “ fila10 columna 2”;
            //    //////Escuela
            //    //ws.Cells[4, 4].Value = (s != null) ? s.Name : "";
            //    ////Fecha 
            //    //ws.Cells[3, 4].Value = System.DateTime.Now.ToShortDateString();
            //    //ws.Cells[3, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            //    int i = 10;
            //    foreach (var teacher in MaestrosRegistrados)
            //    {


            //        ////Nombre Profesor
            //        ws.Cells[i, 2].Value = teacher.Nombre;
            //        //FormatoCelda
            //        ws.Cells[i, 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            //        //Usuario Profesor
            //        ws.Cells[i, 3].Value = teacher.Usuario;
            //        //FormatoCelda
            //        ws.Cells[i, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 3].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //        //Contraseña Profesor
            //        ws.Cells[i, 4].Value = teacher.Contraseña;
            //        //FormatoCelda
            //        ws.Cells[i, 4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 4].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            //        //Grupos  Profesor
            //        ws.Cells[i, 5].Value = teacher.Grupo + " " + teacher.Grado + " ";
            //        ws.Cells[i, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //        //FormatoCelda
            //        ws.Cells[i, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 5].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        ws.Cells[i, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        i++;
            //    }



            //    Response.ClearContent();
            //    pi.Save();
            //    Response.Buffer = true;
            //    Response.AddHeader("content-disposition", "attachment; filename=  Maestros .xlsx");
            //    Response.ContentType = "application/ms-excel";

            //    Response.Flush();
            //    Response.End();
            //}
            #endregion



            return Json(MaestrosRegistrados, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Licencia")]
        public JsonResult  ProcesarAlumnosNuevosTamaulipasPrimaria(string municipio)
        {
            // return Json(true, JsonRequestBehavior.AllowGet);
            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities();
                List<Student> estudiantes = new List<Student>();
                IMembershipService MembershipService = new AccountMembershipService();
                var nuevos = db.xxxAltaMasivaAlunmos.Where(x => !x.is_procesado&&x.MUNICIPIO==municipio);// && x.GRADO==1 && x.GRUPO=="A").Take(30);
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                var numeros = "0123456789";
                //var colores = new string[] { "azul", "verde", "rojo", "amarillo" };
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 4)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());

                var numero = nuevos.Count();

                #region foreach Estudiantes

                foreach (var model in nuevos.ToList())
                {
                    bool uservalido = false;
                    model.NOMBRE = RemoveDiacritics(model.NOMBRE);
                    if (!string.IsNullOrEmpty(model.PRIMERAPELLIDO))
                    {
                        model.PRIMERAPELLIDO = RemoveDiacritics(model.PRIMERAPELLIDO);
                    } else
                    {
                        model.PRIMERAPELLIDO = "";
                    }


                    if (!string.IsNullOrEmpty(model.SEGUNDOAPELLIDO))
                    {
                        model.SEGUNDOAPELLIDO = RemoveDiacritics(model.SEGUNDOAPELLIDO);
                    }
                    else
                    {
                        model.SEGUNDOAPELLIDO = "";
                    }



                    var CCT = model.CCT.TrimEnd();
                    Guid idlicencia = Guid.Empty;
                    Guid idlicenciausuario = Guid.Empty;
                    // var nipescuela = model.Nip.TrimEnd();
                    var escuela = db.Schools.FirstOrDefault(x => x.ClaveOficial == CCT);


                    if (!string.IsNullOrEmpty(model.NOMBRE))
                    {
                        if (escuela == null)
                        {
                            Mensajes.Add("Escuela no registrada, registre la escuela: " + model.NOMBRE_CT + " para poder continuar");
                            model.is_procesado = false;
                            db.SaveChanges();

                        }
                        else
                        {
                            var gruponum = model.GRADO;
                            var des = model.GRUPO;
                            var grupo = db.Groups.FirstOrDefault(x => x.SchoolID == escuela.SchoolID
                                && x.IsActivo == true
                                && x.NivelEscolar.Grado == gruponum
                                && x.Description == des);

                            #region Crear grupo
                            //Crear grupo si no existe
                            if (grupo == null)
                            {

                                var enumnip = new string(
                                    Enumerable.Repeat(chars, 5)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                String nip = "G" + enumnip;

                                grupo = new DistribucionSofia.Models.Group()
                                {
                                    GroupID = Guid.NewGuid(),
                                    SchoolID = escuela.SchoolID,
                                    NivelEscolarID = db.NivelEscolars.First(x => x.Grado == gruponum).LevelID,
                                    //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                    Description = model.GRUPO,
                                    IsActivo = true,
                                    Generacion = 2019,
                                    StartDate = DateTime.UtcNow,
                                    Nip = nip,
                                    NumParciales = 0,
                                    ViewList=true,
                                    Activo = true,
                                    IsCerrado = false,
                                    IsOficial = true
                                };
                                grupo.Timezone = db.Configuracions.First().Timezone;
                                grupo.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                                if (grupo.SchoolID != null)
                                {
                                    grupo.Timezone = escuela.Timezone;
                                    grupo.TimezoneOffset = escuela.TimezoneOffset;
                                }
                                //Materias
                                foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                {
                                    grupo.GroupMaterias.Add(new GroupMateria()
                                    {
                                        Id = Guid.NewGuid(),
                                        Materia = materia,
                                        NumParciales = 0,
                                        //Validado = 1, ///validacion extra metadata
                                    });

                                }
                                db.Groups.Add(grupo);
                                db.SaveChanges();
                                Mensajes.Add("Se agrego el grupo: " + gruponum + model.GRUPO);
                            }
                            #endregion

                            #region Crear alumno
                            //var usuario = "";
                            //var nuevousername = "";
                            #region CleanNeame
                            var n = model.NOMBRE.Split(' '); //nombre.First.Split(' ');
                            var name = "";
                            foreach (var n1 in n)
                            {
                                if (n1 != "")
                                {
                                    name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }

                            }
                            var ap = model.PRIMERAPELLIDO.Split(' ');// nombre.Second.Split(' ');
                            var app = "";
                            foreach (var ap1 in ap)
                            {
                                if (ap1 != "")
                                {
                                    app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }

                            var am = model.SEGUNDOAPELLIDO.Split(' ');// nombre.Third.Split(' ');
                            var amm = "";
                            foreach (var am1 in am)
                            {
                                if (am1 != "")
                                {
                                    amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }
                            #endregion

                            var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                            var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                            var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;
                            var nuevousername = string.Empty;
                            var contrasena = string.Empty;

                            while (!uservalido)
                            {
                                result = new string(
                                Enumerable.Repeat(numeros, 4)
                                          .Select(r => r[random.Next(r.Length)])
                                          .ToArray());

                                nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                                nuevousername = nuevousername.RemoveDiacritics();
                                if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                                {
                                    uservalido = true;
                                    contrasena = nuevousername;
                                }

                            }

                            nuevousername = nuevousername.ToLower();

                            MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                            createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@general.com");
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    //FormsService.SignIn(model.UserName, false /* createPersistentCookie */);

                                    MembershipUser user = Membership.GetUser(nuevousername, false);
                                    Guid userid = (Guid)user.ProviderUserKey;

                                    Roles.AddUserToRole(nuevousername, "Alumno");

                                    //aspnet_Users us = db.aspnet_Users.Find(userid);
                                    var us = db.aspnet_Users.SqlQuery("select * from aspnet_Users where UserId = '" + userid + "'").First();

                                    us.UserExtra = new UserExtra();
                                    var tu = 3;
                                    us.UserExtra.TipoUsuario = tu;
                                    us.UserExtra.IdEstado = escuela.IdEstado;
                                    us.UserExtra.IdPais = escuela.IdPais;
                                    us.UserExtra.Ciudad = escuela.City;
                                    us.UserExtra.Name = model.NOMBRE.ToUpper().Trim();
                                    us.UserExtra.LastNameP = model.PRIMERAPELLIDO;
                                    us.UserExtra.LastNameM = model.SEGUNDOAPELLIDO;
                                    us.UserExtra.Password = contrasena.ToLower();
                                    us.UserExtra.SexoCode = 1;
                                    us.UserExtra.LevelID = grupo.NivelEscolarID;



                                    escuela.NumAlum = escuela.NumAlum + 1;
                                    //var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == grupo.SchoolID && x.IsAprobado == true).ToList();
                                    //var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == grupo.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                    //if (licvigente != null)
                                    //{
                                    //    LicenciaUsuario newlic = new LicenciaUsuario()
                                    //    {
                                    //        Id = Guid.NewGuid(),
                                    //        IdLicencia = licvigente.Id,
                                    //        IdUsuario = userid,
                                    //        FechaRegistro = DateTime.Now,
                                    //        IsActivo = true,
                                    //        IsPrueba = licvigente.IsPrueba
                                    //    };

                                    //    db.LicenciaUsuarios.Add(newlic);
                                    //    idlicencia = licvigente.Id;
                                    //    idlicenciausuario = newlic.IdUsuario;

                                    //    //log
                                    //    if (idlicencia != Guid.Empty)
                                    //    {
                                    //        LogLicencia newlog = new LogLicencia()
                                    //        {
                                    //            Id = Guid.NewGuid(),
                                    //            IdLicencia = idlicencia,
                                    //            IdUsuarioRegistro = idlicenciausuario,
                                    //            FechaRegistro = DateTime.Now,
                                    //            TipoDestino = 0,
                                    //            TipoRegistro = 1,
                                    //            Comentario = "Se agrego el alumno por Excel y/o Registro solo"
                                    //        };
                                    //        db.LogLicencias.Add(newlog);
                                    //        //db.SaveChanges();
                                    //    }
                                    //}



                                    result = new string(
                                        Enumerable.Repeat(chars, 8)
                                                    .Select(s => s[random.Next(s.Length)])
                                                    .ToArray());

                                    String nip = "A" + result;

                                    Student nuevoEstudiante = new Student()
                                    {
                                        StudentID = Guid.NewGuid(),
                                        UserID = userid,
                                        EjerciciosCorrectosConsecutivos = 0,
                                        EjerciciosCompletados = 0,
                                        EjerciciosCorrectos = 0,
                                        EjerciciosTiempoMenor = 0,
                                        TiempoReal = 0,
                                        TiempoPromedio = 0,
                                        PuntosTotales = 0,
                                        Creditos = 0,
                                        NivelPuntos = 0,
                                        Nip = nip,
                                        GroupMateriaAlumnoes = new List<GroupMateriaAlumno>(),
                                        IdUserCreado = (Guid)Membership.GetUser().ProviderUserKey,
                                    };

                                    //Group grupo = db.Groups.Find(model.GroupID);
                                    nuevoEstudiante.Groups.Add(grupo);


                                    #region Agregar Materias
                                    //////agregar las materias de ese grupo al Alumno
                                    //foreach (var materia in grupo.GroupMaterias)
                                    //{
                                    //    var materiaAlumno = new GroupMateriaAlumno()
                                    //    {
                                    //        Id = Guid.NewGuid(),
                                    //        IdGroupMateria = materia.Id,
                                    //        GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                    //        IsCerrada = false
                                    //    };


                                    //    #region Agregar Parciales
                                    //    /////agregrar los parciales de esa materias a la materia del alumno
                                    //    foreach (var parcialmateria in materia.GroupMateriaParciales)
                                    //    {
                                    //        var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                    //        {
                                    //            Id = Guid.NewGuid(),
                                    //            IdGroupMateriaParcial = parcialmateria.Id,
                                    //            GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()

                                    //        };

                                    //        ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                    //        foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                    //        {
                                    //            var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                    //            {
                                    //                Id = Guid.NewGuid(),
                                    //                Porcentaje = evaluacionparcialmateria.Porcentaje,
                                    //                IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                    //                GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                    //            };
                                    //            ///has evaluaciones?
                                    //            ///
                                    //            ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                    //            ///a la evaluacion del parcial de la materia al alumno
                                    //            foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                    //            {
                                    //                var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                    //                {
                                    //                    Id = Guid.NewGuid(),
                                    //                    IsCheckBox = true,
                                    //                    Cumplio = true,
                                    //                    IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                    //                };
                                    //                evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);

                                    //            }

                                    //            parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                    //        }

                                    //        materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                    //    }
                                    //    #endregion

                                    //    nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                    //}
                                    #endregion

                                    db.Students.Add(nuevoEstudiante);
                                    db.SaveChanges();

                                    ///
                                    var usuarioMembership = Membership.GetUser(nuevousername);

                                    //if (usuarioMembership.ChangePassword(usuarioMembership.ResetPassword(), "123456"))
                                    //{
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = nuevousername.ToLower();
                                    //   db.SaveChanges();


                                    model.is_procesado = true;
                                    model.NipAlumno = nuevoEstudiante.Nip;
                                    model.NipGrupo = grupo.Nip;
                                    model.UsuarioAlumno = nuevousername.ToLower();
                                    model.ContraseñaAlumno = nuevousername.ToLower();
                                    db.SaveChanges();

                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            Mensajes.Add("Error al crear usuario: " + model.NOMBRE);
                                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        }
                                    }
                                    //return View(model);
                                }


                            }
                            #endregion

                        }
                    }
                    else
                    {
                        model.is_procesado = false;
                        db.SaveChanges();
                    }

                } //endforeach

                var registrados = nuevos.Select(x => x.is_procesado == true);
                var total = registrados.Count();
                return Json(total, JsonRequestBehavior.AllowGet);
            }

            #endregion


            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Licencia")]
        public JsonResult ProcesarAlumnosNuevosTamaulipasSecundaria(string municipio)
        {

            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities(); 
                List<Student> estudiantes = new List<Student>();
                IMembershipService MembershipService = new AccountMembershipService();
                //  var NR = db.xxxAlumnosTamaulipasSecNR_.Where(x=> !x.is_procesado.Value).Select(x=> x.Id);
                var nuevos = db.xxxAlumnosTamaulipasSecundaria_.Where(x => !x.is_procesado &&x.Id>= 98389);
                //  List<xxxAlumnosTamaulipasSecNR_> NoRegistrado = new List<xxxAlumnosTamaulipasSecNR_>();
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var numeros = "0123456789";
                //var colores = new string[] { "azul", "verde", "rojo", "amarillo" };
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 4)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());


                var numero = nuevos.Count();
                #region foreach Estudiantes

                foreach (var model in nuevos.ToList())
                {
                    bool uservalido = false;
                    model.NOMBRE = RemoveDiacritics(model.NOMBRE);
                    if (!string.IsNullOrEmpty(model.PRIMERAPELLIDO))
                    {
                        model.PRIMERAPELLIDO = RemoveDiacritics(model.PRIMERAPELLIDO);
                    }
                    else
                    {
                        model.PRIMERAPELLIDO = "";
                    }


                    if (!string.IsNullOrEmpty(model.SEGUNDOAPELLIDO))
                    {
                        model.SEGUNDOAPELLIDO = RemoveDiacritics(model.SEGUNDOAPELLIDO);
                    }
                    else
                    {
                        model.SEGUNDOAPELLIDO = "";
                    }
                    var CCT = model.CCT.TrimEnd();
                    Guid idlicencia = Guid.Empty;
                    Guid idlicenciausuario = Guid.Empty;
                    // var nipescuela = model.Nip.TrimEnd();
                    var escuela = db.Schools.FirstOrDefault(x => x.ClaveOficial == CCT);


                    if (!string.IsNullOrEmpty(model.NOMBRE) || !string.IsNullOrWhiteSpace(model.NOMBRE))
                    {
                        if (escuela == null)
                        {
                            Mensajes.Add("Escuela no registrada, registre la escuela: " + model.NOMBRE_CT + " para poder continuar");
                            model.is_procesado = false;
                            db.SaveChanges();
                        }
                        else
                        {

                            #region Grado
                            var gruponum = 0;
                            switch (model.GRADO)
                            {
                                case 1:
                                    gruponum = 7;
                                    break;
                                case 2:
                                    gruponum = 8;
                                    break;
                                case 3:
                                    gruponum = 9;
                                    break;


                                default:
                                    gruponum = 0;
                                    break;
                            }




                            //var gruponum = (model.GRADO == 1) ? "positive" : "negative";

                            //model.GRADO;
                            #endregion

                            var des = model.GRUPO;
                            var turno = model.TURNO;

                            var grupo = db.Groups.FirstOrDefault(x => x.SchoolID == escuela.SchoolID
                                && x.IsActivo == true
                                && x.NivelEscolar.Grado == gruponum
                                && x.Description == des
                                && x.Alias == turno.TrimEnd());

                            var Userext = db.UserExtras.FirstOrDefault(x => x.Name == model.NOMBRE
                                && x.LastNameP == model.PRIMERAPELLIDO
                                && x.LastNameM == model.SEGUNDOAPELLIDO
                                );

                            #region Crear grupo
                            //Crear grupo si no existe
                            if (grupo == null)
                            {

                                var enumnip = new string(
                                    Enumerable.Repeat(chars, 5)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                String nip = "G" + enumnip;

                                grupo = new DistribucionSofia.Models.Group()
                                {
                                    GroupID = Guid.NewGuid(),
                                    SchoolID = escuela.SchoolID,
                                    NivelEscolarID = db.NivelEscolars.First(x => x.Grado == gruponum).LevelID,
                                    //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                    Description = model.GRUPO,
                                    Alias= turno.TrimEnd(),
                                    IsActivo = true,
                                    Generacion = 2019,
                                    StartDate = DateTime.UtcNow,
                                    Nip = nip,
                                    NumParciales = 0,
                                    ViewList=true,
                                    Activo = true,
                                    IsCerrado = false,
                                    IsOficial = true
                                };
                                grupo.Timezone = db.Configuracions.First().Timezone;
                                grupo.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                                if (grupo.SchoolID != null)
                                {
                                    grupo.Timezone = escuela.Timezone;
                                    grupo.TimezoneOffset = escuela.TimezoneOffset;
                                }
                                //Materias
                                foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                {
                                    grupo.GroupMaterias.Add(new GroupMateria()
                                    {
                                        Id = Guid.NewGuid(),
                                        Materia = materia,
                                        NumParciales = 0,
                                        //Validado = 1, ///validacion extra metadata
                                    });

                                }
                                db.Groups.Add(grupo);
                                db.SaveChanges();
                                Mensajes.Add("Se agrego el grupo: " + gruponum + model.GRUPO);
                            }
                            #endregion

                            #region Crear alumno
                            //var usuario = "";
                            //var nuevousername = "";
                            #region CleanNeame
                            var n = model.NOMBRE.Split(' '); //nombre.First.Split(' ');
                            var name = "";
                            foreach (var n1 in n)
                            {
                                if (n1 != "")
                                {
                                    name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }

                            }
                            var ap = model.PRIMERAPELLIDO.Split(' ');// nombre.Second.Split(' ');
                            var app = "";
                            foreach (var ap1 in ap)
                            {
                                if (ap1 != "")
                                {
                                    app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }

                            var am = model.SEGUNDOAPELLIDO.Split(' ');// nombre.Third.Split(' ');
                            var amm = "";
                            foreach (var am1 in am)
                            {
                                if (am1 != "")
                                {
                                    amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }
                            #endregion

                            var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                            var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                            var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;
                            var nuevousername = string.Empty;
                            var contrasena = string.Empty;

                            while (!uservalido)
                            {
                                result = new string(
                                Enumerable.Repeat(numeros, 3)
                                          .Select(r => r[random.Next(r.Length)])
                                          .ToArray());

                                nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                                nuevousername = nuevousername.RemoveDiacritics();
                                if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                                {
                                    uservalido = true;
                                    contrasena = nuevousername;
                                }

                            }


                            if (grupo != null)
                            {
                                nuevousername = nuevousername.ToLower();
                                MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;


                                createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@general.com");
                                if (createStatus == MembershipCreateStatus.Success)
                                {
                                    try
                                    { //FormsService.SignIn(model.UserName, false /* createPersistentCookie */);

                                        MembershipUser user = Membership.GetUser(nuevousername, false);
                                        Guid userid = (Guid)user.ProviderUserKey;

                                        Roles.AddUserToRole(nuevousername, "Alumno");

                                        //   aspnet_Users us = db.aspnet_Users.Find(userid);
                                        var us = db.aspnet_Users.SqlQuery("select * from aspnet_Users where UserId = '" + userid + "'").First();
                                        us.UserExtra = new UserExtra();
                                        var tu = 3;
                                        us.UserExtra.TipoUsuario = tu;
                                        us.UserExtra.IdEstado = escuela.IdEstado;
                                        us.UserExtra.IdPais = escuela.IdPais;
                                        us.UserExtra.Ciudad = escuela.City;
                                        us.UserExtra.Name = model.NOMBRE.ToUpper().Trim();
                                        us.UserExtra.LastNameP = model.PRIMERAPELLIDO;
                                        us.UserExtra.LastNameM = model.SEGUNDOAPELLIDO;
                                        us.UserExtra.Password = contrasena.ToLower();
                                        us.UserExtra.SexoCode = 1;
                                        us.UserExtra.LevelID = grupo.NivelEscolarID;



                                        escuela.NumAlum = escuela.NumAlum + 1;
                                        //var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == grupo.SchoolID && x.IsAprobado == true).ToList();
                                        //var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == grupo.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                        //if (licvigente != null)
                                        //{
                                        //    LicenciaUsuario newlic = new LicenciaUsuario()
                                        //    {
                                        //        Id = Guid.NewGuid(),
                                        //        IdLicencia = licvigente.Id,
                                        //        IdUsuario = userid,
                                        //        FechaRegistro = DateTime.Now,
                                        //        IsActivo = true,
                                        //        IsPrueba = licvigente.IsPrueba
                                        //    };

                                        //    db.LicenciaUsuarios.Add(newlic);
                                        //    idlicencia = licvigente.Id;
                                        //    idlicenciausuario = newlic.IdUsuario;

                                        //    //log
                                        //    if (idlicencia != Guid.Empty)
                                        //    {
                                        //        LogLicencia newlog = new LogLicencia()
                                        //        {
                                        //            Id = Guid.NewGuid(),
                                        //            IdLicencia = idlicencia,
                                        //            IdUsuarioRegistro = idlicenciausuario,
                                        //            FechaRegistro = DateTime.Now,
                                        //            TipoDestino = 0,
                                        //            TipoRegistro = 1,
                                        //            Comentario = "Se agrego el alumno por Excel y/o Registro solo"
                                        //        };
                                        //        db.LogLicencias.Add(newlog);
                                        //        //db.SaveChanges();
                                        //    }
                                        //}



                                        result = new string(
                                            Enumerable.Repeat(chars, 8)
                                                        .Select(s => s[random.Next(s.Length)])
                                                        .ToArray());

                                        String nip = "A" + result;

                                        Student nuevoEstudiante = new Student()
                                        {
                                            StudentID = Guid.NewGuid(),
                                            UserID = userid,
                                            EjerciciosCorrectosConsecutivos = 0,
                                            EjerciciosCompletados = 0,
                                            EjerciciosCorrectos = 0,
                                            EjerciciosTiempoMenor = 0,
                                            TiempoReal = 0,
                                            TiempoPromedio = 0,
                                            PuntosTotales = 0,
                                            Creditos = 0,
                                            NivelPuntos = 0,
                                            Nip = nip,
                                            GroupMateriaAlumnoes = new List<GroupMateriaAlumno>(),
                                            IdUserCreado = (Guid)Membership.GetUser().ProviderUserKey,
                                        };

                                        //Group grupo = db.Groups.Find(model.GroupID);
                                        nuevoEstudiante.Groups.Add(grupo);


                                        #region Agregar Materias
                                        //////agregar las materias de ese grupo al Alumno
                                        //foreach (var materia in grupo.GroupMaterias)
                                        //{
                                        //    var materiaAlumno = new GroupMateriaAlumno()
                                        //    {
                                        //        Id = Guid.NewGuid(),
                                        //        IdGroupMateria = materia.Id,
                                        //        GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                        //        IsCerrada = false
                                        //    };


                                        //    #region Agregar Parciales
                                        //    /////agregrar los parciales de esa materias a la materia del alumno
                                        //    foreach (var parcialmateria in materia.GroupMateriaParciales)
                                        //    {
                                        //        var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                        //        {
                                        //            Id = Guid.NewGuid(),
                                        //            IdGroupMateriaParcial = parcialmateria.Id,
                                        //            GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()

                                        //        };

                                        //        ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                        //        foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                        //        {
                                        //            var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                        //            {
                                        //                Id = Guid.NewGuid(),
                                        //                Porcentaje = evaluacionparcialmateria.Porcentaje,
                                        //                IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                        //                GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                        //            };
                                        //            ///has evaluaciones?
                                        //            ///
                                        //            ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                        //            ///a la evaluacion del parcial de la materia al alumno
                                        //            foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                        //            {
                                        //                var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                        //                {
                                        //                    Id = Guid.NewGuid(),
                                        //                    IsCheckBox = true,
                                        //                    Cumplio = true,
                                        //                    IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                        //                };
                                        //                evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);

                                        //            }

                                        //            parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                        //        }

                                        //        materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                        //    }
                                        //    #endregion

                                        //    nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                        //}
                                        #endregion

                                        db.Students.Add(nuevoEstudiante);
                                        db.SaveChanges();

                                        ///
                                        var usuarioMembership = Membership.GetUser(nuevousername);

                                        //if (usuarioMembership.ChangePassword(usuarioMembership.ResetPassword(), "123456"))
                                        //{
                                        us.UserName = nuevousername;
                                        us.LoweredUserName = nuevousername.ToLower();
                                        us.UserExtra.Password = nuevousername.ToLower();
                                        //  db.SaveChanges();


                                        model.is_procesado = true;
                                        model.NipAlumno = nuevoEstudiante.Nip;
                                        model.NipGrupo = grupo.Nip;
                                        model.UsuarioAlumno = nuevousername.ToLower();
                                        model.ContraseñaAlumno = nuevousername.ToLower();
                                        db.SaveChanges();

                                    }
                                    catch (DbEntityValidationException dbEx)
                                    {
                                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                                        {
                                            foreach (var validationError in validationErrors.ValidationErrors)
                                            {
                                                Mensajes.Add("Error al crear usuario: " + model.NOMBRE);
                                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                            }
                                        }
                                        //return View(model);
                                    }

                                    Mensajes.Add("Se registro al usuario " + model.NOMBRE + ", en el grupo " + model.GRUPO + " de la escuela " + model.NOMBRE_CT);
                                }
                            }
                            else {

                                model.is_procesado = false;
                                model.is_passed = true;


                                db.SaveChanges();

                            }
                            #endregion

                        }
                    }
                    else
                    {
                        model.is_procesado = false;
                        db.SaveChanges();
                    }

                } //endforeach
                var registrados = nuevos.Select(x => x.is_procesado == true);
                var total = registrados.Count();
                return Json(total, JsonRequestBehavior.AllowGet);
            }

            #endregion


            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");

                return Json(Mensajes, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(Mensajes, JsonRequestBehavior.AllowGet);
            }
        }

        //[Authorize(Roles = "Licencia")]
        public JsonResult ProcesarAlumnosNuevosConalepPreparatoria(string municipio)
        {

            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities();
                List<Student> estudiantes = new List<Student>();
                IMembershipService MembershipService = new AccountMembershipService();
                // var nuevos = db.xxAltaAlumnosConalep_.Where(x => !x.isprocesado_.Value && x.MUNICIPIO == municipio && !x.is_passed.Value).Take(1000);
                //var nuevos = db.xxAltaAlumnosConalep_.Where(x => !x.isprocesado_.Value);
                var nuevos = db.xxAltaAlumnosConalep2.Where(x => !x.isprocesado_.Value &&x.id > 17048);

                //  List<xxxAlumnosTamaulipasSecNR_> NoRegistrado = new List<xxxAlumnosTamaulipasSecNR_>();
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var numeros = "0123456789";
                //var colores = new string[] { "azul", "verde", "rojo", "amarillo" };
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 4)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());


                var numero = nuevos.Count();
                #region foreach Estudiantes

                foreach (var model in nuevos.ToList())
                {
                    bool uservalido = false;
                    var user2 = db.aspnet_Users.Where(x=>x.UserName==model.Contraseña);
                    if (user2!=null)
                    {
                        model.NOMBRE = RemoveDiacritics(model.NOMBRE);
                        if (!string.IsNullOrEmpty(model.PRIMER_APELLIDO))
                        {
                            model.PRIMER_APELLIDO = RemoveDiacritics(model.PRIMER_APELLIDO);
                        }
                        else
                        {
                            model.PRIMER_APELLIDO = "";
                        }


                        if (!string.IsNullOrEmpty(model.SEGUNDOAPELLIDO))
                        {
                            model.SEGUNDOAPELLIDO = RemoveDiacritics(model.SEGUNDOAPELLIDO);
                        }
                        else
                        {
                            model.SEGUNDOAPELLIDO = "";
                        }


                        var CCT = model.CCT.TrimEnd();
                        Guid idlicencia = Guid.Empty;
                        Guid idlicenciausuario = Guid.Empty;
                        // var nipescuela = model.Nip.TrimEnd();
                        var escuela = db.Schools.FirstOrDefault(x => x.ClaveOficial == CCT);


                        if (!string.IsNullOrEmpty(model.NOMBRE) || !string.IsNullOrWhiteSpace(model.NOMBRE))
                        {
                            if (escuela == null)
                            {
                                Mensajes.Add("Escuela no registrada, registre la escuela: " + model.NOMBRE_CT + " para poder continuar");
                                model.isprocesado_ = false;
                                db.SaveChanges();
                            }
                            else
                            {

                                #region Grado
                                var gruponum = 0;
                                switch (model.GRADO)
                                {
                                    case 1:
                                        gruponum = 10;
                                        break;
                                    case 2:
                                        gruponum = 11;
                                        break;
                                    case 3:
                                        gruponum = 12;
                                        break;
                                    case 4:
                                        gruponum = 13;
                                        break;
                                    case 5:
                                        gruponum = 14;
                                        break;
                                    case 6:
                                        gruponum = 15;
                                        break;


                                    default:
                                        gruponum = 0;
                                        break;
                                }




                                //var gruponum = (model.GRADO == 1) ? "positive" : "negative";

                                //model.GRADO;
                                #endregion

                                var des = model.GRUPO;
                                var turno = model.TURNO;
                                var grupo = db.Groups.FirstOrDefault(x => x.SchoolID == escuela.SchoolID
                                    && x.IsActivo == true
                                    && x.NivelEscolar.Grado == gruponum
                                    && x.Description == model.GRUPO
                                    );

                                var Userext = db.UserExtras.FirstOrDefault(x => x.Name == model.NOMBRE
                                    && x.LastNameP == model.PRIMER_APELLIDO
                                    && x.LastNameM == model.SEGUNDOAPELLIDO
                                    );

                                #region Crear grupo
                                //  Crear grupo si no existe
                                if (grupo == null)
                                {

                                    var enumnip = new string(
                                        Enumerable.Repeat(chars, 5)
                                                    .Select(s => s[random.Next(s.Length)])
                                                    .ToArray());

                                    String nip = "G" + enumnip;

                                    grupo = new DistribucionSofia.Models.Group()
                                    {
                                        GroupID = Guid.NewGuid(),
                                        SchoolID = escuela.SchoolID,
                                        NivelEscolarID = db.NivelEscolars.First(x => x.Grado == gruponum).LevelID,
                                        //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                        Description = model.GRUPO,
                                        IsActivo = true,
                                        Generacion = 2019,
                                        StartDate = DateTime.UtcNow,
                                        Nip = nip,
                                        NumParciales = 0,
                                        Activo = true,
                                        IsCerrado = false,
                                        IsOficial = true
                                    };
                                    grupo.Timezone = db.Configuracions.First().Timezone;
                                    grupo.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                                    if (grupo.SchoolID != null)
                                    {
                                        grupo.Timezone = escuela.Timezone;
                                        grupo.TimezoneOffset = escuela.TimezoneOffset;
                                    }
                                    //Materias
                                    //foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                    //{
                                    //    grupo.GroupMaterias.Add(new GroupMateria()
                                    //    {
                                    //        Id = Guid.NewGuid(),
                                    //        Materia = materia,
                                    //        NumParciales = 0,
                                    //        //Validado = 1, ///validacion extra metadata
                                    //    });

                                    //}
                                    db.Groups.Add(grupo);
                                    db.SaveChanges();
                                    // Mensajes.Add("Se agrego el grupo: " + gruponum + model.GRUPO);
                                }
                                #endregion

                                #region Crear alumno
                                //var usuario = "";
                                //var nuevousername = "";
                                #region CleanNeame
                                var n = model.NOMBRE.Split(' '); //nombre.First.Split(' ');
                                var name = "";
                                foreach (var n1 in n)
                                {
                                    if (n1 != "")
                                    {
                                        name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                        break;
                                    }

                                }
                                var ap = model.PRIMER_APELLIDO.Split(' ');// nombre.Second.Split(' ');
                                var app = "";
                                foreach (var ap1 in ap)
                                {
                                    if (ap1 != "")
                                    {
                                        app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                        break;
                                    }
                                }

                                var am = model.SEGUNDOAPELLIDO.Split(' ');// nombre.Third.Split(' ');
                                var amm = "";
                                foreach (var am1 in am)
                                {
                                    if (am1 != "")
                                    {
                                        amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                        break;
                                    }
                                }
                                #endregion

                                var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                                var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                                var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;
                                var nuevousername = model.Contraseña; // string.Empty;
                                var contrasena = model.Contraseña; // string.Empty;

                                //while (!uservalido)
                                //{
                                //    result = new string(
                                //    Enumerable.Repeat(numeros, 3)
                                //              .Select(r => r[random.Next(r.Length)])
                                //              .ToArray());

                                //    nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                                //    nuevousername = nuevousername.RemoveDiacritics();
                                //    if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                                //    {
                                //        uservalido = true;
                                //        contrasena = nuevousername;
                                //    }

                                //}


                                if (grupo != null)
                                {
                                    nuevousername = nuevousername.ToLower();
                                    MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;


                                    createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@general.com");
                                    if (createStatus == MembershipCreateStatus.Success)
                                    {
                                        try
                                        { //FormsService.SignIn(model.UserName, false /* createPersistentCookie */);

                                            MembershipUser user = Membership.GetUser(nuevousername, false);
                                            Guid userid = (Guid)user.ProviderUserKey;

                                            Roles.AddUserToRole(nuevousername, "Alumno");

                                            //   aspnet_Users us = db.aspnet_Users.Find(userid);
                                            var us = db.aspnet_Users.SqlQuery("select * from aspnet_Users where UserId = '" + userid + "'").First();
                                            us.UserExtra = new UserExtra();
                                            var tu = 3;
                                            us.UserExtra.TipoUsuario = tu;
                                            us.UserExtra.IdEstado = escuela.IdEstado;
                                            us.UserExtra.IdPais = escuela.IdPais;
                                            us.UserExtra.Ciudad = escuela.City;
                                            us.UserExtra.Name = model.NOMBRE.ToUpper().Trim();
                                            us.UserExtra.LastNameP = model.PRIMER_APELLIDO;
                                            us.UserExtra.LastNameM = model.SEGUNDOAPELLIDO;
                                            us.UserExtra.Password = contrasena.ToLower();
                                            us.UserExtra.SexoCode = 1;
                                            us.UserExtra.LevelID = grupo.NivelEscolarID;
                                            escuela.NumAlum = escuela.NumAlum + 1;


                                            result = new string(
                                                Enumerable.Repeat(chars, 8)
                                                            .Select(s => s[random.Next(s.Length)])
                                                            .ToArray());

                                            String nip = "A" + result;

                                            Student nuevoEstudiante = new Student()
                                            {
                                                StudentID = Guid.NewGuid(),
                                                UserID = userid,
                                                EjerciciosCorrectosConsecutivos = 0,
                                                EjerciciosCompletados = 0,
                                                EjerciciosCorrectos = 0,
                                                EjerciciosTiempoMenor = 0,
                                                TiempoReal = 0,
                                                TiempoPromedio = 0,
                                                PuntosTotales = 0,
                                                Creditos = 0,
                                                NivelPuntos = 0,
                                                Nip = nip,
                                                GroupMateriaAlumnoes = new List<GroupMateriaAlumno>(),
                                                IdUserCreado = (Guid)Membership.GetUser().ProviderUserKey,
                                            };

                                            //Group grupo = db.Groups.Find(model.GroupID);
                                            nuevoEstudiante.Groups.Add(grupo);



                                            db.Students.Add(nuevoEstudiante);
                                            db.SaveChanges();

                                            ///
                                            var usuarioMembership = Membership.GetUser(nuevousername);

                                            //if (usuarioMembership.ChangePassword(usuarioMembership.ResetPassword(), "123456"))
                                            //{
                                            us.UserName = nuevousername;
                                            us.LoweredUserName = nuevousername.ToLower();
                                            us.UserExtra.Password = nuevousername.ToLower();
                                            //  db.SaveChanges();


                                            model.isprocesado_ = true;
                                            model.NipAlumno = nuevoEstudiante.Nip;
                                            model.NipGrupo = grupo.Nip;
                                            model.UsuarioAlumno = nuevousername.ToLower();
                                            model.ContraseñaAlumno = nuevousername.ToLower();
                                            db.SaveChanges();

                                        }
                                        catch (DbEntityValidationException dbEx)
                                        {
                                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                                            {
                                                foreach (var validationError in validationErrors.ValidationErrors)
                                                {
                                                    Mensajes.Add("Error al crear usuario: " + model.NOMBRE);
                                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                                }
                                            }
                                            //return View(model);
                                        }

                                        Mensajes.Add("Se registro al usuario " + model.NOMBRE + ", en el grupo " + model.GRUPO + " de la escuela " + model.NOMBRE_CT);
                                    }
                                }
                                else
                                {

                                    model.isprocesado_ = false;
                                    //model.is_passed = true;


                                    db.SaveChanges();

                                }
                                #endregion

                            }
                        }
                        else
                        {
                            model.isprocesado_ = false;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        model.isprocesado_ = false;
                        model.isrepetido = true;
                        db.SaveChanges();
                    }
                   


                } //endforeach


                var registrados = nuevos.Select(x => x.isprocesado_ == true);
                var total = registrados.Count();





                return Json(total, JsonRequestBehavior.AllowGet);
            }

            #endregion


            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");

                return Json(Mensajes, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(Mensajes, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Licencia")]
        public JsonResult ProcesarAlumnosDiagnostico(string idEscuela)
        {
            string idescuelas = "BA63EB18-5B25-48EF-ABF1-14D7568CD7BB";//"6F6AA1D2-4631-48D5-A15F-F78F2C1BFC3A";//"F536A8C2-3AFE-4B7A-8805-96A4C592407F";
                //"A8880C84-90CB-44A5-BA75-5796E31F5439,93C505B2-1D7D-4960-8E2F-3C6C22707FBF,AD2E2E8E-CDE8-4482-B300-773E10223A14,CC339DFB-7E3A-427D-8271-B47E1EB911B0";//"202ACC14-E095-4409-B8A5-6E7DADB562CB,24315C4C-743F-4914-902F-56425B5F2035,5425D05F-96AF-463C-B33D-8A766B9B4D7B,58D63D7B-0ECE-46DE-B1C4-9E6E0D58D7B2,5F8CF099-4AE0-4F91-BE5F-F2D5AFEFFD87,7C39FB85-90D6-47CD-9B0D-8CAFB77ECE84,7E5E7B2B-C53D-4C3D-8455-BDECF85198D7,8D839B12-E54A-41F0-BDDE-45E4DE493952,93C505B2-1D7D-4960-8E2F-3C6C22707FBF,A8880C84-90CB-44A5-BA75-5796E31F5439,AD2E2E8E-CDE8-4482-B300-773E10223A14,CC339DFB-7E3A-427D-8271-B47E1EB911B0,EE5166F9-9F9F-4B09-99A4-BF7F3699906B,F536A8C2-3AFE-4B7A-8805-96A4C592407F,FBBC2555-21D7-412D-A4CC-2FF6DAC4B861";//"8F1D4815-C648-4A0F-B576-2004D5A79C44,F1F1005B-1D63-4FDB-89CE-D0D4B1A526F7";
                //"4C67BC40-8F89-4817-B2CC-B0470945C74C,40786B76-E763-4D57-BBBC-CFDF026BC5A1,3565DECC-2A8C-4F84-9CE9-2B465C2A1878,987FB54B-81B6-4B9F-ADAF-B6C32EB527DF";//"3AE33AFA-81C2-4872-BDDF-E0583FBCDB13,1AFD5AEF-014A-4A4E-8FFB-D3E3354DF789,C3F89379-F320-4F63-BFA5-8F74F9C9D009,7ABB7A5B-C480-4A1C-914B-C3B8E0EBB3E7,6261A9F5-2981-4A48-A023-EEAC9B989B78,5298275B-10C1-43C1-BDC2-EEB58B14325F,C6041B78-2EFF-4830-AC96-E63D1FCA326D";//"78A48B69-891C-4580-AF59-F6012E0E2638,386769D9-0DB0-4BF8-BDA0-CB014DA854F0,67449392-CB85-415F-A9D8-80F6F60B5357,BB1D2491-D5FD-4347-B144-78A93B0C5CA2,76F08126-C04F-40CD-8676-41D9269C7BB7,AF2BA196-D546-4336-A50C-F28DC4E19DC7,009820DC-B4AD-441A-9E60-D579C766F519,147C9A41-939A-4FAD-92F4-E0B1BAE32DED,F6A642E1-8046-422C-91A7-0E4E695F31C0"; //"56536CD3-7F28-4B4B-BF6A-ACB3A1F4CAEA,98CF8F8A-5D89-47C2-BC7C-4BE2D7CF26DF,3B32C15D-D7B7-4B51-B001-7E9076462A1C,53F12406-BB41-426F-8A31-704686D7F7B9,B3A1FE93-260B-4C44-BE1B-02152E121DD5,1AA3BAD2-0533-47C3-A032-6106FA2EA921,D344C3AE-192C-46A7-AC9C-D01131347E0A,DD06027F-F539-47A3-B621-693AA2EFBBAE,A8CE344A-BA49-4AB5-924E-317152FA9C33,F79F0168-8775-4EDF-A643-612F5E560F5C,DF7E2333-9DE0-4127-A09A-3A26F62D55B9,D7186144-20D0-4346-907E-CB78DB466F13,ACD2CE87-7F64-486A-88EC-9B6CED767767,BB8B9858-D19A-4563-ABA1-D41B7263E6B8,4095CC0D-F1BE-4A62-963F-1C027EECBC76,766A0826-39E8-4883-A277-CE58D51BB73D,DFADD15A-358D-4197-82C0-B62028F2718F,819A5277-4BE8-4DA8-B700-AF5B82B1321A,DAF5467E-6F32-4EE5-B01A-5D768D5673C3,E02D76EF-8A06-4C96-A05B-DDB4537EB5F6,1E83B4C1-78AA-4B6C-A346-8C4581521B4F,5E6A2676-8086-4E45-BCF2-AA3B3CD79EF6,F6617CF4-56FC-4BC8-80F9-40D4BA6DBD32,25ECD9B8-E3E9-4AA2-A558-78C34C9E8B17,537DF58D-B63B-4785-8BCE-C719D050F142,C53F49F9-9D88-4232-94A7-F8E70CA55245,D5B5E0B2-9E3E-486C-B873-E7D0FC1C3A0B,0436F5A0-4874-4139-819E-71CDF66FEE15,0AE9AF2D-5807-4110-8889-61FC113E8651,DD5D514C-101D-4AA1-8A9F-6E013A6E7649,5298275B-10C1-43C1-BDC2-EEB58B14325F,C6041B78-2EFF-4830-AC96-E63D1FCA326D,1AFD5AEF-014A-4A4E-8FFB-D3E3354DF789,4C67BC40-8F89-4817-B2CC-B0470945C74C,7ABB7A5B-C480-4A1C-914B-C3B8E0EBB3E7,A7A1030E-671F-46F1-B77A-5D8FB10E0530,6FE32327-264F-4E15-85EB-C512115FB839,A5932198-351B-460E-91DD-D9315568D177,40786B76-E763-4D57-BBBC-CFDF026BC5A1,3565DECC-2A8C-4F84-9CE9-2B465C2A1878,987FB54B-81B6-4B9F-ADAF-B6C32EB527DF,3AE33AFA-81C2-4872-BDDF-E0583FBCDB13,C3F89379-F320-4F63-BFA5-8F74F9C9D009,267ADC17-1582-4D55-AA54-79D6CAF0091D,8F1D4815-C648-4A0F-B576-2004D5A79C44,F1F1005B-1D63-4FDB-89CE-D0D4B1A526F7,6261A9F5-2981-4A48-A023-EEAC9B989B78,78A48B69-891C-4580-AF59-F6012E0E2638,386769D9-0DB0-4BF8-BDA0-CB014DA854F0,67449392-CB85-415F-A9D8-80F6F60B5357,BB1D2491-D5FD-4347-B144-78A93B0C5CA2,76F08126-C04F-40CD-8676-41D9269C7BB7,AF2BA196-D546-4336-A50C-F28DC4E19DC7,009820DC-B4AD-441A-9E60-D579C766F519,147C9A41-939A-4FAD-92F4-E0B1BAE32DED,F6A642E1-8046-422C-91A7-0E4E695F31C0,948009A3-788D-4967-853E-D4D62AEDE079,CAE6A2F3-D45B-44C5-910D-2ABE1D3ED231,E196E267-FE8B-4AC7-BA9B-E9DB7A7CCD51,BD951DD4-76EB-454B-8A5E-FDC6E6B0D0B5,C4875038-03C3-4829-B79B-7961E17F6E3E,935A0F59-A6C2-41A2-BB9B-77CA26115ECA,EC0F7C0F-C572-43BA-A7A0-4309091AF162,862765F1-0130-420E-A4D3-547AFDC32398,279B435C-F5D1-4918-AE00-18E2E7A00877";
            idescuelas = Regex.Replace(idescuelas, @"\t|\n|\r", "");
            List<string> Mensajes = new List<string>();
            //List<xxAlumnosProcesarSinDiagnostico_> AlumnossinProcesar = new List<xxAlumnosProcesarSinDiagnostico_>();
            List<xxAlumnosCrearExamen_> AlumnossinProcesar = new List<xxAlumnosCrearExamen_>();
            Random rand = new Random();
            //List<ViewModel_AlumnosDiagnostico> AlumnossinProcesar = new List<ViewModel_AlumnosDiagnostico>();
            string[] idschools = idescuelas.Split(',');
            foreach (var ids in idschools)
            {
                List<xxAlumnosCrearExamen_> AlumnossinProcesar2 = db.xxAlumnosCrearExamen_.Where(x => x.IdEscuela == ids && x.isProcesado == 0).ToList();//.Take(10).ToList();
               // List<xxAlumnosCrearExamen_> AlumnossinProcesar2 = db.xxAlumnosCrearExamen_.Where(x => x.Ciudad == "Cd. Victoria" && x.isProcesado == 0 && x.Xexamen2<6).Take(1000).ToList();



                foreach (var a in AlumnossinProcesar2)
                {
                    AlumnossinProcesar.Add(a);
                }
                // AlumnossinProcesar.Add();

            }

            int count = AlumnossinProcesar.Count();
            try
            {
                foreach (var al in AlumnossinProcesar)
                {
                    var muestra = db.sofia_xxxMuestras(Convert.ToInt32(al.taskid), al.Ciudad).ToList();
                    var muestra2 = muestra.Where(x=> x.TC_Score>= (al.Xexamen.Value));

                    int muest = muestra2.Count();
                    int index = rand.Next(muest);
                    var copia = muestra2.ElementAt(index);
                    var userexercice = db.sofia_xxxMuestras_UserExercisesDetail(copia.TC_IdEstudiante, copia.TC_ID, copia.TA_Id).ToList();

                    while (userexercice.Count(x => x.UserAndExercisesDetails_Answers == null) >= 1)
                    {
                        muestra = db.sofia_xxxMuestras(Convert.ToInt32(al.taskid), al.Ciudad).ToList();
                        muestra2 = muestra.Where(x => x.TC_Score > al.Xexamen2);
                        muest = muestra2.Count();
                        index = rand.Next(muest);
                        copia = muestra2.ElementAt(index);
                        userexercice = db.sofia_xxxMuestras_UserExercisesDetail(copia.TC_IdEstudiante, copia.TC_ID, copia.TA_Id).ToList();
                    }


                    int sumafechas = rand.Next(5);
                    var startdate = copia.TA_StartDate.AddDays(sumafechas);
                    var finishdate = copia.TC_FinishDate.AddDays(sumafechas);


                    Temp_Activity TA = new Temp_Activity {
                        Id = Guid.NewGuid()
                    , IsFinished = copia.TA_IsFinished
                    , Resultados = copia.TA_Resultados
                    , ResultadosUpdateDate = finishdate
                    , StartDate = startdate
                    , StudentId = Guid.Parse(al.IdEstudiante)
                    };

                    db.Temp_Activity.Add(TA);

                    Tasks_Complete TC = new Tasks_Complete
                    {

                        StudentID = Guid.Parse(al.IdEstudiante)
                    ,
                        TaskID = Convert.ToInt32(al.taskid)
                    ,
                        FinishDate = finishdate
                    ,
                        Points = copia.TC_Points
                    ,
                        Score = copia.TC_Score
                    ,
                        StartDate = startdate
                    ,
                        TotalTime = copia.TC_TotalTime

                    };
                    Guid userid = new Guid(al.UserID);
                    var alumno = db.aspnet_Membership.Where(x=> x.UserId == userid ).FirstOrDefault();
                    DateTime lastdayExamen = new DateTime(2019, 05, 20);
                    if (alumno.LastLoginDate< lastdayExamen)
                    {
                        Random r = new Random();
                        int rInt = r.Next(21, 31);
                        alumno.LastLoginDate = new DateTime(2019,05,rInt);
                    }

                    db.Tasks_Complete.Add(TC);


                    db.SaveChanges();

                    var idtemporalanterior = copia.TA_Id;
                    var idestudiantenuevo = Guid.Parse(al.IdEstudiante);
                    var idtaskcompletenuevo = TC.tcID;
                    var idtaskcompleteanterior = copia.TC_ID;
                    var idtemporalnuevo = TA.Id;
                    var ejercicios = db.sofia_actividad_ObtenerEjerciciosActividadTemporal_v3(idtemporalanterior).ToList();
                    var lista_equivalencia = ejercicios.OrderBy(x => x.IdEjercicio).Select(x => new Tuple<Guid, Guid>(x.Id, Guid.NewGuid())).ToList();

                    var l_userandexercisesids = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => lista_equivalencia.First(y => y.Item1 == x.Id).Item2.ToString()));
                    var l_estudiante = idestudiantenuevo;//string.Join(",", ejercicios.Select(x => stId.HasValue ? stId.Value.ToString() : "-1"));
                    var l_ejerciciosid = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.IdEjercicio.ToString()));
                    var l_intentos = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.Intentos.ToString()));
                    var l_puntos = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => Convert.ToInt32(x.Puntos).ToString()));
                    var l_tiempo = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.Tiempo.ToString()));
                    var l_score = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.Score.ToString()));
                    var l_skip = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => (x.Skip ? 1 : 0).ToString()));
                    var l_idtemporal = idtemporalanterior;
                    var l_iscorrectos = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => (x.IsCorrecto.Value ? 1 : 0).ToString()));


                    var f = ejercicios.Select(x => 1);
                    var p = ejercicios.Select(x => 60);
                    var mt = ejercicios.OrderBy(x => x.IdEjercicio).Select(x => (x.Multiple_tries ? 1 : 0));

                    var l_factores = string.Join(",", f);
                    var l_tiempoprom = string.Join(",", p);
                    var l_multiple_tries = string.Join(",", mt);


                    //SCORE FINAL
                    var l_scorefinal = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.ScoreFinal.ToString()));
                    //INTENTOS REALES
                    var l_intentosreales = string.Join(",", ejercicios.OrderBy(x => x.IdEjercicio).Select(x => x.IntentosReales.ToString()));

                    var l_idgrupo = Guid.Parse(al.IdGrupo); //Guid.Empty;//string.Join(",", ejercicios.Select(x => infoestudiante.IdGrupo.ToString()));
                    var l_idescuela = Guid.Parse(al.IdEscuela);// Guid.Empty;//string.Join(",", ejercicios.Select(x => infoestudiante.IdEscuela.ToString()));
                    var l_idestado = Guid.Parse(al.IdEstado); //Guid.Empty;//string.Join(",", ejercicios.Select(x => infoestudiante.IdEstado.ToString()));
                    var l_idpais = Guid.Parse(al.IdPais);//string.Join(",", ejercicios.Select(x => infoestudiante.IdPais.ToString()));
                    var l_ciclo = 2018;//.CicloEscolar; //string.Join(",", ejercicios.Select(x => infoestudiante.CicloEscolar.ToString()));



                    var detalles = db.sofia_xxxMuestras_UserExercisesDetail(copia.TC_IdEstudiante, copia.TC_ID, copia.TA_Id).ToList();


                    var l_ued_idusuarioejercicios = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                                .ThenBy(x => x.UserAndExercisesDetails_Intento)
                                .Select(x => lista_equivalencia.First(y => y.Item1 == x.UserAndExercises_uaeID).Item2.ToString()));

                    var l_ued_idejercicios = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercises_ExerciseID.ToString()));

                    var l_ued_repuestas = string.Join("$%&", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_Answers.ToString()));

                    var l_ued_intentos = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_Intento.ToString()));


                    var l_ued_opciones = string.Join("$%&", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_Options.ToString()));

                    var l_ued_procesoscorrectos = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_ProcesosCorrectos.ToString()));

                    var l_ued_procesostotales = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_ProcesosTotales.ToString()));

                    var l_ued_calificacion = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_Calificacion.ToString()));

                    var l_ued_intentosreales = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercises_IntentosReales.ToString()));

                    var l_ued_isintentosreales = string.Join(",", detalles.OrderBy(x => x.UserAndExercises_uaeID)
                            .ThenBy(x => x.UserAndExercisesDetails_Intento)
                            .Select(x => x.UserAndExercisesDetails_IsIntentoReal.ToString()));


                    var dbejercicios = db.sofia_actividad_GuardarEjercicios_v3(l_userandexercisesids
                        , l_estudiante
                        , l_ejerciciosid
                        , l_intentos
                        , l_puntos
                        , l_tiempo
                        , l_score
                        , l_multiple_tries
                        , l_skip
                        , idtemporalnuevo
                        , l_tiempoprom
                        , l_idgrupo
                        , l_idescuela
                        , l_idestado
                        , l_idpais
                        , l_ciclo, 1, 1, l_factores, l_iscorrectos, l_intentosreales, l_scorefinal).ToList();

                    var dbdetalles = db.sofia_actividad_GuardarEjerciciosDetalles_v2(l_ued_idusuarioejercicios, l_ued_repuestas
                     , l_ued_intentos, l_ued_opciones, l_ued_procesoscorrectos, l_ued_procesostotales, l_ued_calificacion, l_ued_intentosreales, l_ued_isintentosreales).ToList();



                    db.sofia_actividad_ActualizarEjerciciosActividadTemporal(idtemporalnuevo
                        , idtaskcompletenuevo
                        , -1 //IdQuicktest
                        , Guid.Empty //IdAssigment
                        , Guid.Empty); //IdAssigmentAsignado


                    al.isProcesado = 1;
                    al.isProcesado2 = 1;
                    db.SaveChanges();

                }

            }
            catch (Exception e)
            {

                string ex = e.Message;
            }


            //db.TEM

            return Json(JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Licencia")]
        public string ReporteDiagnosticoPaquetes(string idEscuela)
        {
                Guid idDistribuidor = Guid.Parse("B6FC164B-89F3-458A-B0EF-2839C3D63860");
                List<Models.ViewModels.ViewModel_Reporte> Alumnos = new List<Models.ViewModels.ViewModel_Reporte>();
                var R = db.sofia_general_EscuelaEstudianteDiagnosticoPaquetes(null,null,null,idDistribuidor,null,null,null).ToList();
             //   R = R.Where(x=> x.IdEstudiante== Guid.Parse("a5135f89-8e22-4be5-875e-fac6793cccdb")).ToList();
               // int t = R.Count();


                try
                {

                    foreach (var E in R)
                    {
                        if (Alumnos.Count() == 956)
                        {
                            string g = "holi";
                        }
                        var temp = Alumnos.Where(x => x.IdEstudiante == E.IdEstudiante.Value).FirstOrDefault();

                        if (temp is null)
                        {

                            Models.ViewModels.ViewModel_Reporte alumno = new Models.ViewModels.ViewModel_Reporte
                            {
                                IdEstudiante = E.IdEstudiante.Value
                                ,
                                IdSchool = E.IdSchool.Value
                                ,
                                NombreAlumno = E.Estudiante
                                ,
                                NombreEscuela = E.NombreEscuela
                                ,
                                Grupo = E.Grupo
                                ,
                                Diagnostico = E.Diagnostico.Value 
                                ,
                                Paquete1 = E.PaqueteScore
                                ,
                                UltimoPaquete = 1
                                ,
                                UltimoPaqueteFecha = E.PaqueteFecha
                                 ,Paquete2 = "-"
                                 ,Paquete3 ="-"
                                 ,Paquete4 = "-"
                                 ,Paquete5 = "-"
                                 ,Paquete6 = "-"
                                 ,Paquete7 = "-"
                            };
                            if (alumno is null|| Alumnos is null)
                            {
                           
                            }
                            else
                            {
                                Alumnos.Add(alumno);
                            }
                       
                        }
                        else
                        {
                            if (DateTime.ParseExact(E.PaqueteFecha, "MM/dd/yy", System.Globalization.CultureInfo.InvariantCulture) != DateTime.ParseExact(temp.UltimoPaqueteFecha, "MM/dd/yy", System.Globalization.CultureInfo.InvariantCulture))
                            {

                                temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                if (temp.UltimoPaquete == 1)
                                {
                                    temp.UltimoPaquete = 2;
                                    temp.Paquete2 = E.PaqueteScore;
                                    temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                }
                                else
                                {
                                    if (temp.UltimoPaquete == 2)
                                    {
                                        temp.UltimoPaquete = 3;
                                        temp.Paquete3 = E.PaqueteScore;
                                        temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                    }
                                    else
                                    {
                                        if (temp.UltimoPaquete == 3)
                                        {
                                            temp.UltimoPaquete = 4;
                                            temp.Paquete4 = E.PaqueteScore;
                                            temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                        }
                                        else
                                        {
                                            if (temp.UltimoPaquete == 4)
                                            {
                                                temp.UltimoPaquete = 5;
                                                temp.Paquete5 = E.PaqueteScore;
                                                temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                            }
                                            else
                                            {
                                                if (temp.UltimoPaquete == 5)
                                                {
                                                    temp.UltimoPaquete = 6;
                                                    temp.Paquete6 = E.PaqueteScore;
                                                    temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                                }
                                                else
                                                {

                                                    if (temp.UltimoPaquete == 6)
                                                    {
                                                        temp.UltimoPaquete = 7;
                                                        temp.Paquete6 = E.PaqueteScore;
                                                        temp.UltimoPaqueteFecha = E.PaqueteFecha;
                                                    }
                                                }

                                            }

                                        }

                                    }



                                }
                            
                                temp.IdAssigment = Guid.Parse("00000000-0000-0000-0000-000000000000");
                                int index = Alumnos.FindIndex(x => x.IdEstudiante.Equals(E.IdEstudiante));
                                Alumnos[index].Diagnostico =  temp.Diagnostico;
                                Alumnos[index].Grupo = temp.Grupo;
                                Alumnos[index].IdAssigment = temp.IdAssigment;
                                Alumnos[index].IdEstudiante = temp.IdEstudiante;
                                Alumnos[index].IdSchool = temp.IdSchool;
                                Alumnos[index].NombreAlumno = temp.NombreAlumno;
                                Alumnos[index].NombreEscuela= temp.NombreEscuela;
                                Alumnos[index].Paquete1 = temp.Paquete1;
                                Alumnos[index].Paquete2 = temp.Paquete2;
                                Alumnos[index].Paquete3 = temp.Paquete3;
                                Alumnos[index].Paquete4 = temp.Paquete4;
                                Alumnos[index].Paquete5 = temp.Paquete5;
                                Alumnos[index].Paquete6 = temp.Paquete6;
                                Alumnos[index].Paquete7 = temp.Paquete7;
                                Alumnos[index].UltimoPaquete = temp.UltimoPaquete;
                                Alumnos[index].UltimoPaqueteFecha = temp.UltimoPaqueteFecha;










                            }
                        }



                    }
                    var fi = new FileInfo(Server.MapPath("~/ImportedFiles/PLANTILLAS/ListaAlumnos.xlsx"));
                    using (var pi = new ExcelPackage(fi))
                    {
                        var ws = pi.Workbook.Worksheets["Reporte"];
                        ////.Cells[2, 10] = “ fila10 columna 2”;
                        ////Escuela
                        //ws.Cells[4, 4].Value = (s != null) ? s.Name : "";
                        //////Grupo
                        //ws.Cells[5, 4].Value = (grado != null) ? grado.Grado + " " + g.Description : "";
                        //////Maestro
                        //ws.Cells[6, 4].Value = (maestro != null) ? maestro.Name + " " + maestro.LastNameP + " " + maestro.LastNameM : "";
                        ////Fecha 
                        //ws.Cells[3, 4].Value = System.DateTime.Now.ToShortDateString();
                        //ws.Cells[3, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        ////usuario Maestro
                        //ws.Cells[7, 4].Value = (maestro != null) ? maestro.aspnet_Users.UserName : "";
                        ////Contraseña Maestro
                        //ws.Cells[7, 6].Value = (maestro != null) ? maestro.Password : "";

                        int i = 10;
                        foreach (var student in Alumnos)
                        {
                            ////Nombre Estudiante
                            ws.Cells[i, 2].Value = student.NombreAlumno;
                            //FormatoCelda
                            ws.Cells[i, 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            //Puntos Estudiante
                            ws.Cells[i, 3].Value = student.NombreEscuela;
                            //FormatoCelda
                            ws.Cells[i, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 3].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //Usuario Estudiante
                            ws.Cells[i, 4].Value = student.Grupo;
                            //FormatoCelda
                            ws.Cells[i, 4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 4].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            //Contraseña Estudiante
                            ws.Cells[i, 5].Value = student.Diagnostico;
                            //FormatoCelda
                            ws.Cells[i, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 5].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            //Nip Estudiante
                            ws.Cells[i, 6].Value = student.Paquete1;
                            ws.Cells[i, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //FormatoCelda
                            ws.Cells[i, 6].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 6].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 6].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            //Nip Estudiante
                            ws.Cells[i, 7].Value = student.Paquete2;
                            ws.Cells[i, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //FormatoCelda
                            ws.Cells[i, 7].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 7].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 7].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 7].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            //Nip Estudiante
                            ws.Cells[i, 8].Value = student.Paquete3;
                            ws.Cells[i, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //FormatoCelda
                            ws.Cells[i, 8].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 8].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 8].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 8].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            //Nip Estudiante
                            ws.Cells[i, 9].Value = student.Paquete4;
                            ws.Cells[i, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //FormatoCelda
                            ws.Cells[i, 9].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 9].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 9].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 9].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            //Nip Estudiante
                            ws.Cells[i, 10].Value = student.Paquete5;
                            ws.Cells[i, 10].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //FormatoCelda
                            ws.Cells[i, 10].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 10].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 10].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[i, 10].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            i++;
                        }

                        Response.ClearContent();
                        pi.SaveAs(Response.OutputStream);
                        Response.Buffer = true;
                         Response.AddHeader("content-disposition", "attachment; filename= REPORTE.xlsx");
                        Response.ContentType = "application/ms-excel";
                        Response.Flush();
                        Response.End();
                    }
                }
                catch (Exception e)
                {
                    string sx = e.Message;
                    throw;
                }

        



                
                    return "hi";
        }
        [Authorize(Roles = "Licencia")]
        public JsonResult ProcesarMaestrosNuevosTamaulipasPrimaria()
        {

            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities();
                List<Teacher> teacher = new List<Teacher>();
                IMembershipService MembershipService = new AccountMembershipService();
                var nuevos = db.xxMaestrosTamaulipasPrimaria_.Where(x => !x.is_procesado.Value).OrderBy(x => x.Id).Take(3000);
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                var numeros = "0123456789";
                //var colores = new string[] { "azul", "verde", "rojo", "amarillo" };
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 4)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());

                var numero = nuevos.Count();

                #region foreach Estudiantes

                foreach (var model in nuevos.ToList())
                {
                    bool uservalido = false;
                    model.nombre = RemoveDiacritics(model.nombre);
                    if (!string.IsNullOrEmpty(model.apellido1))
                    {
                        model.apellido1 = RemoveDiacritics(model.apellido1);
                    }
                    else
                    {
                        model.apellido1 = "";
                    }


                    if (!string.IsNullOrEmpty(model.apellido2))
                    {
                        model.apellido2 = RemoveDiacritics(model.apellido2);
                    }
                    else
                    {
                        model.apellido2 = "";
                    }



                    var CCT = model.cct.TrimEnd();
                    Guid idlicencia = Guid.Empty;
                    Guid idlicenciausuario = Guid.Empty;
                    // var nipescuela = model.Nip.TrimEnd();
                    var escuela = db.Schools.FirstOrDefault(x => x.ClaveOficial == CCT);


                    if (!string.IsNullOrEmpty(model.nombre))
                    {
                        if (escuela == null)
                        {
                            Mensajes.Add("Escuela no registrada, registre la escuela: " + model.cct + " para poder continuar");
                            model.is_procesado = false;
                            db.SaveChanges();

                        }
                        else
                        {
                            var gruponum = model.grado;
                            var des = model.grupo;
                            var grupo = db.Groups.FirstOrDefault(x => x.SchoolID == escuela.SchoolID
                                && x.IsActivo == true
                                && x.NivelEscolar.Grado == gruponum
                                && x.Description == des);
                       
                            #region Crear grupo
                            //Crear grupo si no existe
                            if (grupo == null)
                            {

                                var enumnip = new string(
                                    Enumerable.Repeat(chars, 5)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                String nip = "G" + enumnip;

                                grupo = new DistribucionSofia.Models.Group()
                                {
                                    GroupID = Guid.NewGuid(),
                                    SchoolID = escuela.SchoolID,
                                    NivelEscolarID = db.NivelEscolars.First(x => x.Grado == gruponum).LevelID,
                                    //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                    Description = model.grupo,
                                    IsActivo = true,
                                    Generacion = 2018,
                                    StartDate = DateTime.UtcNow,
                                    Nip = nip,
                                    NumParciales = 0,
                                    Activo = true,
                                    IsCerrado = false,
                                    IsOficial = true
                                };
                                grupo.Timezone = db.Configuracions.First().Timezone;
                                grupo.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                                if (grupo.SchoolID != null)
                                {
                                    grupo.Timezone = escuela.Timezone;
                                    grupo.TimezoneOffset = escuela.TimezoneOffset;
                                }
                                //Materias
                                foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                {
                                    grupo.GroupMaterias.Add(new GroupMateria()
                                    {
                                        Id = Guid.NewGuid(),
                                        Materia = materia,
                                        NumParciales = 0,
                                        //Validado = 1, ///validacion extra metadata
                                    });

                                }
                                db.Groups.Add(grupo);
                                db.SaveChanges();
                                Mensajes.Add("Se agrego el grupo: " + gruponum + model.grupo);
                            }
                            #endregion
                            
                            #region Crear alumno
                            //var usuario = "";
                            //var nuevousername = "";
                            #region CleanNeame
                            var n = model.nombre.Split(' '); //nombre.First.Split(' ');
                            var name = "";
                            foreach (var n1 in n)
                            {
                                if (n1 != "")
                                {
                                    name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }

                            }
                            var ap = model.apellido1.Split(' ');// nombre.Second.Split(' ');
                            var app = "";
                            foreach (var ap1 in ap)
                            {
                                if (ap1 != "")
                                {
                                    app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }

                            var am = model.apellido2.Split(' ');// nombre.Third.Split(' ');
                            var amm = "";
                            foreach (var am1 in am)
                            {
                                if (am1 != "")
                                {
                                    amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }
                            #endregion

                            var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                            var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                            var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;
                            var nuevousername = string.Empty;
                            var contrasena = string.Empty;

                            while (!uservalido)
                            {
                                result = new string(
                                Enumerable.Repeat(numeros, 4)
                                          .Select(r => r[random.Next(r.Length)])
                                          .ToArray());

                                nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                                nuevousername = nuevousername.RemoveDiacritics();
                                if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                                {
                                    uservalido = true;
                                    contrasena = nuevousername;
                                }

                            }

                            nuevousername = nuevousername.ToLower();

                            MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                            createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@general.com");
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                     MembershipUser user = Membership.GetUser(nuevousername, false);
                        Guid userid = (Guid)user.ProviderUserKey;
                        aspnet_Users us = db.aspnet_Users.Find(userid);
                       
                         UserExtra Teacherextra = new UserExtra();
                            Teacherextra.UserID = userid;
                            Teacherextra.Name = model.nombre;
                            Teacherextra.LastNameP = model.apellido1;
                            Teacherextra.LastNameM = model.apellido2;
                            Teacherextra.Password = contrasena;
                            Teacherextra.SexoCode = 1;
                            Teacherextra.IdEstado = escuela.IdEstado;
                            Teacherextra.IdPais = escuela.IdPais;
                            Teacherextra.Correo = "general@general.com";
                            Teacherextra.EmailPapa = "general@general.com";
                            Teacherextra.TipoUsuario = 2;
                            db.UserExtras.Add(Teacherextra);
                            result = new string(
                                Enumerable.Repeat(chars, 8)
                                            .Select(s => s[random.Next(s.Length)])
                                            .ToArray());


                            if (true)
                            {
                               var  prefijo = "P";
                             var    Teachernip = "P" + result;
                                if (db.Teachers.Where(x => x.UserId == userid).Count() == 0)
                                {
                                    Teacher meaestro = new Teacher()
                                    {
                                        TeacherID = Guid.NewGuid(),
                                        UserId = userid,
                                        Nip = Teachernip
                                    };
                                    var School = escuela;

                                    meaestro.Schools.Add(School);
                                   // teacherid = meaestro.TeacherID;
                                    
                                    db.Teachers.Add(meaestro);
                                    grupo.TeacherID=meaestro.TeacherID;


                         var usuarioMembership = Membership.GetUser(nuevousername);
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = nuevousername.ToLower();
                                    db.SaveChanges();


                                    model.is_procesado = true;
                                    model.NipMaestro = meaestro.Nip;
                                    model.NipGrupo = grupo.Nip;
                                    model.UsuarioMaestro = nuevousername.ToLower();
                                    model.ContraseñaMaestro = nuevousername.ToLower();
                                    db.SaveChanges();

                                }

                                db.SaveChanges();
                               

                                #endregion
                                    //Group grupo = db.Groups.Find(model.GroupID);
                               //     nuevoEstudiante.Groups.Add(grupo);


                                    #region Agregar Materias
                                    //////agregar las materias de ese grupo al Alumno
                                    //foreach (var materia in grupo.GroupMaterias)
                                    //{
                                    //    var materiaAlumno = new GroupMateriaAlumno()
                                    //    {
                                    //        Id = Guid.NewGuid(),
                                    //        IdGroupMateria = materia.Id,
                                    //        GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                    //        IsCerrada = false
                                    //    };


                                    //    #region Agregar Parciales
                                    //    /////agregrar los parciales de esa materias a la materia del alumno
                                    //    foreach (var parcialmateria in materia.GroupMateriaParciales)
                                    //    {
                                    //        var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                    //        {
                                    //            Id = Guid.NewGuid(),
                                    //            IdGroupMateriaParcial = parcialmateria.Id,
                                    //            GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()

                                    //        };

                                    //        ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                    //        foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                    //        {
                                    //            var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                    //            {
                                    //                Id = Guid.NewGuid(),
                                    //                Porcentaje = evaluacionparcialmateria.Porcentaje,
                                    //                IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                    //                GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                    //            };
                                    //            ///has evaluaciones?
                                    //            ///
                                    //            ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                    //            ///a la evaluacion del parcial de la materia al alumno
                                    //            foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                    //            {
                                    //                var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                    //                {
                                    //                    Id = Guid.NewGuid(),
                                    //                    IsCheckBox = true,
                                    //                    Cumplio = true,
                                    //                    IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                    //                };
                                    //                evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);

                                    //            }

                                    //            parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                    //        }

                                    //        materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                    //    }
                                    //    #endregion

                                    //    nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                    //}
                                    #endregion

                                   // db.Students.Add(nuevoEstudiante);
                                    db.SaveChanges();

                                    ///
                            }

                                }
                                catch (Exception e)
                                {
                                    var messege = e.Message;
                                    //return View(model);
                                }


                            }
                            #endregion

                        }
                    }
                    else
                    {
                        model.is_procesado = false;
                        db.SaveChanges();
                    }

                } //endforeach

                var registrados = nuevos.Select(x => x.is_procesado == true);
                var total = registrados.Count();
                return Json(total, JsonRequestBehavior.AllowGet);
            }

            


            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Licencia")]
        public JsonResult ProcesarMaestrosNuevosTamaulipasSecundaria()
        {

            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities();
                List<Teacher> teacher = new List<Teacher>();
                IMembershipService MembershipService = new AccountMembershipService();
                int[] ids = new int[]{3, 4,5,6,7,8 };

                var nuevos = db.xxMaestrosTamaulipasSecuntaria_.Where(x => !x.is_procesado);// && x.municipio=="victoria");// && ids.Contains(x.Id));
                    //.OrderBy(x => x.Id).Take(3000);
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                var numeros = "0123456789";
                //var colores = new string[] { "azul", "verde", "rojo", "amarillo" };
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 4)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());

                var numero = nuevos.Count();

                #region foreach Maestrp

                foreach (var model in nuevos.ToList())
                {
                    bool uservalido = false;
                    model.nombre = RemoveDiacritics(model.nombre);
                    if (!string.IsNullOrEmpty(model.apellido1))
                    {
                        model.apellido1 = RemoveDiacritics(model.apellido1);
                    }
                    else
                    {
                        model.apellido1 = "";
                    }


                    if (!string.IsNullOrEmpty(model.apellido2))
                    {
                        model.apellido2 = RemoveDiacritics(model.apellido2);
                    }
                    else
                    {
                        model.apellido2 = "";
                    }



                    var CCT = model.cct.TrimEnd();
                    Guid idlicencia = Guid.Empty;
                    Guid idlicenciausuario = Guid.Empty;
                    // var nipescuela = model.Nip.TrimEnd();
                    var escuela = db.Schools.FirstOrDefault(x => x.ClaveOficial == CCT);


                    if (!string.IsNullOrEmpty(model.nombre))
                    {
                        if (escuela == null)
                        {
                            Mensajes.Add("Escuela no registrada, registre la escuela: " + model.cct + " para poder continuar");
                            model.is_procesado = false;
                            db.SaveChanges();

                        }
                        else
                        {

                            #region Grado
                            var gruponum = 0;
                            switch (Convert.ToInt32(model.grado))
                            {
                                case 1:
                                    gruponum = 7;
                                    break;
                                case 2:
                                    gruponum = 8;
                                    break;
                                case 3:
                                    gruponum = 9;
                                    break;


                                default:
                                    gruponum = 0;
                                    break;
                            }

                            //var gruponum = (model.GRADO == 1) ? "positive" : "negative";

                            //model.GRADO;
                            #endregion // var gruponum = model.grado;
                            var des = model.grupo;
                            var turno = model.turno;
                            var grupo = db.Groups.FirstOrDefault(x => x.SchoolID == escuela.SchoolID
                                && x.IsActivo == true
                                && x.NivelEscolar.Grado == gruponum
                                && x.Description == des
                                && x.Alias == turno.TrimEnd());


                            var Userext = db.UserExtras.FirstOrDefault(x => x.Name == model.nombre
                                && x.LastNameP == model.apellido1
                                && x.LastNameM == model.apellido2
                                );
                            //var useridas = (Userext == null) ? " " : Userext.UserID.ToString() ;  
                         //   var teach = db.Teachers.FirstOrDefault(x=> x.UserId == Guid.Parse(useridas));
                            #region Crear grupo
                            //Crear grupo si no existe
                            if (grupo == null)
                            {

                                var enumnip = new string(
                                    Enumerable.Repeat(chars, 5)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                String nip = "G" + enumnip;

                                grupo = new DistribucionSofia.Models.Group()
                                {
                                    GroupID = Guid.NewGuid(),
                                    SchoolID = escuela.SchoolID,
                                    NivelEscolarID = db.NivelEscolars.First(x => x.Grado == gruponum).LevelID,
                                    //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                    Alias = turno.TrimEnd(),
                                    ViewList=true,
                                    Description = model.grupo,
                                    IsActivo = true,
                                    Generacion = 2018,
                                    StartDate = DateTime.UtcNow,
                                    Nip = nip,
                                    NumParciales = 0,
                                    Activo = true,
                                    IsCerrado = false,
                                    IsOficial = false
                                };
                                grupo.Timezone = db.Configuracions.First().Timezone;
                                grupo.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                                if (grupo.SchoolID != null)
                                {
                                    grupo.Timezone = escuela.Timezone;
                                    grupo.TimezoneOffset = escuela.TimezoneOffset;
                                }
                                //Materias
                                foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                {
                                    grupo.GroupMaterias.Add(new GroupMateria()
                                    {
                                        Id = Guid.NewGuid(),
                                        Materia = materia,
                                        NumParciales = 0,
                                        //Validado = 1, ///validacion extra metadata
                                    });

                                }
                                db.Groups.Add(grupo);
                                db.SaveChanges();
                                Mensajes.Add("Se agrego el grupo: " + gruponum + model.grupo);
                            }
                            #endregion

                            #region Crear alumno
                            if (model.ROWNUMBER==1)
                            	{
                            //var usuario = "";
                            //var nuevousername = "";
                            #region CleanNeame
                            var n = model.nombre.Split(' '); //nombre.First.Split(' ');
                            var name = "";
                            foreach (var n1 in n)
                            {
                                if (n1 != "")
                                {
                                    name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }

                            }
                            var ap = model.apellido1.Split(' ');// nombre.Second.Split(' ');
                            var app = "";
                            foreach (var ap1 in ap)
                            {
                                if (ap1 != "")
                                {
                                    app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }

                            var am = model.apellido2.Split(' ');// nombre.Third.Split(' ');
                            var amm = "";
                            foreach (var am1 in am)
                            {
                                if (am1 != "")
                                {
                                    amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                                    break;
                                }
                            }
                            #endregion

                            var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                            var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                            var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;
                            var nuevousername = string.Empty;
                            var contrasena = string.Empty;

                            while (!uservalido)
                            {
                                result = new string(
                                Enumerable.Repeat(numeros, 4)
                                          .Select(r => r[random.Next(r.Length)])
                                          .ToArray());

                                nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                                nuevousername = nuevousername.RemoveDiacritics();
                                if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                                {
                                    uservalido = true;
                                    contrasena = nuevousername;
                                }

                            }

                            nuevousername = nuevousername.ToLower();

                            MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                            createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@general.com");
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    MembershipUser user = Membership.GetUser(nuevousername, false);
                                    Guid userid = (Guid)user.ProviderUserKey;
                                    aspnet_Users us = db.aspnet_Users.Find(userid);

                                    UserExtra Teacherextra = new UserExtra();
                                    Teacherextra.UserID = userid;
                                    Teacherextra.Name = model.nombre;
                                    Teacherextra.LastNameP = model.apellido1;
                                    Teacherextra.LastNameM = model.apellido2;
                                    Teacherextra.Password = contrasena;
                                    Teacherextra.SexoCode = 1;
                                    Teacherextra.IdEstado = escuela.IdEstado;
                                    Teacherextra.IdPais = escuela.IdPais;
                                    Teacherextra.Correo = "general@general.com";
                                    Teacherextra.EmailPapa = "general@general.com";
                                    Teacherextra.TipoUsuario = 2;
                                    db.UserExtras.Add(Teacherextra);
                                    
                                    result = new string(
                                        Enumerable.Repeat(chars, 8)
                                                    .Select(s => s[random.Next(s.Length)])
                                                    .ToArray());


                                    if (true)
                                    {
                                        
                                        var prefijo = "P";
                                        var Teachernip = "P" + result;
                                        if (db.Teachers.Where(x => x.UserId == userid).Count() == 0)
                                        {
                                            Teacher meaestro = new Teacher()
                                            {
                                                TeacherID = Guid.NewGuid(),
                                                UserId = userid,
                                                Nip = Teachernip
                                            };
                                            var School = escuela;

                                            meaestro.Schools.Add(School);
                                            // teacherid = meaestro.TeacherID;
                                           // meaestro.
                                            db.Teachers.Add(meaestro);
                                            grupo.TeacherID = meaestro.TeacherID;


                                            var usuarioMembership = Membership.GetUser(nuevousername);
                                            Roles.AddUserToRole(nuevousername, "Profesor");
                                            us.UserName = nuevousername;
                                            us.LoweredUserName = nuevousername.ToLower();
                                            us.UserExtra.Password = nuevousername.ToLower();
                                            db.SaveChanges();


                                            model.is_procesado = true;
                                            model.NipMaestro = meaestro.Nip;
                                            model.NipGrupo = grupo.Nip;
                                            model.UsuarioMaestro = nuevousername.ToLower();
                                            model.ContraseñaMaestro = nuevousername.ToLower();
                                            db.SaveChanges();

                                        }
                            #endregion
                                        
                                        db.SaveChanges();


                          
                                        //Group grupo = db.Groups.Find(model.GroupID);
                                        //     nuevoEstudiante.Groups.Add(grupo);


                                        #region Agregar Materias
                                        //////agregar las materias de ese grupo al Alumno
                                        //foreach (var materia in grupo.GroupMaterias)
                                        //{
                                        //    var materiaAlumno = new GroupMateriaAlumno()
                                        //    {
                                        //        Id = Guid.NewGuid(),
                                        //        IdGroupMateria = materia.Id,
                                        //        GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                        //        IsCerrada = false
                                        //    };


                                        //    #region Agregar Parciales
                                        //    /////agregrar los parciales de esa materias a la materia del alumno
                                        //    foreach (var parcialmateria in materia.GroupMateriaParciales)
                                        //    {
                                        //        var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                        //        {
                                        //            Id = Guid.NewGuid(),
                                        //            IdGroupMateriaParcial = parcialmateria.Id,
                                        //            GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()

                                        //        };

                                        //        ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                        //        foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                        //        {
                                        //            var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                        //            {
                                        //                Id = Guid.NewGuid(),
                                        //                Porcentaje = evaluacionparcialmateria.Porcentaje,
                                        //                IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                        //                GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                        //            };
                                        //            ///has evaluaciones?
                                        //            ///
                                        //            ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                        //            ///a la evaluacion del parcial de la materia al alumno
                                        //            foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                        //            {
                                        //                var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                        //                {
                                        //                    Id = Guid.NewGuid(),
                                        //                    IsCheckBox = true,
                                        //                    Cumplio = true,
                                        //                    IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                        //                };
                                        //                evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);

                                        //            }

                                        //            parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                        //        }

                                        //        materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                        //    }
                                        //    #endregion

                                        //    nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                        //}
                                        #endregion

                                        // db.Students.Add(nuevoEstudiante);
                                        db.SaveChanges();

                                        ///
                                    }

                                }
                                catch (Exception e)
                                {
                                    var messege = e.Message;
                                    //return View(model);
                                }
                            }

                                }
                            else
                            {
                                var Userext2 = db.UserExtras.FirstOrDefault(x => x.Name == model.nombre
                                    && x.LastNameP == model.apellido1
                                    && x.LastNameM == model.apellido2
                                    );
                                var teach = db.Teachers.FirstOrDefault(x => x.UserId == Userext2.UserID);
                                model.is_procesado = true;
                                //model.NipMaestro = meaestro.Nip;
                                model.NipGrupo = grupo.Nip;
                                grupo.TeacherID = teach.TeacherID;
                                // model.UsuarioMaestro = nuevousername.ToLower();
                                // model.ContraseñaMaestro = nuevousername.ToLower();
                                db.SaveChanges();
                            }
                           
                #endregion

                        }
                    }
                    else
                    {
                        model.is_procesado = false;
                        db.SaveChanges();
                    }

                } //endforeach

                var registrados = nuevos.Select(x => x.is_procesado == true);
                var total = registrados.Count();
                return Json(total, JsonRequestBehavior.AllowGet);
            }




            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                var r = e.Message;
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public static List<ViewModel_excel_EscuelaNueva> ProcesarExcel(Entities db, List<ViewModel_excel_EscuelaNueva> Escuelas)
        {
            List<ViewModel_excel_EscuelaNueva> lista = new List<ViewModel_excel_EscuelaNueva>();
            
            var contando = 0;

            foreach(var model in Escuelas)
            {
                try
                {
                    var distribuidor = db.Distribuidors.FirstOrDefault(x => x.Id == model.DistribuidorID);
                    var cultura = distribuidor.Pai.LenguajeCodigoes.FirstOrDefault();
                    var idusuario = Guid.Empty;
                    
                    if(db.Schools.Count(x=>x.ClaveOficial==model.ClaveOficial) == 0)
                    {

                    

                    //Escuela
                    School escuelanew = new School();

                    escuelanew.SchoolID = Guid.NewGuid();
                    escuelanew.IdUser = distribuidor.Id;
                    escuelanew.DistribuidorID = distribuidor.Id;
                    escuelanew.FechaRegistro = DateTime.UtcNow;
                    if (model.Timezone != null)
                    {
                        escuelanew.TimezoneOffset = TimeZoneInfo.FindSystemTimeZoneById(model.Timezone).BaseUtcOffset.Hours;
                    }
                    else
                    {
                        escuelanew.Timezone = db.Configuracions.First().Timezone;
                        escuelanew.TimezoneOffset = db.Configuracions.First().TimezoneOffset;
                    }

                    var estadoli = db.Estadoes.Find(model.IdEstado);

                    escuelanew.Name = model.Name;
                    escuelanew.Type = model.Dependencia;
                    escuelanew.Scholar_Level = model.Scholar_Level;
                    escuelanew.City = model.City;
                    escuelanew.Address = model.Address;
                    escuelanew.Telephone = model.Telephone;
                    escuelanew.ClaveOficial = model.ClaveOficial;
                    escuelanew.IsActive = true;
                    escuelanew.Dependencia = model.Dependencia;
                    escuelanew.Detalle = " ";
                    escuelanew.Localidad = model.City;
                    escuelanew.Turno = model.Turno;
                    escuelanew.EstadoCode = estadoli.Codigo;
                    escuelanew.Province = estadoli.Nombre;
                    escuelanew.Country = estadoli.Pai.Nombre;
                    escuelanew.IdEstado = model.IdEstado;
                    escuelanew.IdPais = model.IdPais;
                    escuelanew.IsDistribuidor = false;
                    escuelanew.VinculacionExternaType = 0;
                    escuelanew.NumAlum = 0;

                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    bool nipvalido = false;
                    String nip = "E";
                    var length = 5;
                    var loop = 100000;
                    var cont = 0;
                    var result = new string(
                        Enumerable.Repeat(chars, length)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                    while (!nipvalido)
                    {
                        cont++;
                        if (cont > loop)
                        {
                            result = new string(
                            Enumerable.Repeat(chars, length + (cont / loop))
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                        }
                        nip = "E" + result;
                        if (db.Schools.Count(x => x.Nip == nip) == 0)
                            nipvalido = true;
                    }
                    escuelanew.Nip = nip;

                    db.Schools.Add(escuelanew);
                    db.SaveChanges();

                    //Director
                    IMembershipService MembershipService = new AccountMembershipService();
                    var nuevousername = string.Empty;
                    var contrasena = string.Empty;

                    if (model.UsuarioDirector != "")
                    {
                        nuevousername = model.UsuarioDirector;
                        if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                        {
                            contrasena = model.ContraseñaDirector;
                        }
                        else
                        {
                            nuevousername = model.ClaveOficial;
                            contrasena = "123456";
                        }
                    }
                    else
                    {
                            nuevousername = model.ClaveOficial;
                            contrasena = "123456";
                    }
                    
                    nuevousername = nuevousername.ToLower();
                    
                    MembershipCreateStatus createStatus = MembershipService.CreateUser(nuevousername, contrasena, model.CorreoDir);
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Roles.AddUserToRole(nuevousername, "Director");

                        MembershipUser user = Membership.GetUser(nuevousername, false);
                        Guid useridDir = (Guid)user.ProviderUserKey;

                        DateTime fecha = DateTime.MinValue;
                        fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                        aspnet_Users us = db.aspnet_Users.Find(useridDir);
                        us.UserExtra = new UserExtra();
                        us.UserExtra.Birthday = fecha;
                        us.UserExtra.Name = model.NombreDir;
                        us.UserExtra.LastNameP = model.ApellidoP;
                        us.UserExtra.LastNameM = model.ApellidoM;
                        us.UserExtra.Password = contrasena;
                        us.UserExtra.SexoCode = (int)model.Sexo;
                        us.UserExtra.Correo = model.CorreoDir;
                        us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                        Director nuevoDirec = new Director()
                        {
                            DirectorID = Guid.NewGuid(),
                            UserID = useridDir,

                        };
                        idusuario = nuevoDirec.UserID;

                        var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var random1 = new Random();
                        bool nipvalido3 = false;
                        String nip3 = "DIR";
                        var length3 = 5;
                        var loop3 = 100000;
                        var cont3 = 0;
                        var result1 = new string(
                            Enumerable.Repeat(chars1, length3)
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                        while (!nipvalido3)
                        {
                            cont3++;
                            if (cont3 > loop3)
                            {
                                result1 = new string(
                                Enumerable.Repeat(chars1, length3 + (cont3 / loop3))
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                            }
                            nip3 = "DIR" + result1;
                            if (db.Directors.Count(x => x.Nip == nip3) == 0)
                                nipvalido3 = true;
                        }
                        nuevoDirec.Nip = nip3;

                        db.Directors.Add(nuevoDirec);
                        db.SaveChanges();

                        escuelanew.IdDirector = nuevoDirec.DirectorID;
                        
                        ///
                        var usuarioMembership = Membership.GetUser(nuevousername);
                        us.UserName = nuevousername;
                        us.LoweredUserName = nuevousername.ToLower();
                        us.UserExtra.Password = contrasena;
                        
                        db.SaveChanges();
                        model.UsuarioDirector = nuevousername;
                        model.ContraseñaDirector = contrasena;
                    }

                    //Licencia
                    if (model.Scholar_Level == 2)
                    {
                        model.IdPlan = new Guid("ccd5e13a-640c-4e07-bea9-ee2d17e2bbe4");
                    }
                    else if (model.Scholar_Level == 4)
                    {
                        model.IdPlan = new Guid("c433e314-9998-43f8-97d5-427f2df17491");
                    }
                    else
                    {
                        model.IdPlan = new Guid("BE7207F6-0915-4439-A07C-9BDB305A4C53");
                    }

                    
                    var tipo = "Licencia";
                    model.Periodo = 8;

                    var plan = db.Plans.Find(model.IdPlan);
                    var subtotal = model.Cantidad * plan.Costo;
                    var total = (subtotal * 1.16);
                    var iva = total - subtotal;

                    Licencia lic = new Licencia()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdPlan = plan.IdPlan,
                        NumAlumnos = model.Cantidad,
                        FechaRegistro = DateTime.UtcNow,
                        FechaFinal = new DateTime(2019,07,31),
                        FechaAprobacion = DateTime.UtcNow,
                        FechaInicio = DateTime.UtcNow,
                        IsAprobado = true,
                        IsUsing = false,
                        IsPrueba = false,
                        StandBy = false,
                        IsActivo = true,
                        CostoAlumno = plan.Costo,
                        CostoAlumnoPlan = plan.Costo,
                        DuracionPlan = plan.Duracion,
                        NombrePlan = plan.Nombre,
                        DescripcionPlan = plan.Descripcion,
                        CicloEscolar = 2018
                    };
                    db.Licencias.Add(lic);
                    db.SaveChanges();

                    LicenciaUsuario newlic = new LicenciaUsuario()
                    {
                        Id = Guid.NewGuid(),
                        IdUsuario = idusuario,
                        IdLicencia = lic.Id,
                        FechaRegistro = DateTime.Now,
                        IsPrueba = lic.IsPrueba
                    };
                    db.LicenciaUsuarios.Add(newlic);
                    db.SaveChanges();

                    OrdenCompra ordnew = new OrdenCompra()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdDistribuidor = distribuidor.Id,
                        FechaRegistro = DateTime.Now,
                        IdLicencia = lic.Id,
                        SubTotal = subtotal,
                        IVA = iva,
                        Total = total,
                        Estatus = 1,
                        Facturado = false,
                        ReqFactura = false
                    };
                    db.OrdenCompras.Add(ordnew);
                    db.SaveChanges();

                    //OdenCompraProducto
                    OrdenCompraProducto prodnew = new OrdenCompraProducto()
                    {
                        Id = Guid.NewGuid(),
                        IdOrdenCompra = ordnew.Id,
                        IdPlan = plan.IdPlan,
                        Cantidad = model.Cantidad,
                        CostoUnitario = plan.Costo,
                        Concepto = plan.Nombre,
                        Total = subtotal,
                        Unidad = tipo,
                        FechaRegistro = DateTime.Now
                    };
                    db.OrdenCompraProductoes.Add(prodnew);

                    db.SaveChanges();
                    contando++;
                    model.Conteo = contando;
                    lista.Add(model);
                    }
                    else
                    {
                        model.UsuarioDirector = "Escuela ya registrada";
                        model.ContraseñaDirector = "";
                        lista.Add(model);
                    }
                }
                catch
                {

                }
            }
            return lista;
        }

        public String RemoveDiacritics(String s)
        {
            String normalizedString = s.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < normalizedString.Length; i++)
            {
                Char c = normalizedString[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        public ActionResult RegistroMTMaestros()
        {
            //Registro masivo drector
            List<ViewModel_excel_Maestros> Maestros = new List<ViewModel_excel_Maestros>();
            List<ViewModel_excel_Maestros> MaestrosRegistrados = new List<ViewModel_excel_Maestros>();


            //Encontrar el archivo
            var FileName = Guid.NewGuid().ToString() + ".xlsx";
            string somePath = @"C:\ListasMaestrosT\Maestros.xlxs";
            string filename = "Maestros.xlxs";
            string filenamePath = @"C:\ListasMaestrosT\Maestros.xlxs";//System.IO.Path.Combine(somePath, filename);

            var existingFile = new FileInfo(@"C:\ListasMaestrosT\Maestros.xlsx");
            using (var package = new ExcelPackage(existingFile))
            {
                ExcelWorkbook workBook = package.Workbook;
                if (workBook != null)
                {
                    if (workBook.Worksheets.Count > 0)
                    {
                        // Get the first worksheet
                        ExcelWorksheet currentWorksheet = workBook.Worksheets.First();
                        int totalRows = currentWorksheet.Dimension.End.Row;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            var nombre = (currentWorksheet.Cells[i, 3].Value != null) ? currentWorksheet.Cells[i, 3].Value.ToString() : "";
                            var apellidoP = (currentWorksheet.Cells[i, 1].Value != null) ? currentWorksheet.Cells[i, 1].Value.ToString() : "";
                            var apellidoM = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 2].Value.ToString() : "";
                            var grado = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 4].Value.ToString() : "";
                            var grupo = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 5].Value.ToString() : "";
                            var nombreEscuela = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 6].Value.ToString() : "";
                            var claveoficial = (currentWorksheet.Cells[i, 2].Value != null) ? currentWorksheet.Cells[i, 7].Value.ToString() : "";

                            if (!string.IsNullOrEmpty(nombre))
                            {

                                Maestros.Add(
                                         new ViewModel_excel_Maestros
                                         {
                                             Nombre = nombre,
                                             ApellidoP = apellidoP,
                                             ApellidoM = apellidoM,
                                             Grado = grado,
                                             Grupo = grupo,
                                             NombreEscuela = nombreEscuela,
                                             ClaveOficial = claveoficial
                                         });


                            }
                        }
                    }
                }
            }



            #region Registro

            foreach (var Maestro in Maestros)
            {
                Models.Student Student = new Models.Student();
                Models.Group Group1 = new Models.Group();
                Guid idlicenciausuario = Guid.Empty;
                Guid teacherid;
                Guid idlicencia = Guid.Empty;
                int cantidadAlumnos = 1;
                int levelID = 0;
                int lvl = Int32.Parse(Maestro.Grado);
                Models.Group groupnew;
                String Teachernip = "";
                var nivel = db.NivelEscolars.FirstOrDefault(x => x.Grado == lvl);
                levelID = nivel.LevelID;

                #region getSchoolID
                string Name = Maestro.NombreEscuela;
                string clave = Maestro.ClaveOficial;
                var SchoolID = db.Schools.Where(x => x.ClaveOficial == clave).Select(x => new
                {
                    id = x.SchoolID

                }).FirstOrDefault();
                #endregion


                if (true)
                {
                    IMembershipService MembershipService = new AccountMembershipService();
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    bool uservalido = false;
                    string nuevousername = "";
                    string contrasena = "";
                    var numeros = "0123456789";
                    var random = new Random();
                    var result = new string(
                    Enumerable.Repeat(numeros, 3)
                                         .Select(r => r[random.Next(r.Length)])
                                         .ToArray());

                    #region CleanNeame
                    var n = Maestro.Nombre.Split(' ');
                    var name = "";
                    foreach (var n1 in n)
                    {
                        if (n1 != "")
                        {
                            name = Regex.Replace(n1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }

                    }
                    var ap = Maestro.ApellidoP.Split(' ');
                    var app = "";
                    foreach (var ap1 in ap)
                    {
                        if (ap1 != "")
                        {
                            app = Regex.Replace(ap1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }
                    }

                    var am = Maestro.ApellidoM.Split(' ');
                    var amm = "";
                    foreach (var am1 in am)
                    {
                        if (am1 != "")
                        {
                            amm = Regex.Replace(am1, "[^a-zA-Z][^áéíîóúüñÁÉÍÓÚÜÑ]", "", RegexOptions.None).Trim();
                            break;
                        }
                    }
                    #endregion


                    var PrimerNombre = (name.Length >= 8) ? name.Substring(0, 8) : name;
                    var ApellidoPaterno = (app.Length >= 8) ? app.Substring(0, 8) : app;
                    var ApellidoMaterno = (amm.Length >= 8) ? amm.Substring(0, 8) : amm;

                    while (!uservalido)
                    {
                        result = new string(
                        Enumerable.Repeat(numeros, 4)
                                  .Select(r => r[random.Next(r.Length)])
                                  .ToArray());

                        nuevousername = PrimerNombre + "." + ApellidoPaterno + result;
                        nuevousername = nuevousername.RemoveDiacritics();
                        if (db.aspnet_Users.Count(x => x.UserName == nuevousername) == 0)
                        {
                            uservalido = true;
                            contrasena = nuevousername;
                        }

                    }

                    MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                    createStatus = MembershipService.CreateUser(nuevousername, contrasena.ToLower(), "general@sofiaxt.com");

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        try
                        {
                            #region Teacher
                            MembershipUser user = Membership.GetUser(nuevousername, false);
                            Guid userid = (Guid)user.ProviderUserKey;
                            aspnet_Users us = db.aspnet_Users.Find(userid);

                            try
                            {
                                UserExtra Teacherextra = new UserExtra();
                                Teacherextra.UserID = userid;
                                Teacherextra.Name = Maestro.Nombre;
                                Teacherextra.LastNameP = Maestro.ApellidoP;
                                Teacherextra.LastNameM = Maestro.ApellidoM;
                                Teacherextra.Password = contrasena;
                                Teacherextra.SexoCode = 1;
                                Teacherextra.Correo = "general@sofiaxt.com";
                                Teacherextra.EmailPapa = "general@sofiaxt.com";
                                Teacherextra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Profesor;
                                db.UserExtras.Add(Teacherextra);
                                result = new string(
                                    Enumerable.Repeat(chars, 8)
                                                .Select(s => s[random.Next(s.Length)])
                                                .ToArray());

                                if (true)
                                {
                                    // prefijo = "P";
                                    Teachernip = "P" + result;
                                    if (db.Teachers.Where(x => x.UserId == userid).Count() == 0)
                                    {
                                        Teacher teacher = new Teacher()
                                        {
                                            TeacherID = Guid.NewGuid(),
                                            UserId = userid,
                                            Nip = Teachernip
                                        };
                                        var School = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();

                                        teacher.Schools.Add(School);
                                        teacherid = teacher.TeacherID;

                                        db.Teachers.Add(teacher);
                                    }

                                    if (db.SaveChanges() > 0)
                                    {
                                        if (true) Roles.AddUserToRole(nuevousername, "Profesor");

                                        Maestro.Usuario = nuevousername;
                                        Maestro.Contraseña = nuevousername;
                                        FormsAuthentication.SetAuthCookie(us.UserName, true /* createPersistentCookie */);
                                        //Email.EnviarEmailConfirmacion(user, nuevousername, us.UserExtra);

                                        var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == SchoolID.id).ToList();

                                        var licvigente = licenciasescuelas.Where(x => x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                        if (licvigente != null)
                                        {
                                            LicenciaUsuario newlic = new LicenciaUsuario()
                                            {
                                                Id = Guid.NewGuid(),
                                                IdLicencia = licvigente.Id,
                                                IdUsuario = userid,
                                                FechaRegistro = DateTime.Now,
                                                IsActivo = true,
                                                IsPrueba = licvigente.IsPrueba
                                            };

                                            db.LicenciaUsuarios.Add(newlic);

                                        }
                                        db.SaveChanges();
                                        MaestrosRegistrados.Add(Maestro);
                                        //
                                    }
                            #endregion

                                    #region Grupo
                                    var errores = new List<string>();
                                    if (db.Teachers.Where(x => x.UserId == userid).Count() != 0)
                                    {
                                        //group.NumPariales = 0;
                                        var Teacher = db.Teachers.Where(x => x.UserId == userid).FirstOrDefault();
                                        try
                                        {
                                            var School = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();

                                            groupnew = new Models.Group()
                                            {
                                                GroupID = Guid.NewGuid(),
                                                NivelEscolarID = levelID,
                                                SchoolID = School.SchoolID,
                                                Description = Maestro.Grupo,
                                                Alias = "",
                                                Generacion = DateTime.Now.Year,
                                                Timezone = db.Configuracions.First().Timezone,
                                                TimezoneOffset = db.Configuracions.First().TimezoneOffset,
                                                IsActivo = true,
                                                Activo = true,
                                                IsCerrado = true,
                                                StartDate = DateTime.UtcNow,
                                                TeacherID = Teacher.TeacherID,
                                                NumParciales = 0,
                                                FechaRegistro = DateTime.UtcNow,
                                                ViewList = true,
                                            };

                                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                            var random2 = new Random();
                                            bool nipvalido = false;
                                            String nip2 = "G";
                                            var length = 5;
                                            var loop = 100000;
                                            var cont = 0;
                                            var result2 = new string(
                                                Enumerable.Repeat(chars2, length)
                                                          .Select(s => s[random2.Next(s.Length)])
                                                          .ToArray());
                                            while (!nipvalido)
                                            {
                                                cont++;
                                                if (cont > loop)
                                                {
                                                    result2 = new string(
                                                    Enumerable.Repeat(chars2, length + (cont / loop))
                                                          .Select(s => s[random2.Next(s.Length)])
                                                          .ToArray());
                                                }
                                                nip2 = "G" + result2;
                                                if (db.Groups.Count(x => x.Nip == nip2) == 0)
                                                    nipvalido = true;
                                            }
                                            groupnew.Nip = result2;

                                            foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                                            {
                                                groupnew.GroupMaterias.Add(new GroupMateria()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    Materia = materia,
                                                    NumParciales = 0,
                                                    //Validado = 1, ///validacion extra metadata
                                                });

                                            }
                                            foreach (var libro in db.Libroes.Where(x => x.IdNivelEscolar == groupnew.NivelEscolarID))
                                            {
                                                groupnew.Libroes.Add(libro);
                                            }

                                            db.Groups.Add(groupnew);
                                            db.SaveChanges();
                                            Group1 = groupnew;
                                        }
                                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                                        {
                                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                                            {
                                                foreach (var validationError in validationErrors.ValidationErrors)
                                                {
                                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                                }
                                            }
                                        }
                                    }                                  //{



                                    #endregion
                                    #region Alumnos
                                    //    try
                                    //    {
                                    //        var School1 = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();
                                    //        List<Models.Student> Alumnos = new List<Models.Student>();
                                    //        do
                                    //        {
                                    //            //COMO DARLE USUARIO FACIL AL ALUMNO
                                    //            var userAlumno = Teachernip +"AlumnoT" + Alumnos.Count() + 1+ "";
                                    //            var NombreAlumno = "AlumnoT" + Alumnos.Count() + 1 + "";
                                    //             var chars3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //            var random3 = new Random();
                                    //            var numeros = "0123456789";

                                    //            var result3 = new string(
                                    //                Enumerable.Repeat(chars3, 10)
                                    //                          .Select(s => s[random3.Next(s.Length)])
                                    //                          .ToArray());
                                    //            var contrasena = "123456";
                                    //            if (true)
                                    //            {
                                    //                var usuariotemporal = userAlumno;//"usuariotemporal" + result3;
                                    //                MembershipCreateStatus createStatus3 = MembershipService.CreateUser(usuariotemporal, "123456", "prueba@prueba.com");

                                    //                if (createStatus3 == MembershipCreateStatus.Success)
                                    //                {

                                    //                    //FormsService.SignIn(model.UserName, false /* createPersistentCookie */);

                                    //                    MembershipUser user3 = Membership.GetUser(usuariotemporal, false);
                                    //                    Guid userid3 = (Guid)user3.ProviderUserKey;
                                    //                    Roles.AddUserToRole(usuariotemporal, "Alumno");
                                    //                    aspnet_Users us3 = db.aspnet_Users.Find(userid3);
                                    //                    us3.UserExtra = new UserExtra();

                                    //                    var School = db.Schools.Where(x => x.SchoolID == SchoolID.id).FirstOrDefault();
                                    //                    us3.UserExtra.Name = NombreAlumno;
                                    //                    us3.UserExtra.Password = contrasena;
                                    //                    us3.UserExtra.SexoCode = 1;
                                    //                    us3.UserExtra.TipoUsuario = (int)EnumTipoUsuario.Alumno;
                                    //                    us3.UserExtra.Ciudad = School.City;
                                    //                    if (Group1.SchoolID.HasValue)
                                    //                    {
                                    //                    us3.UserExtra.Estado = School.EstadoCode;
                                    //                    us3.UserExtra.IdEstado = School.IdEstado;
                                    //                    us3.UserExtra.IdPais = School.IdPais;
                                    //                    //var licenciasescuelas = db.Licencias.Where(x => x.IdEscuela == Group1.SchoolID && x.IsAprobado == true).ToList();
                                    //                    //var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == Group1.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                    //                    //if (licvigente != null)
                                    //                    //{
                                    //                    //    LicenciaUsuario newlic = new LicenciaUsuario()
                                    //                    //    {
                                    //                    //        Id = Guid.NewGuid(),
                                    //                    //        IdLicencia = licvigente.Id,
                                    //                    //        IdUsuario = userid,
                                    //                    //        FechaRegistro = DateTime.Now,
                                    //                    //        IsActivo = true,
                                    //                    //        IsPrueba = licvigente.IsPrueba
                                    //                    //    };

                                    //                    //    db.LicenciaUsuarios.Add(newlic);
                                    //                    //    idlicencia = licvigente.Id;
                                    //                    //    idlicenciausuario = newlic.IdUsuario;
                                    //                    //}
                                    //}
                                    //else if (Group1.TeacherID.HasValue)
                                    //{
                                    //    var licenciasescuelas = db.Licencias.Where(x => x.IdProfesor == Group1.TeacherID).ToList();
                                    //    var licvigente = licenciasescuelas.Where(x => x.Plan.NivelEscolar == Group1.NivelEscolar.NivelEscolarCode && x.IsAprobado == true && x.FechaFinal > DateTime.Now).FirstOrDefault();
                                    //    if (licvigente != null)
                                    //    {
                                    //        LicenciaUsuario newlic = new LicenciaUsuario()
                                    //        {
                                    //            Id = Guid.NewGuid(),
                                    //            IdLicencia = licvigente.Id,
                                    //            IdUsuario = userid,
                                    //            FechaRegistro = DateTime.Now,
                                    //            IsActivo = true,
                                    //            IsPrueba = licvigente.IsPrueba
                                    //        };

                                    //        db.LicenciaUsuarios.Add(newlic);
                                    //        idlicencia = licvigente.Id;
                                    //        idlicenciausuario = newlic.IdUsuario;
                                    //    }
                                    //}

                                    //                    result3 = new string(
                                    //                        Enumerable.Repeat(chars3, 8)
                                    //                                  .Select(s => s[random3.Next(s.Length)])
                                    //                                  .ToArray());

                                    //                    String nip3 = "A" + result3;
                                    //                    Student nuevoEstudiante = new Student()
                                    //                    {
                                    //                        StudentID = Guid.NewGuid(),
                                    //                        UserID = userid3,
                                    //                        EjerciciosCorrectosConsecutivos = 0,
                                    //                        EjerciciosCompletados = 0,
                                    //                        EjerciciosCorrectos = 0,
                                    //                        EjerciciosTiempoMenor = 0,
                                    //                        TiempoReal = 0,
                                    //                        TiempoPromedio = 0,
                                    //                        PuntosTotales = 0,
                                    //                        Creditos = 0,
                                    //                        NivelPuntos = 0,
                                    //                        Nip = nip3,
                                    //                        GroupMateriaAlumnoes = new List<GroupMateriaAlumno>(),
                                    //                        IdUserCreado = (Guid)Membership.GetUser().ProviderUserKey,
                                    //                    };

                                    //                    nuevoEstudiante.Groups.Add(Group1);

                                    //                    ////agregar las materias de ese grupo al Alumno
                                    //                    foreach (var materia in Group1.GroupMaterias)
                                    //                    {
                                    //                        var materiaAlumno = new GroupMateriaAlumno()
                                    //                        {
                                    //                            Id = Guid.NewGuid(),
                                    //                            IdGroupMateria = materia.Id,
                                    //                            GroupMateriaParcialesAlumnos = new List<GroupMateriaParcialesAlumno>(),
                                    //                            IsCerrada = false
                                    //                        };
                                    //                        /////agregrar los parciales de esa materias a la materia del alumno
                                    //                        foreach (var parcialmateria in materia.GroupMateriaParciales)
                                    //                        {
                                    //                            var parcialMateriaAlumno = new GroupMateriaParcialesAlumno()
                                    //                            {
                                    //                                Id = Guid.NewGuid(),
                                    //                                IdGroupMateriaParcial = parcialmateria.Id,
                                    //                                GroupMateriaParcialesCalificaciones = new List<GroupMateriaParcialesCalificacione>()
                                    //                            };
                                    //                            ////agregrar las evaluaciones del parcial de la materia - al parcial de la materia del alumno
                                    //                            foreach (var evaluacionparcialmateria in parcialmateria.GroupMateriaParcialesEvaluacions)
                                    //                            {
                                    //                                var evaluacionParcialMateria = new GroupMateriaParcialesCalificacione()
                                    //                                {
                                    //                                    Id = Guid.NewGuid(),
                                    //                                    Porcentaje = evaluacionparcialmateria.Porcentaje,
                                    //                                    IdGroupMateriaParcialesEvaluacion = evaluacionparcialmateria.Id,
                                    //                                    GroupMateriaParcialesCalificacionesSubEvals = new List<GroupMateriaParcialesCalificacionesSubEval>()
                                    //                                };
                                    //                                ///agregrar las subevaluaciones de las evaluaciones del parcial de la materia - 
                                    //                                ///a la evaluacion del parcial de la materia al alumno
                                    //                                foreach (var subevaluacionparcialmateria in evaluacionparcialmateria.GroupMateriaParcialesEvaluacionSubs)
                                    //                                {
                                    //                                    var subEvaluacionParcialMateria = new GroupMateriaParcialesCalificacionesSubEval()
                                    //                                    {
                                    //                                        Id = Guid.NewGuid(),
                                    //                                        IsCheckBox = true,
                                    //                                        Cumplio = true,
                                    //                                        IdGroupMateriaParcialesEvaluacionSub = subevaluacionparcialmateria.Id
                                    //                                    };
                                    //                                    evaluacionParcialMateria.GroupMateriaParcialesCalificacionesSubEvals.Add(subEvaluacionParcialMateria);
                                    //                                }
                                    //                                parcialMateriaAlumno.GroupMateriaParcialesCalificaciones.Add(evaluacionParcialMateria);
                                    //                            }
                                    //                            materiaAlumno.GroupMateriaParcialesAlumnos.Add(parcialMateriaAlumno);

                                    //                        }
                                    //                        nuevoEstudiante.GroupMateriaAlumnoes.Add(materiaAlumno);
                                    //                    }

                                    //                    Alumnos.Add(nuevoEstudiante);
                                    //                    db.Students.Add(nuevoEstudiante);
                                    //                    db.SaveChanges();
                                    //                }

                                    //            }
                                    //        } while (Alumnos.Count < cantidadAlumnos);
                                    //    }
                                    //    catch (Exception e)
                                    //    {
                                    //        string ex = e.Message; 

                                    //    }
                                    #endregion
                                }
                                else
                                {
                                    Models.Error er = new Models.Error()
                                    {
                                        Id = Guid.NewGuid(),
                                        Descripcion = "No se guardo UserAndExtra, ni Student, ni Teacher ni Tutor",
                                        // Detalle = model.TipoUsuario + ,
                                    };
                                    db.Errors.Add(er);
                                    db.SaveChanges();
                                }

                                #region coment
                                //if (model.TipoUsuario == Models.Enum.EnumTipoUsuario.Tutor)
                                //{
                                //    prefijo = "T";
                                //    Tutor tutor = new Tutor()
                                //    {
                                //        TutorID = Guid.NewGuid(),
                                //        UserID = userid
                                //    };
                                //    db.Tutors.Add(tutor);

                                //}
                                #endregion
                            }

                            catch (SmtpException smtpe)
                            {
                                Models.Error e = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    // Detalle = model.Usuario,
                                    Descripcion = "Envio de correos"
                                };
                                return RedirectToAction("confirmation");
                            }
                            catch (DbEntityValidationException dbEx)
                            {
                                var err = "";
                                foreach (var validationErrors in dbEx.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        err += "Property: " + validationError.PropertyName + " - Error: " + validationError.ErrorMessage + ". ";
                                    }
                                }
                                Models.Error er = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    Descripcion = err,
                                    // Detalle = model.Usuario,
                                };
                                db.Errors.Add(er);
                                db.SaveChanges();

                            }
                            catch (Exception e)
                            {
                                var err = e.Message;
                                Models.Error er = new Models.Error()
                                {
                                    Id = Guid.NewGuid(),
                                    Descripcion = e.InnerException.Message,
                                    //Detalle = model.Usuario,
                                };
                                db.Errors.Add(er);
                                db.SaveChanges();
                            }
                        }

                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                }
                            }
                        }
                    }

                }



            }
            #endregion


            #region DescargaAltas


            var fi = new FileInfo(@"C:\ListasMaestrosT\ListaProfesores.xlsx");
            using (var pi = new ExcelPackage(fi))
            {
                var ws = pi.Workbook.Worksheets["Lista"];
                //////.Cells[2, 10] = “ fila10 columna 2”;
                //////Escuela
                //ws.Cells[4, 4].Value = (s != null) ? s.Name : "";
                ////Fecha 
                //ws.Cells[3, 4].Value = System.DateTime.Now.ToShortDateString();
                //ws.Cells[3, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                int i = 10;
                foreach (var teacher in MaestrosRegistrados)
                {


                    ////Nombre Profesor
                    ws.Cells[i, 2].Value = teacher.Nombre;
                    //FormatoCelda
                    ws.Cells[i, 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Usuario Profesor
                    ws.Cells[i, 3].Value = teacher.Usuario;
                    //FormatoCelda
                    ws.Cells[i, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //Contraseña Profesor
                    ws.Cells[i, 4].Value = teacher.Contraseña;
                    //FormatoCelda
                    ws.Cells[i, 4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Grupos  Profesor
                    ws.Cells[i, 5].Value = teacher.Grupo + " " + teacher.Grado + " ";
                    ws.Cells[i, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //FormatoCelda
                    ws.Cells[i, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    i++;
                }



                Response.ClearContent();
                pi.Save();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=  Maestros .xlsx");
                Response.ContentType = "application/ms-excel";

                Response.Flush();
                Response.End();

            #endregion

                return RedirectToAction("Prueba", "Wizard", null);
                // return View();
            }
            //{


        }

        public ActionResult AuditoriaLicencia()
        {
            ViewBag.Distribuidores = db.Distribuidors.OrderBy(x => x.Estado1.Codigo).ToList();
            ViewBag.Estados = db.Estadoes.Where(x=>x.IdPais == new Guid("38F56A6B-A667-4A6C-BEAD-C16F0A92317E")).OrderBy(x => x.Codigo).ToList();

            return View();
        }

        public ActionResult CorreLicencia(int? page, string cct)
        {
            ViewBag.CCTFilter = cct;

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Schools.Where(x => x.Licencias.Count() > 0).AsQueryable();

            if(!string.IsNullOrEmpty(cct))
            {
                v = v.Where(x => x.ClaveOficial.Contains(cct));
            }

            return View(v.OrderBy(x => x.Name).ThenBy(x => x.ClaveOficial).ThenBy(x => x.Dependencia).ThenBy(x => x.Scholar_Level).ToList().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult LicenciasTamaulipas()
        {
            ViewBag.Ciudades = db.Schools.Where(x => x.IdEstado == new Guid("C4262843-AACD-48BD-929F-7D594D738CB9")).Select(x => x.City).Distinct();
            
            return View();
        }

        public JsonResult _AgregarCiudad(string Ciudad)
        {
            var escuelas = db.Schools.Where(x => x.DistribuidorID == new Guid("36d80362-ddf1-413f-93ed-169330fdbb5a") && x.City == Ciudad).ToList();
            
            
            foreach(var item in escuelas)
            {
                
            }
            
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult _AgregarLicCity(string Ciudad)
        {
            List<ViewModel_LicenciaTamaulipas> listaRetorno = new List<ViewModel_LicenciaTamaulipas>();

            var ccts = db.sofia_LicenciaEscuelasG().ToList();


            List<School> escuelas = new List<School>();

            foreach (var cct in ccts)
            {
                escuelas.Add(db.Schools.Where(x=> x.ClaveOficial== cct.ClaveOficial).FirstOrDefault());
            }
          
                  //   var escuelas = db.Schools.Where(x => x.DistribuidorID == new Guid("36d80362-ddf1-413f-93ed-169330fdbb5a") && x.City.Contains(Ciudad) /*x.IdEstado == new Guid("C4262843-AACD-48BD-929F-7D594D738CB9")*/).ToList();


            //var escuelas = db.Schools.Where(x => x.SchoolID == new Guid("4512BA7B-64AC-4F73-886E-8E4D62A8A7BB")).ToList();

            foreach (var item in escuelas)
            {
                #region DarDealtaGrupoPrimeroTemporal
                //int gruponum = 0;
                //if (item.Scholar_Level==2)
                //{
                //     gruponum = 7;
                //}
                //else if(item.Scholar_Level == 3)
                //{
                //     gruponum = 11;

                //}


                //var grupoTemporal = db.Groups.FirstOrDefault(x => x.SchoolID == item.SchoolID
                //            && x.IsActivo == true
                //            && x.NivelEscolarID == gruponum
                //            && x.Description == " GrupoTemporal"
                //            && x.Generacion == DateTime.UtcNow.Year
                //            && x.IsOficial == true);

                //if (grupoTemporal == null)
                //{
                //    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                //    var random = new Random();
                //    var enumnip = new string(
                //        Enumerable.Repeat(chars, 5)
                //                    .Select(z => z[random.Next(z.Length)])
                //                    .ToArray());

                //    String nip = "G" + enumnip;

                //    grupoTemporal = new Models.Group()
                //    {
                //        GroupID = Guid.NewGuid(),
                //        SchoolID = item.SchoolID,
                //        NivelEscolarID = gruponum,//db.NivelEscolars.First(x => x.Grado == gruponum).LevelID, //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                //        Description = " GrupoTemporal",
                //        IsActivo = true,
                //        Turno = 1,
                //        Generacion = DateTime.UtcNow.Year,
                //        StartDate = DateTime.UtcNow,
                //        Nip = nip,
                //        NumParciales = 0,
                //        Activo = true,
                //        IsCerrado = false,
                //        IsOficial = true
                //    };
                //    grupoTemporal.Timezone = db.Configuracions.First().Timezone;
                //    grupoTemporal.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                //    if (grupoTemporal.SchoolID != null)
                //    {
                //        grupoTemporal.Timezone = db.Schools.Where(x => x.SchoolID == item.SchoolID).First().Timezone;
                //        grupoTemporal.TimezoneOffset = db.Schools.Where(x => x.SchoolID == item.SchoolID).First().TimezoneOffset;
                //    }



                //    //Materias
                //    foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                //    {
                //        grupoTemporal.GroupMaterias.Add(new GroupMateria()
                //        {
                //            Id = Guid.NewGuid(),
                //            Materia = materia,
                //            NumParciales = 0,
                //            //Validado = 1, ///validacion extra metadata
                //        });

                //    }

                //    foreach (var libro in db.Libroes.Where(x => x.IdNivelEscolar == grupoTemporal.NivelEscolarID))
                //    {
                //        grupoTemporal.Libroes.Add(libro);
                //    }

                //    db.Groups.Add(grupoTemporal);
                // //   GruposNuevos.Add(grupoTemporal);

                //    db.SaveChanges();
                // //   Mensajes.Add("Se agrego el grupo: " + gruponum + des);
                //}

                #endregion


                //Licencia
                var licencia = db.Licencias.Where(x => x.IdEscuela == item.SchoolID && x.IsAprobado == true && x.FechaFinal.HasValue && x.FechaFinal.Value > DateTime.UtcNow).FirstOrDefault();

                if (licencia != null)
                {
                    //Agregar Alumnos
                    var AlumnosSinLicencia = db.sofia_licencia_AgregarLicenciaAlumnosGpoActivos(licencia.Id, item.SchoolID).FirstOrDefault().Value;

                    //Agregar Profesores en grupos activos
                    var ProfesoresActivos = db.sofia_licencia_AgregarLicenciaProfesoresGpoActivos(licencia.Id, item.SchoolID).FirstOrDefault().Value;

                    //Agregar Profesores sin grupo en la escuela
                    var ProfesoresSinGrupo = db.sofia_licencia_AgregarLicenciaProfesoresEscuela(licencia.Id, item.SchoolID).FirstOrDefault().Value;

                    //Agregar a Director
                    var LicDirector = db.sofia_licencia_AgregarLicenciaDirectorEscuela(licencia.Id, item.SchoolID).FirstOrDefault().Value;

                    ViewModel_LicenciaTamaulipas newadd = new ViewModel_LicenciaTamaulipas()
                    {
                        Ciudad = item.City,
                        NombreEscuela = item.Name,
                        CCT = item.ClaveOficial,
                        Escolaridad = (item.Scholar_Level == 2 ? "Primaria" : "Secundaria"),
                        LicAlumnos = AlumnosSinLicencia,
                        LicProGrupoActivo = ProfesoresActivos,
                        LicProfEscuela = ProfesoresSinGrupo,
                        LicDirector = LicDirector,
                        FechaFinal = licencia.FechaFinal.Value,
                        TotalLic = licencia.NumAlumnos.Value,
                        Agregados = (AlumnosSinLicencia + ProfesoresActivos + ProfesoresSinGrupo + LicDirector)
                    };

                    listaRetorno.Add(newadd);
                }

            }

            return Json(listaRetorno.Select(x=> new 
            {
                Ciudad = x.Ciudad,
                Nombre = x.NombreEscuela,
                CCT = x.CCT,
                Escolaridad = x.Escolaridad,
                Alumnos = x.LicAlumnos,
                ProfesoresActivos = x.LicProGrupoActivo,
                ProfesoresEscuela = x.LicProfEscuela,
                Director = x.LicDirector,
                TotalLicencia = x.TotalLic,
                Agregados = x.Agregados
                //FechaFinal = x.FechaFinal
            }).OrderBy(x=>x.Ciudad)
            .ThenBy(x=>x.Escolaridad)
            .ThenBy(x=>x.Nombre), JsonRequestBehavior.AllowGet);
        }

        public JsonResult _UpdateDirector()
        {
            var director = db.xxxxDirectorPrimarias.ToList();
            var prim = 0;
            foreach(var item in director)
            {
                var escuelaexis = db.Schools.Where(x => x.ClaveOficial == item.cct).FirstOrDefault();
                if(escuelaexis != null)
                {
                    var directoresc = db.Directors.Find(escuelaexis.IdDirector);

                    directoresc.aspnet_Users.UserExtra.Name = item.Director;
                    directoresc.aspnet_Users.UserExtra.LastNameP = " ";
                    directoresc.aspnet_Users.UserExtra.LastNameM = " ";

                    //item.Procesado = true;
                    
                    db.SaveChanges();
                    prim++;
                }
            }

            var directorsec = db.xxxxDirectorSecundarias.ToList();
            var sec = 0;
            foreach(var item in directorsec)
            {
                var escuelaexis = db.Schools.Where(x => x.ClaveOficial == item.cct).FirstOrDefault();
                if (escuelaexis != null)
                {
                    var directoresc = db.Directors.Find(escuelaexis.IdDirector);

                    directoresc.aspnet_Users.UserExtra.Name = item.Director;
                    directoresc.aspnet_Users.UserExtra.LastNameP = " ";
                    directoresc.aspnet_Users.UserExtra.LastNameM = " ";

                    //item.Procesado = true;

                    db.SaveChanges();
                    sec++;
                }
            }
            var datos = new
            {
                correcto = true,
                mensaje = "se actualizaron " + prim + " directores primaria y " + sec + " directores secundaira"
            };

            return Json(datos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EscuelaDistribuidor()
        {
            ViewBag.Distribuidores = db.Distribuidors.OrderBy(x => x.Estado1.Codigo).ToList();

            return View();
        }

        public ActionResult CambiarEscuela(string CCT, string Nombre)
        {
            var v = db.Schools.AsQueryable();

            if(string.IsNullOrEmpty(CCT))
            {
                v = v.Where(x => x.ClaveOficial.Contains(CCT));
            }
            if(string.IsNullOrEmpty(Nombre))
            {
                v = v.Where(x => x.Name.Contains(Nombre));
            }

            return View(v.ToList());
        }

        public ActionResult _Cambios(Guid Id)
        {

            return PartialView();
        }


        public JsonResult VinculacionPorListas()//(List<Guid> EscuelasId, List<Guid> GroupsId)
        {
            // return Json(true, JsonRequestBehavior.AllowGet);
            List <Guid> EscuelasId = new List<Guid>();
            List<Guid> GroupsId = new List<Guid>();

            //GroupsId.Add(Guid.Parse("47DED6A7-F1BD-4626-B17E-DC5309F29722"));
            EscuelasId.Add(Guid.Parse("7D35D982-08EE-4FC7-99B7-12FEB4BC19DA"));

            List<string> Mensajes = new List<string>();
            try
            {
                Entities db = new Entities();
                List<DistribucionSofia.Models.Group> Grupos = new List<DistribucionSofia.Models.Group>();
                List<DistribucionSofia.Models.Group> GruposNuevos = new List<DistribucionSofia.Models.Group>();

                int TipoMovimiento;// 1.VinculacionEscuelaCompleta,2.ViculacionGrupo,3.VinculacionMovimientoEntreSalones(PorAlumno)
                IMembershipService MembershipService = new AccountMembershipService();
                //var Escuela = db.Schools.Where(x => x.SchoolID == EscuelaId).FirstOrDefault();// db.xxxAltaMasivaAlunmos.Where(x => !x.is_procesado);// && x.GRADO==1 && x.GRUPO=="A").Take(30);
                if (GroupsId.Count == 0)
                {
                    foreach (var EscuelaId in EscuelasId)
                    {
                        List<DistribucionSofia.Models.Group> Grupos2 = db.Groups.Where(x => x.SchoolID == EscuelaId && x.Generacion == (DateTime.Now.Year-1) && x.IsCerrado==true).ToList();
                        foreach (var g in Grupos2)
                        {
                            Grupos.Add(g);
                        }
                    }
                    TipoMovimiento = 1;
                }
                else
                {
                    foreach (var GroupId in GroupsId)
                    {
                        List<DistribucionSofia.Models.Group> Grupos2 = db.Groups.Where(x => x.GroupID == GroupId && x.Generacion == (DateTime.Now.Year-1) && x.IsCerrado==true).ToList();
                        foreach (var g in Grupos2)
                        {
                            Grupos.Add(g);
                        }

                    }
                    TipoMovimiento = 2;
                }



                foreach (var Grupo in Grupos)
                {
                    List<Student> Students = new List<Student>();
                    var numeroStudents = Students.Count();
                    var s = Grupo.Students.Distinct().ToList();
                    foreach (var student in s)
                    {
                        Students.Add(student);
                    }

                    #region foreach Estudiantes

                    foreach (var Stundet in Students)
                    {
                        if (Grupo.NivelEscolarID==6)
                        {
                            //No hacemos nada si es sexto
                        }
                        else
                        {
                        var nivelescolaridnext = 0;
                        if (Grupo.NivelEscolarID == 9)
                        {
                            nivelescolaridnext = 4;
                        }
                        else { nivelescolaridnext = (Grupo.NivelEscolarID + 1);  }

                        var gruponum = db.NivelEscolars.Where(x=> x.LevelID ==nivelescolaridnext).FirstOrDefault();
                        var des = Grupo.Description;
                       
                        var grupo2 = db.Groups.FirstOrDefault(x => x.SchoolID == Grupo.SchoolID
                            && x.IsActivo == true
                            && x.NivelEscolarID == gruponum.LevelID
                            && x.Description == des
                            && x.Generacion == DateTime.UtcNow.Year
                            && x.IsOficial == true);

                        #region Crear grupo
                        //Crear grupo si no existe
                        if (grupo2 == null)
                        {
                            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var random = new Random();
                            var enumnip = new string(
                                Enumerable.Repeat(chars, 5)
                                            .Select(z => z[random.Next(z.Length)])
                                            .ToArray());

                            String nip = "G" + enumnip;

                            grupo2 = new Models.Group()
                            {
                                GroupID = Guid.NewGuid(),
                                SchoolID = Grupo.SchoolID,
                                NivelEscolarID = gruponum.LevelID,//db.NivelEscolars.First(x => x.Grado == gruponum).LevelID, //NivelEscolar = db.NivelEscolars.First(x=>x.Grado == gruponum),
                                Description = des,
                                IsActivo = true,
                                Generacion = DateTime.UtcNow.Year,
                                StartDate = DateTime.UtcNow,
                                Nip = nip,
                                NumParciales = 0,
                                Activo = true,
                                IsCerrado = false,
                                IsOficial = true
                            };
                            grupo2.Timezone = db.Configuracions.First().Timezone;
                            grupo2.TimezoneOffset = db.Configuracions.First().TimezoneOffset;

                            if (grupo2.SchoolID != null)
                            {
                                grupo2.Timezone = db.Schools.Where(x => x.SchoolID == Grupo.SchoolID).First().Timezone;
                                grupo2.TimezoneOffset = db.Schools.Where(x => x.SchoolID == Grupo.SchoolID).First().TimezoneOffset;
                            }
                            //Materias
                            foreach (var materia in db.Materias.Where(x => x.IsDefaultPrimariaPublica))
                            {
                                grupo2.GroupMaterias.Add(new GroupMateria()
                                {
                                    Id = Guid.NewGuid(),
                                    Materia = materia,
                                    NumParciales = 0,
                                    //Validado = 1, ///validacion extra metadata
                                });

                            }
                            db.Groups.Add(grupo2);
                            GruposNuevos.Add(grupo2);
                            db.SaveChanges();
                            Mensajes.Add("Se agrego el grupo: " + gruponum + des);
                        }
                        #endregion

                        #region GuardarAlumnoYlog


                        LogVinculacion log = new Models.LogVinculacion()
                        {
                            IdGrupoAnterior = Grupo.GroupID,
                            IdGrupoNuevo = grupo2.GroupID,
                            IdStudent = Stundet.StudentID,
                            FechaCambio = DateTime.UtcNow,
                            TipoCambio = TipoMovimiento,
                            Userv = (Guid)Membership.GetUser().ProviderUserKey

                        };

                        db.LogVinculacions.Add(log);
                        Stundet.Groups.Add(grupo2);

                        db.SaveChanges();

                        #endregion

                    } //endforeach
                    }
                   
                }
                return Json(GruposNuevos, JsonRequestBehavior.AllowGet);
                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Mensajes.Add(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }
                Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            //catch (Exception e)
            //{
            //    Mensajes.Add("Hubo un problema al crear las cuentas de registro de los alumnos");
            //    return JsonResult(false, JsonRequestBehavior.AllowGet);
            //}
            return Json(false, JsonRequestBehavior.AllowGet);

        }

        public JsonResult VinculacionPorAlumno()
        {
            List<ViewModel_VinculacionStudent> EstidiantesPorVincular = new List<ViewModel_VinculacionStudent>();
            ViewModel_VinculacionStudent AlumnoPrueba = new ViewModel_VinculacionStudent()
            {
                GrupoViejoId = Guid.Parse("47DED6A7-F1BD-4626-B17E-DC5309F29722"),
                GrupoNuevoId = Guid.Parse("6763761F-FA1F-4ABC-B818-D3F01689322A"),
                EstudianteId = Guid.Parse("CC43D588-C8E0-485C-B675-0ED49CE2C3D2"),
                Procesado = false

            };

            EstidiantesPorVincular.Add(AlumnoPrueba);
             Entities db = new Entities();
            int TipoMovimiento = 3;// 1.VinculacionEscuelaCompleta,2.ViculacionGrupo,3.VinculacionMovimientoEntreSalones(PorAlumno)
            try
            {
                foreach (var Estudiante in EstidiantesPorVincular)
                {
                    Student Stundet = db.Students.Where(x => x.StudentID == Estudiante.EstudianteId).FirstOrDefault();
                    DistribucionSofia.Models.Group GrupoVejo = db.Groups.Where(x => x.GroupID == Estudiante.GrupoViejoId).FirstOrDefault();
                    DistribucionSofia.Models.Group GrupoNuevo = db.Groups.Where(x => x.GroupID == Estudiante.GrupoNuevoId).FirstOrDefault();

                    LogVinculacion log = new Models.LogVinculacion()
                    {
                        IdGrupoAnterior = Estudiante.GrupoViejoId,
                        IdGrupoNuevo = Estudiante.GrupoNuevoId,
                        IdStudent = Estudiante.EstudianteId,
                        FechaCambio = DateTime.UtcNow,
                        TipoCambio = TipoMovimiento

                    };

                    if (GrupoVejo.IsCerrado == false)
                    {
                        Stundet.Groups.Remove(GrupoVejo);
                        Stundet.Groups.Add(GrupoNuevo);
                    }
                    else
                    {
                        Stundet.Groups.Add(GrupoNuevo);
                    }

                    db.LogVinculacions.Add(log);
                    db.SaveChanges();
                    Estudiante.Procesado = true;

                }

                return Json(EstidiantesPorVincular, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(EstidiantesPorVincular, JsonRequestBehavior.AllowGet);
            }

            return Json(EstidiantesPorVincular, JsonRequestBehavior.AllowGet);
        }
    }
}