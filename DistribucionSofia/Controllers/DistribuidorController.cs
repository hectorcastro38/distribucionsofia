﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.Security;
using System.Data.Entity.Validation;
using System.IO;
using System.Diagnostics;
using System.Web.Routing;
using DistribucionSofia.Models;
using DistribucionSofia.Models.Enum;
using DistribucionSofia.Models.ViewModels;
using PagedList;
using ConektaCSharp;
using Newtonsoft.Json.Linq;
using System.Web.UI.WebControls;
using System.Net.Http;
using OfficeOpenXml;

namespace DistribucionSofia.Controllers
{
    public class DistribuidorController : Controller
    {


        Entities db = new Entities();
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        Guid userid = (Guid)Membership.GetUser().ProviderUserKey;
        private aspnet_Users user = new aspnet_Users();

        protected override void Initialize(RequestContext requestContext)
        {

            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        //------------------------------- INTERFAZ ---------------------------//
        [Authorize(Roles = "Distribuidor")]
        public ActionResult Index()
        {
            ViewBag.EscuelasDis = db.Schools.Where(x => x.DistribuidorID == userid).Count();
            ViewBag.OrdenCompraDis = db.OrdenCompras.Where(x => x.IdDistribuidor == userid && x.Estatus != 2).Count();
            ViewBag.OrdenAdesaprobadaDis = db.OrdenCompras.Where(x => x.IdDistribuidor == userid && x.Estatus == 0).Count();
            ViewBag.Asesores = db.Distribuidors.Where(x => x.IdDistribuidor == userid).Count();
            ViewBag.EscuelasExpira = db.Schools.Where(x => x.DistribuidorID == userid).SelectMany(y => y.Licencias).Where(r => r.FechaFinal.Value.Month == (DateTime.Now.Month + 1) && r.FechaFinal.Value.Year == DateTime.Now.Year).Select(u => u.IdEscuela).Distinct().Count();

            return View();
        }

        public JsonResult _coutnAlum()
        {
            var escuelas = db.Schools.Where(x => x.IdDirector.HasValue && x.NumAlum == null);
            //Guid algo = new Guid("b8721d42-ec88-493e-a721-e205bfe4fcb3");
            //var escuela = db.Schools.Find(algo);
            //var est = escuela.Groups.Where(x => x.IsActivo == true).SelectMany(y => y.Students).Distinct();

            foreach (var esta in escuelas)
            {
                using (var dbe = new Entities())
                {
                    var escuela = dbe.Schools.Find(esta.SchoolID);
                    var algo = (esta.Groups.Where(x => x.IsActivo == true).Count() > 0 ? esta.Groups.Where(x => x.IsActivo == true).SelectMany(y => y.Students).Distinct().Count() : 0);
                    escuela.NumAlum = algo;

                    try
                    {

                        dbe.SaveChanges();
                    }
                    catch (Exception ex)
                    {


                    }
                }

            }


            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult NuevaOrden(int tipo, Guid? EscuelaID)
        {
            using (var db = new Entities())
            {
                var distribuidor = db.Distribuidors.Find(userid);
                ViewBag.Escuelas = db.Schools.Where(x => x.DistribuidorID == distribuidor.Id).ToList().Select(x => new
                {
                    Text = x.Name + " (" + x.ClaveOficial + ")",
                    Value = x.SchoolID
                });
                //ViewBag.Dependencia = Extensions.ToSelectListDisplayName(typeof(EnumDependenciaEscuela), "");

                ViewBag.Paises = db.Pais.Where(x => x.Id == distribuidor.IdPais).ToList();
                ViewBag.Estados = db.Estadoes.Where(x => x.IdPais == distribuidor.IdPais).OrderBy(x => x.Codigo).ToList();

                if (tipo == 1)
                {
                    ViewBag.Tipo = "Nueva orden de compra";
                }
                else if (tipo == 2)
                {
                    ViewBag.Tipo = "Agregar escuela nueva";
                }
                else
                {
                    ViewBag.Tipo = "Renovar/Nueva Licencia";
                }
                ViewBag.TipoNum = tipo;
                var cultura = distribuidor.Pai.LenguajeCodigoes.FirstOrDefault();
                ViewBag.Planes = db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0 && x.IdCultura == cultura.Id).Select(y => new ViewModel_CheckBoxPlan { IdPlan = y.IdPlan, Nombre = y.Nombre, Descripcion = y.Descripcion, Costo = y.Costo, Duracion = y.Duracion, IsChecked = false }).ToList();
                var model = new DistribucionSofia.Models.ViewModel_d_OrdenCompra();

                model.PeriodoPrueba = false;
                ViewBag.TieneDirector = 0;

                if (EscuelaID.HasValue)
                {
                    var escuela = db.Schools.Find(EscuelaID.Value);

                    model.SchoolID = escuela.SchoolID;
                    model.Name = escuela.Name;
                    model.ClaveOficial = escuela.ClaveOficial;
                    model.Type = escuela.Type;
                    model.Dependencia = escuela.Dependencia;
                    model.Scholar_Level = escuela.Scholar_Level;
                    model.Address = escuela.Address;
                    model.Telephone = escuela.Telephone;
                    model.EstadoCode = escuela.EstadoCode;
                    model.City = escuela.City;
                    model.Province = escuela.Province;
                    model.Country = escuela.Country;
                    model.Timezone = escuela.Timezone;
                    model.Detalle = escuela.Detalle;

                    if (escuela.IdDirector.HasValue)
                    {
                        ViewBag.TieneDirector = 1;
                    }
                    else
                    {
                        ViewBag.TieneDirector = 0;
                    }


                    model.IsPrueba = escuela.IsPrueba;

                }

                var escdis = new School();
                if (db.Schools.Where(x => x.DistribuidorID == userid && x.IsDistribuidor == true).Count() == 0)
                {
                    escdis.Name = "Sofía Premium " + distribuidor.aspnet_Users.UserExtra.Name + " " + distribuidor.aspnet_Users.UserExtra.LastNameP;
                    escdis.ClaveOficial = "00" + distribuidor.aspnet_Users.UserName + "" + distribuidor.aspnet_Users.UserExtra.LastNameP;
                    escdis.Type = 3;
                    escdis.Dependencia = 3;
                    escdis.Scholar_Level = 2;
                    escdis.Address = distribuidor.Calle + " " + distribuidor.Colonia;
                    escdis.Telephone = distribuidor.aspnet_Users.UserExtra.Telefono;
                    escdis.EstadoCode = distribuidor.Estado;
                    escdis.City = distribuidor.DELEGACION;
                    escdis.Province = distribuidor.DELEGACION;
                    escdis.Country = distribuidor.Pai.Nombre;
                    escdis.Detalle = "Escuela Distribuidor";

                    escdis.IsPrueba = true;


                }
                else
                {
                    escdis = null;
                }
                ViewBag.EscuelaDistribuidor = escdis;

                return View(model);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult NuevaOrden(ViewModel_d_OrdenCompra model, FormCollection fc)
        {
            var valido = ModelState.IsValid;
            Guid idusuario = Guid.Empty;
            try
            {
                var distribuidor = db.Distribuidors.FirstOrDefault(x => x.Id == userid);
                var cultura = distribuidor.Pai.LenguajeCodigoes.FirstOrDefault();

                if (distribuidor != null)
                {
                    model.Distribuidor = distribuidor;
                }
                var existe = db.Schools.FirstOrDefault(x => x.SchoolID == model.SchoolID);
                if (existe == null)
                {
                    //step 1
                    School escuelanew = new School();

                    escuelanew.SchoolID = Guid.NewGuid();
                    escuelanew.IdUser = userid;
                    escuelanew.DistribuidorID = userid;
                    escuelanew.FechaRegistro = DateTime.Now;
                    if (model.Timezone != null)
                    {
                        escuelanew.TimezoneOffset = TimeZoneInfo.FindSystemTimeZoneById(model.Timezone).BaseUtcOffset.Hours;
                    }
                    else
                    {
                        escuelanew.Timezone = db.Configuracions.First().Timezone;
                        escuelanew.TimezoneOffset = db.Configuracions.First().TimezoneOffset;
                    }

                    var estadoli = db.Estadoes.Find(model.IdEstado);

                    escuelanew.Name = model.Name;
                    escuelanew.Type = model.Dependencia;
                    escuelanew.Scholar_Level = model.Scholar_Level;
                    escuelanew.City = model.City;
                    escuelanew.Address = model.Address;
                    escuelanew.Telephone = model.Telephone;
                    escuelanew.ClaveOficial = model.ClaveOficial;
                    escuelanew.IsActive = true;
                    escuelanew.Dependencia = model.Dependencia;
                    escuelanew.Detalle = model.Detalle;
                    escuelanew.Localidad = model.Localidad;
                    escuelanew.Turno = model.Turno;
                    escuelanew.EstadoCode = estadoli.Codigo;
                    escuelanew.Province = estadoli.Nombre;
                    escuelanew.Country = estadoli.Pai.Nombre;
                    escuelanew.IdEstado = model.IdEstado;
                    escuelanew.IdPais = model.IdPais;
                    escuelanew.IsDistribuidor = false;
                    escuelanew.VinculacionExternaType = 0;
                    escuelanew.NumAlum = 0;

                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    bool nipvalido = false;
                    String nip = "E";
                    var length = 5;
                    var loop = 100000;
                    var cont = 0;
                    var result = new string(
                        Enumerable.Repeat(chars, length)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                    while (!nipvalido)
                    {
                        cont++;
                        if (cont > loop)
                        {
                            result = new string(
                            Enumerable.Repeat(chars, length + (cont / loop))
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                        }
                        nip = "E" + result;
                        if (db.Schools.Count(x => x.Nip == nip) == 0)
                            nipvalido = true;
                    }
                    escuelanew.Nip = nip;

                    db.Schools.Add(escuelanew);
                    db.SaveChanges();

                    if (!model.omiteContacto)
                    {
                        //registro director 
                        IMembershipService MembershipService = new AccountMembershipService();
                        if (true)
                        {
                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var random2 = new Random();
                            var result2 = new string(
                                Enumerable.Repeat(chars2, 10)
                                          .Select(s => s[random2.Next(s.Length)])
                                          .ToArray());
                            var usuariotemporal = "usuariotemporal" + result2;
                            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.correoDir);

                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    Roles.AddUserToRole(usuariotemporal, "Director");

                                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                                    Guid useridDir = (Guid)user.ProviderUserKey;

                                    DateTime fecha = DateTime.MinValue;
                                    fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                                    aspnet_Users us = db.aspnet_Users.Find(useridDir);
                                    us.UserExtra = new UserExtra();
                                    us.UserExtra.Birthday = fecha;
                                    us.UserExtra.Name = model.nombreDir;
                                    us.UserExtra.LastNameP = model.apellidoP;
                                    us.UserExtra.LastNameM = model.apellidoM;
                                    us.UserExtra.Password = "123456";
                                    us.UserExtra.SexoCode = (int)model.sexo;
                                    us.UserExtra.Correo = model.correoDir;
                                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                                    Director nuevoDirec = new Director()
                                    {
                                        DirectorID = Guid.NewGuid(),
                                        UserID = useridDir,

                                    };
                                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var random1 = new Random();
                                    bool nipvalido3 = false;
                                    String nip3 = "DIR";
                                    var length3 = 5;
                                    var loop3 = 100000;
                                    var cont3 = 0;
                                    var result1 = new string(
                                        Enumerable.Repeat(chars1, length3)
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                    while (!nipvalido3)
                                    {
                                        cont3++;
                                        if (cont3 > loop3)
                                        {
                                            result1 = new string(
                                            Enumerable.Repeat(chars1, length3 + (cont3 / loop3))
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                        }
                                        nip3 = "DIR" + result1;
                                        if (db.Directors.Count(x => x.Nip == nip3) == 0)
                                            nipvalido3 = true;
                                    }
                                    nuevoDirec.Nip = nip3;

                                    db.Directors.Add(nuevoDirec);
                                    db.SaveChanges();

                                    escuelanew.IdDirector = nuevoDirec.DirectorID;

                                    idusuario = nuevoDirec.UserID;

                                    string nuevousername;
                                    var indice = us.UserExtra.Indice;
                                    if (indice > 9999)
                                    {

                                        nuevousername = "D" + indice.ToString();
                                    }
                                    else
                                    {
                                        nuevousername = "D" + String.Format("{0:D4}", indice);
                                    }
                                    ///
                                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = "123456";
                                    db.SaveChanges();

                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                            }
                        }
                    }

                    var tipo = "";
                    //step 2
                    if (model.PeriodoPrueba)
                    {
                        if (model.Scholar_Level == 2)//Primaria
                        {
                            if (cultura.Id == new Guid("0A023B6D-3E8D-4227-87D9-02101D50D61C"))//Peru
                            {
                                model.IdPlan = new Guid("4E8E1745-EF1C-41A1-9B0C-99B8EBB4EDB1");
                            }
                            else if (cultura.Id == new Guid("48CAF20D-0186-41B5-9A18-E9CA2AA3417A"))//Colombia
                            {
                                model.IdPlan = new Guid("1BA9EBF5-28FE-4C30-874D-A947E66960A0");
                            }
                            else
                            {
                                model.IdPlan = new Guid("ccd5e13a-640c-4e07-bea9-ee2d17e2bbe4");//Mexico
                            }

                        }
                        else if (model.Scholar_Level == 4)//preparatoria
                        {
                            //7ECF3055-9F57-4708-97A0-32446F6C3631
                            if (cultura.Id == new Guid("7ECF3055-9F57-4708-97A0-32446F6C3631"))//Peru
                            {
                                model.IdPlan = new Guid("4E8E1745-EF1C-41A1-9B0C-99B8EBB4EDB1");
                            }
                            else
                            {
                                model.IdPlan = new Guid("c433e314-9998-43f8-97d5-427f2df17491");//Mexico
                            }
                        }
                        else if (model.Scholar_Level == 1)//preescolar
                        {
                            model.IdPlan = new Guid("3a7bf727-f543-4e8a-8b36-3960357c6385");
                        }
                        else //Secundaria
                        {
                            if (cultura.Id == new Guid("0A023B6D-3E8D-4227-87D9-02101D50D61C"))//Peru
                            {
                                model.IdPlan = new Guid("2F51A391-3E94-42E9-B71D-2DBA1E97DAF6");
                            }
                            else
                            {
                                model.IdPlan = new Guid("BE7207F6-0915-4439-A07C-9BDB305A4C53");//Mexico
                            }
                        }
                        if (cultura.Codigo == "es-co")
                        {
                            model.periodo = 12;
                        }
                        else if(User.Identity.Name == "D99680")
                        {
                            model.periodo = 12;
                        }
                        else {
                            model.periodo = 6;
                        }

                        model.Cantidad = model.Cantidad2;
                        tipo = "Licencia Prueba";
                        escuelanew.IsPrueba = true;
                        model.IsPrueba = true;
                    }
                    else
                    {
                        model.periodo = 12;
                        tipo = "Licencia";
                    }
                    var plan = db.Plans.Find(model.IdPlan);
                    var subtotal = model.Cantidad * plan.Costo;
                    var total = (subtotal * 1.16);
                    var iva = total - subtotal;

                    if (model.PeriodoPrueba)
                    {
                        subtotal = 0;
                        total = 0;
                        iva = 0;
                    }

                    Licencia lic = new Licencia()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdPlan = plan.IdPlan,
                        NumAlumnos = model.Cantidad,
                        FechaRegistro = DateTime.Now,
                        FechaFinal = DateTime.Now.AddMonths(model.periodo),
                        IsAprobado = false,
                        //IsAprobado = true,
                        IsUsing = false,
                        IsPrueba = model.IsPrueba,
                        StandBy = false,
                        IsActivo = false,
                        //IsActivo = true,
                        CostoAlumno = plan.Costo,
                        CostoAlumnoPlan = plan.Costo,
                        DuracionPlan = plan.Duracion,
                        NombrePlan = plan.Nombre,
                        DescripcionPlan = plan.Descripcion,
                        CicloEscolar = (DateTime.Now.Month < 7 ? (DateTime.Now.Year - 1) : DateTime.Now.Year)
                    };
                    
                    if (User.Identity.Name == "D100767" || User.Identity.Name == "D118210" || User.Identity.Name == "D162894" || User.Identity.Name == "sonoraprivadas" || User.Identity.Name == "SofiaTamaulipas" || User.Identity.Name == "LauraTanoris")
                    {
                        lic.IsActivo = true;
                        lic.IsAprobado = true;
                    }

                    db.Licencias.Add(lic);
                    db.SaveChanges();

                    if (!model.omiteContacto)
                    {
                        if (idusuario != Guid.Empty)
                        {
                            LicenciaUsuario newlic = new LicenciaUsuario()
                            {
                                Id = Guid.NewGuid(),
                                IdUsuario = idusuario,
                                IdLicencia = lic.Id,
                                FechaRegistro = DateTime.Now,
                                IsPrueba = lic.IsPrueba
                            };
                            db.LicenciaUsuarios.Add(newlic);
                            db.SaveChanges();
                        }
                    }

                    //OrdenCompra
                    OrdenCompra ordnew = new OrdenCompra()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdDistribuidor = userid,
                        FechaRegistro = DateTime.Now,
                        IdLicencia = lic.Id,
                        SubTotal = subtotal,
                        IVA = iva,
                        Total = total,
                        Estatus = 0
                        //Estatus = 1
                    };
                    if (model.aplicaFactura)
                    {
                        ordnew.Facturado = false;
                        ordnew.ReqFactura = true;
                    }
                    else
                    {
                        ordnew.ReqFactura = false;
                    }
                    if (User.Identity.Name == "D100767" || User.Identity.Name == "D118210" || User.Identity.Name == "LauraTanoris" || User.Identity.Name == "D162894" || User.Identity.Name == "sonoraprivadas" || User.Identity.Name == "SofiaTamaulipas")
                    {
                        ordnew.Estatus = 1;
                    }

                    db.OrdenCompras.Add(ordnew);
                    db.SaveChanges();

                    //OdenCompraProducto
                    OrdenCompraProducto prodnew = new OrdenCompraProducto()
                    {
                        Id = Guid.NewGuid(),
                        IdOrdenCompra = ordnew.Id,
                        IdPlan = plan.IdPlan,
                        Cantidad = model.Cantidad,
                        CostoUnitario = plan.Costo,
                        Concepto = plan.Nombre,
                        Total = subtotal,
                        Unidad = tipo,
                        FechaRegistro = DateTime.Now
                    };
                    db.OrdenCompraProductoes.Add(prodnew);

                    db.SaveChanges();

                    //enviar correos
                    Email.EnviarNotificacionUsuario(userid, escuelanew.SchoolID);
                    if (model.PeriodoPrueba)
                    {
                        Email.EnviarNotificacionNuevaPrueba(escuelanew.SchoolID);
                    } else
                    {
                        Email.EnviarNotificacionNueva(escuelanew.SchoolID);
                    }

                    return RedirectToAction("Orden", "Distribuidor", new { idorden = ordnew.Id });
                }
                else
                {
                    //step 1
                    School escuelanew = db.Schools.Find(model.SchoolID);

                    escuelanew.IsPrueba = model.IsPrueba;

                    if (!model.omiteContacto)
                    {
                        //registro director 
                        IMembershipService MembershipService = new AccountMembershipService();
                        if (true)
                        {
                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var random2 = new Random();
                            var result2 = new string(
                                Enumerable.Repeat(chars2, 10)
                                          .Select(s => s[random2.Next(s.Length)])
                                          .ToArray());
                            var usuariotemporal = "usuariotemporal" + result2;
                            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.correoDir);

                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    Roles.AddUserToRole(usuariotemporal, "Director");

                                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                                    Guid useridDir = (Guid)user.ProviderUserKey;

                                    DateTime fecha = DateTime.MinValue;
                                    fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                                    aspnet_Users us = db.aspnet_Users.Find(useridDir);
                                    us.UserExtra = new UserExtra();
                                    us.UserExtra.Birthday = fecha;
                                    us.UserExtra.Name = model.nombreDir;
                                    us.UserExtra.LastNameP = model.apellidoP;
                                    us.UserExtra.LastNameM = model.apellidoM;
                                    us.UserExtra.Password = "123456";
                                    us.UserExtra.SexoCode = (int)model.sexo;
                                    us.UserExtra.Correo = model.correoDir;
                                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                                    Director nuevoDirec = new Director()
                                    {
                                        DirectorID = Guid.NewGuid(),
                                        UserID = useridDir,

                                    };


                                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var random1 = new Random();
                                    bool nipvalido3 = false;
                                    String nip3 = "DIR";
                                    var length3 = 5;
                                    var loop3 = 100000;
                                    var cont3 = 0;
                                    var result1 = new string(
                                        Enumerable.Repeat(chars1, length3)
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                    while (!nipvalido3)
                                    {
                                        cont3++;
                                        if (cont3 > loop3)
                                        {
                                            result1 = new string(
                                            Enumerable.Repeat(chars1, length3 + (cont3 / loop3))
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                        }
                                        nip3 = "DIR" + result1;
                                        if (db.Directors.Count(x => x.Nip == nip3) == 0)
                                            nipvalido3 = true;
                                    }
                                    nuevoDirec.Nip = nip3;

                                    db.Directors.Add(nuevoDirec);
                                    db.SaveChanges();

                                    escuelanew.IdDirector = nuevoDirec.DirectorID;

                                    string nuevousername;
                                    var indice = us.UserExtra.Indice;
                                    if (indice > 9999)
                                    {

                                        nuevousername = "D" + indice.ToString();
                                    }
                                    else
                                    {
                                        nuevousername = "D" + String.Format("{0:D4}", indice);
                                    }
                                    ///
                                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = "123456";

                                    escuelanew.IdDirector = nuevoDirec.DirectorID;

                                    db.SaveChanges();

                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                            }
                        }
                    }

                    //step 2

                    var tipo = "";
                    if (model.PeriodoPrueba)
                    {
                        if (model.Scholar_Level == 2)//Primaria
                        {
                            if (cultura.Id == new Guid("0A023B6D-3E8D-4227-87D9-02101D50D61C"))//Peru
                            {
                                model.IdPlan = new Guid("4E8E1745-EF1C-41A1-9B0C-99B8EBB4EDB1");
                            }
                            else if (cultura.Id == new Guid("48CAF20D-0186-41B5-9A18-E9CA2AA3417A"))//Colombia
                            {
                                model.IdPlan = new Guid("1BA9EBF5-28FE-4C30-874D-A947E66960A0");
                            }
                            else
                            {
                                model.IdPlan = new Guid("ccd5e13a-640c-4e07-bea9-ee2d17e2bbe4");//Mexico
                            }

                        }
                        else if (model.Scholar_Level == 4)//preparatoria
                        {
                            model.IdPlan = new Guid("c433e314-9998-43f8-97d5-427f2df17491");
                        }
                        else if (model.Scholar_Level == 1)//preescolar
                        {
                            model.IdPlan = new Guid("3a7bf727-f543-4e8a-8b36-3960357c6385");
                        }
                        else //Secundaria
                        {
                            if (cultura.Id == new Guid("0A023B6D-3E8D-4227-87D9-02101D50D61C"))//Peru
                            {
                                model.IdPlan = new Guid("2F51A391-3E94-42E9-B71D-2DBA1E97DAF6");
                            }
                            else
                            {
                                model.IdPlan = new Guid("BE7207F6-0915-4439-A07C-9BDB305A4C53");//Mexico
                            }
                        }
                        if (cultura.Codigo == "es-co")
                        {
                            model.periodo = 12;
                        }
                        else if (User.Identity.Name == "D99680")
                        {
                            model.periodo = 12;
                        }
                        else
                        {
                            model.periodo = 6;
                        }

                        model.Cantidad = model.Cantidad2;
                        tipo = "Licencia Prueba";
                        escuelanew.IsPrueba = true;
                        model.IsPrueba = true;
                    }
                    else
                    {
                        model.periodo = 12;
                        tipo = "Licencia";
                    }
                    var plan = db.Plans.Find(model.IdPlan);
                    var subtotal = model.Cantidad * plan.Costo;
                    var total = subtotal * 1.16;
                    var iva = total - subtotal;

                    if (model.PeriodoPrueba)
                    {
                        subtotal = 0;
                        total = 0;
                        iva = 0;
                    }

                    Licencia lic = new Licencia()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdPlan = plan.IdPlan,
                        NumAlumnos = model.Cantidad,
                        FechaRegistro = DateTime.Now,
                        FechaFinal = DateTime.Now.AddMonths(model.periodo),
                        IsAprobado = false,
                        IsUsing = false,
                        IsPrueba = model.IsPrueba,
                        StandBy = false,
                        IsActivo = false,
                        CostoAlumno = plan.Costo,
                        CostoAlumnoPlan = plan.Costo,
                        DuracionPlan = plan.Duracion,
                        NombrePlan = plan.Nombre,
                        DescripcionPlan = plan.Descripcion,
                        CicloEscolar = (DateTime.Now.Month < 7 ? (DateTime.Now.Year - 1) : DateTime.Now.Year)
                    };
                    if (User.Identity.Name == "D100767" || User.Identity.Name == "D162894" || User.Identity.Name == "sonoraprivadas" || User.Identity.Name == "SofiaTamaulipas")
                    {
                        lic.IsActivo = true;
                        lic.IsAprobado = true;
                    }
                    db.Licencias.Add(lic);
                    db.SaveChanges();

                    if (!model.omiteContacto)
                    {
                        if (idusuario != Guid.Empty)
                        {
                            LicenciaUsuario newlic = new LicenciaUsuario()
                            {
                                Id = Guid.NewGuid(),
                                IdUsuario = idusuario,
                                IdLicencia = lic.Id,
                                FechaRegistro = DateTime.Now,
                                IsPrueba = lic.IsPrueba
                            };
                            db.LicenciaUsuarios.Add(newlic);
                            db.SaveChanges();
                        }
                    }


                    //OrdenCompra
                    OrdenCompra ordnew = new OrdenCompra()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdDistribuidor = userid,
                        FechaRegistro = DateTime.Now,
                        IdLicencia = lic.Id,
                        SubTotal = subtotal,
                        IVA = iva,
                        Total = total,
                        Estatus = 0
                        //Estatus = 1
                    };
                    if (model.aplicaFactura)
                    {
                        ordnew.Facturado = false;
                        ordnew.ReqFactura = true;
                    }
                    else
                    {
                        ordnew.ReqFactura = false;
                    }
                    if (User.Identity.Name == "D100767" || User.Identity.Name == "D162894" || User.Identity.Name == "sonoraprivadas" || User.Identity.Name == "SofiaTamaulipas")
                    {
                        ordnew.Estatus = 1;
                    }
                    db.OrdenCompras.Add(ordnew);
                    db.SaveChanges();

                    //OdenCompraProducto
                    OrdenCompraProducto prodnew = new OrdenCompraProducto()
                    {
                        Id = Guid.NewGuid(),
                        IdOrdenCompra = ordnew.Id,
                        IdPlan = plan.IdPlan,
                        Cantidad = model.Cantidad,
                        CostoUnitario = plan.Costo,
                        Concepto = plan.Nombre,
                        Total = subtotal,
                        Unidad = tipo,
                        FechaRegistro = DateTime.Now
                    };
                    db.OrdenCompraProductoes.Add(prodnew);

                    db.SaveChanges();

                    if (escuelanew.IdDirector.HasValue && !model.omiteContacto)
                    {
                        var director = db.Directors.Where(x => x.DirectorID == escuelanew.IdDirector).FirstOrDefault();
                        LicenciaUsuario newlicuser = new LicenciaUsuario()
                        {
                            Id = Guid.NewGuid(),
                            IdUsuario = director.UserID,
                            IdLicencia = lic.Id,
                            FechaRegistro = DateTime.Now,
                            IsActivo = true,
                            IsPrueba = lic.IsPrueba
                        };
                        db.LicenciaUsuarios.Add(newlicuser);
                        db.SaveChanges();
                        db.sofia_licencia_AgregarLicenciaEscuelaUsuarios(lic.Id, escuelanew.SchoolID, lic.IsPrueba);
                    }

                    //enviar correos
                    Email.EnviarNotificacionUsuario(userid, escuelanew.SchoolID);
                    if (model.PeriodoPrueba)
                    {
                        Email.EnviarNotificacionNuevaPrueba(escuelanew.SchoolID);
                    }
                    else
                    {
                        Email.EnviarNotificacionNueva(escuelanew.SchoolID);
                    }

                    return RedirectToAction("Orden", "Distribuidor", new { idorden = ordnew.Id });
                }
            }
            catch (DbEntityValidationException e)
            {
                var errores = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        errores.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                //throw;
                ViewBag.Errores = errores;

                return View(model);
            }
        }

        public JsonResult _ValidaCCT(string cct)
        {
            var esta = db.Schools.Where(x => x.ClaveOficial == cct).FirstOrDefault();

            if (esta != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Orden(Guid idorden)
        {
            return View(db.OrdenCompras.Find(idorden));
        }

        public JsonResult _GetSchool(Guid SchoolID)
        {
            using (var dbe = new Entities()) {
                var escuela = dbe.Schools.Find(SchoolID);

                var licenciaValida = dbe.Licencias.Where(x => x.IdEscuela == escuela.SchoolID).OrderByDescending(y => y.FechaFinal).FirstOrDefault();
                var licenciaPrueba = dbe.Licencias.Where(x => x.IdEscuela == escuela.SchoolID && x.IsPrueba == true).Count();
                var mensaje = "";
                var mensajetipo = 0;

                if (licenciaValida != null)
                {
                    if (licenciaValida.FechaFinal > DateTime.Now)
                    {
                        mensaje = "La escuela cuenta con licencia hasta " + licenciaValida.FechaFinal;
                        mensajetipo = 0;
                    }
                    else
                    {
                        if (licenciaPrueba != 0)
                        {

                        }
                        mensaje = "La licencia caduco " + licenciaValida.FechaFinal;
                        mensajetipo = 1;
                    }
                }
                else
                {
                    mensaje = "La escuela no tiene licencia";
                    mensajetipo = 2;
                }

                var datos = new {
                    Id = escuela.SchoolID,
                    Nombre = escuela.Name,
                    Clave = escuela.ClaveOficial,
                    Tipo = escuela.Type,
                    Escolaridad = escuela.Scholar_Level,
                    Direccion = escuela.Address,
                    Telefono = escuela.Telephone,
                    Estado = escuela.IdEstado,
                    Ciudad = escuela.City,
                    Pais = escuela.IdPais,
                    Horario = escuela.Timezone,
                    Comentarios = escuela.Detalle,
                    Mensaje = mensaje,
                    TipoMensaje = mensajetipo,
                    Prueba = escuela.IsPrueba
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }
        }

        public async System.Threading.Tasks.Task<ActionResult> ReporteEscuelas(int? page, int? Dependencia, string Ciudad, string CCT,
            string Nombre, int? DiaI, int? MesI, int? AnioI, int? DiaF, int? MesF, int? AnioF, int? NivelEscolar, int? Excel)
        {
            Guid userid = (Guid)Membership.GetUser().ProviderUserKey;
            var usuario = db.UserExtras.Find(userid);
            var idestado = Guid.Empty;
            var param_iddistribuidor = userid;


            List<ViewModel_d_ReporteEstadistico_Escuela> lista = new List<ViewModel_d_ReporteEstadistico_Escuela>();
            IEnumerable<ViewModel_d_ReporteEstadistico_Escuela> Consulta = null;

            ViewBag.NivelEscolarFilter = NivelEscolar;
            ViewBag.DependenciaFilter = Dependencia;
            ViewBag.NombreFilter = Nombre;
            ViewBag.DiaIFilter = DiaI;
            ViewBag.MesIFilter = MesI;
            ViewBag.AnioIFilter = AnioI;
            ViewBag.DiaFFilter = DiaF;
            ViewBag.MesFFilter = MesF;
            ViewBag.AnioFFilter = AnioF;
            ViewBag.CCTFilter = CCT;
            ViewBag.CiudadFilter = Ciudad;
            ViewBag.TAM = 0;

            ViewBag.NivelEscolar = Extensions.ToSelectListDisplayName(typeof(EnumNivelEscolar), NivelEscolar.ToString());
            ViewBag.Dependencia = Extensions.ToSelectListDisplayName(typeof(EnumDependenciaEscuela), Dependencia.ToString());

            var fechainicio = DateTime.UtcNow.AddMonths(-12).ToShortDateString();
            if (DiaI.HasValue && MesI.HasValue && AnioI.HasValue)
            {
                fechainicio = new DateTime(AnioI.Value, MesI.Value, DiaI.Value).ToShortDateString();
            }

            var fechafinal = DateTime.UtcNow.ToShortDateString();
            if (DiaF.HasValue && MesF.HasValue && AnioF.HasValue)
            {
                fechafinal = new DateTime(AnioF.Value, MesF.Value, DiaF.Value).ToShortDateString();
            }

            var path = "GeneralEscuelas.xlsx";
            
            var param_dependencia = Dependencia.HasValue ? Dependencia.Value : 0;
            var param_fechainicio = fechainicio;
            var param_fechafinal = fechafinal;
            var param_nombre = !string.IsNullOrEmpty(Nombre) ? Nombre : "";
            var param_nivelescolar = NivelEscolar.HasValue ? NivelEscolar.Value : 0;
            var param_CCT = !string.IsNullOrEmpty(CCT) ? CCT : "";
            var param_Ciudad = !string.IsNullOrEmpty(Ciudad) ? Ciudad : "";
            var param_Tipo = 2;
            ViewBag.Errores = 0;

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://sofiareportes.azurewebsites.net/api/ReporteSemaforo?IdDistribuidor=+" + param_iddistribuidor
                        + "+&FechaInicio=" + param_fechainicio
                        + "&FechaFinal=" + param_fechafinal
                        + "&Dependencia=" + param_dependencia
                        + "&Ciudad=" + param_Ciudad
                        + "&CCT=" + param_CCT
                        + "&Nombre=" + param_nombre
                        + "&NivelEscolar=" + param_nivelescolar
                        + "&Tipo=" + param_Tipo + "");
                    //HTTP GET
                    var result = await client.GetAsync(client.BaseAddress.AbsoluteUri);
                    //responseTask.Wait();

                    //var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<IList<ViewModel_d_ReporteEstadistico_Escuela>>();
                        readTask.Wait();
                        Consulta = readTask.Result;
                    }

                    foreach (var item in Consulta)
                    {
                        ViewModel_d_ReporteEstadistico_Escuela escuela = new ViewModel_d_ReporteEstadistico_Escuela()
                        {
                            FechaInicio = DateTime.Parse(fechainicio),
                            FechaFinal = DateTime.Parse(fechafinal),
                            Ciudad = item.Ciudad,
                            NombreEscuela = item.NombreEscuela,
                            CCT = item.CCT,
                            Actividades_Asignadas = item.Actividades_Asignadas,
                            EjerciciosActividades = item.EjerciciosActividades,
                            EjerciciosLibres = item.EjerciciosLibres,
                            EjerciciosOtros = item.EjerciciosOtros,
                            TotalEjercicios = item.TotalEjercicios
                        };
                        lista.Add(escuela);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Errores = 1;
            }

            var rows = 20;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;
            ViewBag.FechaInicio = fechainicio;
            ViewBag.FechaFinal = fechafinal;

            if (Excel.HasValue)
            {
                var fi = new FileInfo(Server.MapPath("~/ImportedFiles/Plantillas/" + path));
                using (var pi = new ExcelPackage(fi))
                {

                    var ws = pi.Workbook.Worksheets["ESCUELAS"];

                    ////Fecha Inicio
                    ws.Cells[2, 7].Value = fechainicio;
                    //FormatoCelda
                    ws.Cells[2, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ////Fecha Final
                    ws.Cells[2, 9].Value = fechafinal;
                    //FormatoCelda
                    ws.Cells[2, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    int i = 4;
                    foreach (var item in Consulta)
                    {
                        ////Numeración
                        ws.Cells[i, 1].Value = (i - 4) + 1;
                        //FormatoCelda
                        ws.Cells[i, 1].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////Ciudad
                        ws.Cells[i, 2].Value = item.Ciudad;
                        //FormatoCelda
                        ws.Cells[i, 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////CCT
                        ws.Cells[i, 3].Value = item.CCT;
                        //FormatoCelda
                        ws.Cells[i, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 3].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////Nombre Escuela
                        ws.Cells[i, 4].Value = item.NombreEscuela;
                        //FormatoCelda
                        ws.Cells[i, 4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 4].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                        ////Actividades Asignadas
                        ws.Cells[i, 5].Value = item.Actividades_Asignadas;
                        //FormatoCelda
                        ws.Cells[i, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 5].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////EjerciciosActividades
                        ws.Cells[i, 6].Value = item.EjerciciosActividades;
                        //FormatoCelda
                        ws.Cells[i, 6].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 6].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 6].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////EjerciciosLibres
                        ws.Cells[i, 7].Value = item.EjerciciosLibres;
                        //FormatoCelda
                        ws.Cells[i, 7].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 7].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 7].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 7].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////EjerciciosOtros
                        ws.Cells[i, 8].Value = item.EjerciciosOtros;
                        //FormatoCelda
                        ws.Cells[i, 8].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 8].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 8].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 8].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        ////TotalEjercicios
                        ws.Cells[i, 9].Value = item.TotalEjercicios;
                        //FormatoCelda
                        ws.Cells[i, 9].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 9].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 9].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 9].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells[i, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        i++;
                    }

                    Response.ClearContent();
                    pi.SaveAs(Response.OutputStream);
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename= " + DateTime.Now.ToShortDateString() + " Reporte Estadísticas.xlsx");
                    Response.ContentType = "application/ms-excel";

                    Response.Flush();
                    Response.End();
                }
                return RedirectToAction("ReporteEscuelas");
            }

            return View(lista.ToList().ToPagedList(pageNumber, pageSize));
        }

        public async System.Threading.Tasks.Task<ActionResult> ImprimirEscuela(string CCT, int? DiaI, int? MesI, int? AnioI, int? DiaF, int? MesF, int? AnioF, int tipo)
        {
            IEnumerable<ViewModel_d_ReporteSemaforo> Consulta = null;

            var fechainicio = DateTime.UtcNow.AddMonths(-12).ToShortDateString();
            if (DiaI.HasValue && MesI.HasValue && AnioI.HasValue)
            {
                fechainicio = new DateTime(AnioI.Value, MesI.Value, DiaI.Value).ToShortDateString();
            }

            var fechafinal = DateTime.UtcNow.ToShortDateString();
            if (DiaF.HasValue && MesF.HasValue && AnioF.HasValue)
            {
                fechafinal = new DateTime(AnioF.Value, MesF.Value, DiaF.Value).ToShortDateString();
            }

            var param_iddistribuidor = Guid.Empty;
            var param_dependencia = 0;
            var param_fechainicio = fechainicio;
            var param_fechafinal = fechafinal;
            var param_nombre = "";
            var param_nivelescolar = 0;
            var param_CCT = CCT;
            var param_Ciudad = "";
            var param_Tipo = 1;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://sofiareportes.azurewebsites.net/api/ReporteSemaforo?IdDistribuidor=+" + param_iddistribuidor
                    + "+&FechaInicio=" + param_fechainicio
                    + "&FechaFinal=" + param_fechafinal
                    + "&Dependencia=" + param_dependencia
                    + "&Ciudad=" + param_Ciudad
                    + "&CCT=" + param_CCT
                    + "&Nombre=" + param_nombre
                    + "&NivelEscolar=" + param_nivelescolar
                    + "&Tipo=" + param_Tipo + "");
                //HTTP GET
                var result = await client.GetAsync(client.BaseAddress.AbsoluteUri);
                //responseTask.Wait();

                //var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<ViewModel_d_ReporteSemaforo>>();
                    readTask.Wait();
                    Consulta = readTask.Result;
                }

            }

            var path = "";
            path = "GeneralGrupal.xlsx";

            var fi = new FileInfo(Server.MapPath("~/ImportedFiles/Plantillas/" + path));
            using (var pi = new ExcelPackage(fi))
            {

                var ws = pi.Workbook.Worksheets["GRUPAL"];

                ////Fecha Inicio
                ws.Cells[2, 7].Value = fechainicio;
                //FormatoCelda
                ws.Cells[2, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////Fecha Final
                ws.Cells[3, 7].Value = fechafinal;
                //FormatoCelda
                ws.Cells[3, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////Escuela
                ws.Cells[2, 4].Value = Consulta.FirstOrDefault().NombreEscuela;
                //FormatoCelda
                ws.Cells[2, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////CCT
                ws.Cells[3, 4].Value = Consulta.FirstOrDefault().CCT;
                //FormatoCelda
                ws.Cells[3, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////Escolaridad
                ws.Cells[4, 4].Value = Consulta.FirstOrDefault().Escolaridad;
                //FormatoCelda
                ws.Cells[4, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////Turno
                ws.Cells[5, 4].Value = Consulta.FirstOrDefault().Turno;
                //FormatoCelda
                ws.Cells[5, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ////Licencias
                ws.Cells[6, 4].Value = Consulta.FirstOrDefault().LicenciaAlumno;
                //FormatoCelda
                ws.Cells[6, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                int i = 8;
                foreach (var item in Consulta)
                {
                    ////Numeración
                    ws.Cells[i, 2].Value = (i - 8) + 1;
                    //FormatoCelda
                    ws.Cells[i, 2].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ////Grupo
                    ws.Cells[i, 3].Value = item.Grupo;
                    //FormatoCelda
                    ws.Cells[i, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ////Maestro
                    ws.Cells[i, 4].Value = item.Profesor;
                    //FormatoCelda
                    ws.Cells[i, 4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                    ////Ejercicios Actividad
                    ws.Cells[i, 5].Value = item.EjerciciosActividades;
                    //FormatoCelda
                    ws.Cells[i, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ////Actividades Asignadas
                    ws.Cells[i, 6].Value = item.Actividades_Asignadas;
                    //FormatoCelda
                    ws.Cells[i, 6].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 6].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 6].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[i, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    i++;
                }

                Response.ClearContent();
                pi.SaveAs(Response.OutputStream);
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename= " + DateTime.Now.ToShortDateString() + " Reporte Escuela " + Consulta.FirstOrDefault().NombreEscuela + ".xlsx");
                Response.ContentType = "application/ms-excel";

                Response.Flush();
                Response.End();
            }
            return RedirectToAction("ReportedeUso");

        }


        [Authorize(Roles = "Distribuidor")]
        public ActionResult OrdenesRealizadas()
        {
            var distribuidor = db.Distribuidors.Find(userid);

            return View(db.OrdenCompras.Where(x => x.IdDistribuidor == distribuidor.Id).ToList());
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _CancelarOrden(Guid id)
        {
            var orden = db.OrdenCompras.Find(id);
            orden.Estatus = 2;

            try
            {
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _DetalleCompra(Guid id)
        {

            return PartialView(db.OrdenCompras.Find(id));
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult ConsultarPlanes()
        {
            using (var db = new Entities())
            {
                return View(db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0).ToList());
            }
        }

        public JsonResult _ActivarSchool(Guid idSchool)
        {
            var school = db.Schools.Find(idSchool);

            school.IsDistribuidor = true;

            Licencia licnew = new Licencia()
            {
                Id = Guid.NewGuid(),
                IdEscuela = school.SchoolID,
                IdPlan = new Guid("CCD5E13A-640C-4E07-BEA9-EE2D17E2BBE4"),
                FechaInicio = DateTime.Now,
                FechaAprobacion = DateTime.Now,
                FechaFinal = DateTime.Now.AddYears(2),
                FechaRegistro = DateTime.Now,
                NumAlumnos = 10,
                IsAprobado = true,
                IsPrueba = true
            };
            db.Licencias.Add(licnew);
            try
            {
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult Escuela(int? page, int? Dependencia, int? Nivelescolar, string Clave, string Nombre)
        {
            ViewBag.DependenciaFilter = Dependencia;
            ViewBag.NivelescolarFilter = Nivelescolar;
            ViewBag.ClaveFilter = Clave;
            ViewBag.NombreFilter = Nombre;
            ViewBag.Dependencia = Extensions.ToSelectListDisplayName(typeof(EnumDependenciaEscuela), Dependencia.ToString());
            ViewBag.NivelEscolar = Extensions.ToSelectListDisplayName(typeof(EnumNivelEscolar), Nivelescolar.ToString());

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Schools.Where(x => x.DistribuidorID == userid).AsQueryable();

            if (Dependencia.HasValue)
            {
                v = v.Where(x => x.Dependencia == Dependencia);
            }
            if (Nivelescolar.HasValue)
            {
                v = v.Where(x => x.Scholar_Level == Nivelescolar);
            }

            if (!string.IsNullOrEmpty(Nombre))
            {
                v = v.Where(x => x.Name.Contains(Nombre));
            }

            if (!string.IsNullOrEmpty(Clave))
            {
                v = v.Where(x => x.ClaveOficial.Contains(Clave));
            }
            var escuelas = db.Schools.Where(x => x.DistribuidorID == userid).ToList();
            if (escuelas.Where(x => x.IsDistribuidor == true).Count() > 0)
            {
                ViewBag.AddSchool = true;
            }
            else
            {
                ViewBag.AddSchool = false;
            }
            return View(v.OrderBy(x => x.Name).ThenBy(x => x.ClaveOficial).ThenBy(x => x.Dependencia).ThenBy(x => x.Scholar_Level).ToList().ToPagedList(pageNumber, pageSize));
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _CreateDirectorDistri(string Name, string LastNameP, string LastNameM, string Email, DateTime Birthday, int Sexo, Guid SchoolID)
        {
            IMembershipService MembershipService = new AccountMembershipService();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            var usuariotemporal = "usuariotemporal" + result;
            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", Email);

            if (createStatus == MembershipCreateStatus.Success)
            {
                try
                {
                    Roles.AddUserToRole(usuariotemporal, "Director");

                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                    Guid userid = (Guid)user.ProviderUserKey;

                    aspnet_Users us = db.aspnet_Users.Find(userid);
                    us.UserExtra = new UserExtra();
                    us.UserExtra.Birthday = DateTime.Parse(Birthday.ToString("dd/MM/yyyy"));
                    us.UserExtra.Name = Name;
                    us.UserExtra.LastNameP = LastNameP;
                    us.UserExtra.LastNameM = LastNameM;
                    us.UserExtra.Password = "123456";
                    us.UserExtra.SexoCode = (int)Sexo;
                    us.UserExtra.Correo = Email;
                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                    Director nuevoDirec = new Director()
                    {
                        DirectorID = Guid.NewGuid(),
                        UserID = userid,

                    };

                    var school = db.Schools.Find(SchoolID);
                    school.IdDirector = nuevoDirec.DirectorID;

                    Licencia checa = db.Licencias.Where(x => x.IdEscuela == school.SchoolID).OrderByDescending(y => y.FechaRegistro).FirstOrDefault();
                    if (checa != null)
                    {
                        LicenciaUsuario newlic = new LicenciaUsuario()
                        {
                            Id = Guid.NewGuid(),
                            IdUsuario = userid,
                            IdLicencia = checa.Id,
                            FechaRegistro = DateTime.Now
                        };
                        db.LicenciaUsuarios.Add(newlic);
                    }

                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random1 = new Random();
                    bool nipvalido = false;
                    String nip = "DIR";
                    var length = 5;
                    var loop = 100000;
                    var cont = 0;
                    var result1 = new string(
                        Enumerable.Repeat(chars1, length)
                                  .Select(s => s[random1.Next(s.Length)])
                                  .ToArray());
                    while (!nipvalido)
                    {
                        cont++;
                        if (cont > loop)
                        {
                            result1 = new string(
                            Enumerable.Repeat(chars1, length + (cont / loop))
                                  .Select(s => s[random1.Next(s.Length)])
                                  .ToArray());
                        }
                        nip = "DIR" + result1;
                        if (db.ATPs.Count(x => x.NIP == nip) == 0)
                            nipvalido = true;
                    }
                    nuevoDirec.Nip = nip;

                    db.Directors.Add(nuevoDirec);
                    db.SaveChanges();

                    string nuevousername;
                    var indice = us.UserExtra.Indice;
                    if (indice > 9999)
                    {

                        nuevousername = "D" + indice.ToString();
                    }
                    else
                    {
                        nuevousername = "D" + String.Format("{0:D4}", indice);
                    }
                    ///
                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                    us.UserName = nuevousername;
                    us.LoweredUserName = nuevousername.ToLower();
                    us.UserExtra.Password = "123456";
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);

                }
                catch (DbEntityValidationException dbEx)
                {

                    return Json(false, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _FacturaSchool(Guid id)
        {
            ViewBag.Estados = db.EstadoMXes.ToList();
            return PartialView(db.Schools.Find(id));
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult _FacturaSchool(School school)
        {
            ViewBag.FacEstadoCode = db.EstadoMXes.ToList();

            if (ModelState.IsValid)
            {
                var schoolup = db.Schools.Find(school.SchoolID);
                schoolup.FacRFC = school.FacRFC;
                schoolup.FacRazonSocial = school.FacRazonSocial;
                schoolup.FacCalle = school.FacCalle;
                schoolup.FacNumExt = school.FacNumExt;
                schoolup.FacNumInt = school.FacNumInt;
                schoolup.FacColonia = school.FacColonia;
                schoolup.FacLocalidad = school.FacLocalidad;
                schoolup.FacReferencia = school.FacReferencia;
                schoolup.FacDelegacion = school.FacDelegacion;
                schoolup.FacEstadoCode = school.FacEstadoCode;
                schoolup.FacPais = school.FacPais;
                schoolup.FacCodigoPostal = school.FacCodigoPostal;

                db.SaveChanges();

                return RedirectToAction("Escuela", "Distribuidor");
            }
            return PartialView(school);
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _LicenciaSchool(Guid id)
        {
            ViewBag.idSchool = id;
            var licencia = db.Licencias.AsQueryable();
            ViewBag.Planes = db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0).ToList();
            if (db.Licencias.Where(x => x.IdEscuela == id).Count() > 0)
            {
                licencia = licencia.Where(x => x.IdEscuela == id);
            }
            else
            {
                licencia = null;
            }
            return PartialView(licencia);
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _ContactoSchool(Guid id)
        {
            var contacto = db.SchoolContactoes.AsQueryable();
            if (db.SchoolContactoes.Where(x => x.IdSchool == id).Count() > 0)
            {
                contacto = contacto.Where(x => x.IdSchool == id);
            }
            else
            {
                contacto = null;
            }
            ViewBag.SchoolID = id;
            return PartialView(contacto);
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _deleteContacto(Guid SchoolID, Guid idContacto)
        {
            var Escuela = db.Schools.Find(SchoolID);
            var contacto = db.SchoolContactoes.Find(idContacto);
            Escuela.SchoolContactoes.Remove(contacto);
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _addContacto(Guid SchoolID, string Nombre, string Telefono, string Correo)
        {
            SchoolContacto newcontacto = new SchoolContacto()
            {
                IdSchoolContacto = Guid.NewGuid(),
                Nombre = Nombre,
                Correo = Correo,
                Telefono = Telefono,
                IdSchool = SchoolID
            };
            db.SchoolContactoes.Add(newcontacto);
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _DetalleEscuela(Guid id)
        {
            var teacherSchool = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Teachers).ToList();

            var studentSchool = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Groups).SelectMany(y => y.Students).ToList();
            var tareaScho = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Groups).SelectMany(t => t.GroupMaterias).SelectMany(x => x.Assigments).Count();
            ViewBag.TareasSchool = tareaScho;

            var totalEx = 0;
            var alumnos = studentSchool.Where(x => x.EjerciciosCompletados != 0).Distinct().Count();
            ViewBag.AlumnoCom = alumnos;
            foreach (var algo in studentSchool)
            {
                totalEx += algo.EjerciciosCompletados;
            }
            ViewBag.ExcersiceTotal = totalEx;

            return PartialView(db.Schools.Find(id));
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult EditarEscuela(Guid id)
        {
            return PartialView(db.Schools.Find(id));
        }

        [Authorize(Roles = "Distribuidor")]
        [HttpPost]
        public ActionResult EditarEscuela(School school)
        {
            if (ModelState.IsValid)
            {
                var schoolup = db.Schools.Find(school.SchoolID);
                schoolup.VinculacionExternaType = school.VinculacionExternaType;
                schoolup.IdVinculacionExterna = school.IdVinculacionExterna;

                db.SaveChanges();

                return RedirectToAction("Escuela", "Distribuidor");
            }
            return PartialView(school);
        }

        public JsonResult _ActivarCiclo(Guid IdEscuela)
        {
            var escuela = db.Schools.Find(IdEscuela);
            var licencia = db.Licencias.Where(x => x.IdEscuela == escuela.SchoolID && x.IsActivo == true).FirstOrDefault();
            var grupos = escuela.Groups.Where(x => x.IsActivo == true && x.Activo == true).ToList();
            var mensaje = "";

            try
            {
                var cont = 0;
                foreach (var item in grupos)
                {
                    item.Activo = false;
                    item.IsActivo = false;
                    item.IsCerrado = true;
                    item.FinishDate = DateTime.UtcNow;
                    cont++;
                    db.SaveChanges();
                }
                mensaje += "Se cerrarón " + cont + " Grupos del ciclo 2017. ";

                licencia.IsActivo = false;
                licencia.FechaFinal = DateTime.UtcNow;
                licencia.FechaTermino = DateTime.UtcNow;
                licencia.IsUsing = false;

                db.SaveChanges();

                var plan = db.Plans.Find(licencia.IdPlan);
                var subtotal = licencia.NumAlumnos * plan.Costo;
                var total = subtotal * 1.16;
                var iva = total - subtotal;

                Licencia lic = new Licencia()
                {
                    Id = Guid.NewGuid(),
                    IdEscuela = escuela.SchoolID,
                    IdPlan = plan.IdPlan,
                    NumAlumnos = licencia.NumAlumnos,
                    FechaRegistro = DateTime.Now,
                    FechaFinal = new DateTime(2018, 08, 31),
                    FechaAprobacion = DateTime.UtcNow,
                    FechaInicio = DateTime.UtcNow,
                    IsAprobado = true,
                    IsUsing = true,
                    IsPrueba = true,
                    StandBy = false,
                    IsActivo = true,
                    CostoAlumno = plan.Costo,
                    CostoAlumnoPlan = plan.Costo,
                    DuracionPlan = plan.Duracion,
                    NombrePlan = plan.Nombre,
                    DescripcionPlan = plan.Descripcion,
                    CicloEscolar = 2018
                };
                db.Licencias.Add(lic);
                db.SaveChanges();

                //OrdenCompra
                OrdenCompra ordnew = new OrdenCompra()
                {
                    Id = Guid.NewGuid(),
                    IdEscuela = escuela.SchoolID,
                    IdDistribuidor = userid,
                    FechaRegistro = DateTime.Now,
                    IdLicencia = lic.Id,
                    SubTotal = subtotal.Value,
                    IVA = iva.Value,
                    Total = total.Value,
                    Estatus = 1,
                    Facturado = false,
                    ReqFactura = false
                    //Estatus = 1
                };
                db.OrdenCompras.Add(ordnew);
                db.SaveChanges();

                //OdenCompraProducto
                OrdenCompraProducto prodnew = new OrdenCompraProducto()
                {
                    Id = Guid.NewGuid(),
                    IdOrdenCompra = ordnew.Id,
                    IdPlan = plan.IdPlan,
                    Cantidad = licencia.NumAlumnos.Value,
                    CostoUnitario = plan.Costo,
                    Concepto = plan.Nombre,
                    Total = subtotal.Value,
                    Unidad = "Licencia Provicional",
                    FechaRegistro = DateTime.Now
                };
                db.OrdenCompraProductoes.Add(prodnew);

                db.SaveChanges();

                if (escuela.IdDirector.HasValue)
                {
                    var director = db.Directors.Where(x => x.DirectorID == escuela.IdDirector).FirstOrDefault();
                    LicenciaUsuario newlicuser = new LicenciaUsuario()
                    {
                        Id = Guid.NewGuid(),
                        IdUsuario = director.UserID,
                        IdLicencia = lic.Id,
                        FechaRegistro = DateTime.Now,
                        IsActivo = true,
                        IsPrueba = lic.IsPrueba
                    };
                    db.LicenciaUsuarios.Add(newlicuser);
                    db.SaveChanges();
                    //db.sofia_licencia_AgregarLicenciaEscuelaUsuarios(lic.Id, escuela.SchoolID, lic.IsPrueba);
                    var profesores = escuela.Teachers;
                    foreach (var item in profesores)
                    {
                        LicenciaUsuario newlicuser2 = new LicenciaUsuario()
                        {
                            Id = Guid.NewGuid(),
                            IdUsuario = item.UserId,
                            IdLicencia = lic.Id,
                            FechaRegistro = DateTime.Now,
                            IsActivo = true,
                            IsPrueba = lic.IsPrueba
                        };
                        db.LicenciaUsuarios.Add(newlicuser2);
                        db.SaveChanges();
                    }

                }
                mensaje += "se agregó, correctamente la licencia provicional del ciclo 2018 para la escuela " + escuela.Name;
                var datos = new
                {
                    correcto = true,
                    mensaje = mensaje
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var datos = new
                {
                    correcto = false,
                    mensaje = "ha ocurrido un error"
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult _CancelarLicencia(Guid idEscuela)
        {
            var escuela = db.Schools.Find(idEscuela);
            var licencia = db.Licencias.Where(x => x.IdEscuela == escuela.SchoolID && x.IsActivo == true).FirstOrDefault();
            var grupos = escuela.Groups.Where(x => x.IsActivo == true && x.Activo == true).ToList();
            var mensaje = "";

            try
            {
                licencia.IsActivo = false;
                licencia.FechaFinal = DateTime.UtcNow;
                licencia.FechaTermino = DateTime.UtcNow;
                licencia.IsUsing = false;

                db.SaveChanges();

                var licuser = licencia.LicenciaUsuarios;
                var conuser = 0;
                foreach(var user in licuser)
                {
                    user.FechaTermino = DateTime.UtcNow;
                    user.IsActivo = false;
                    conuser++;
                    db.SaveChanges();
                }
                mensaje += "Se finalizarón " + conuser + " Licencias de los usuarios. ";

                var cont = 0;
                foreach (var item in grupos)
                {
                    item.Activo = false;
                    item.IsActivo = false;
                    item.IsCerrado = true;
                    item.FinishDate = DateTime.UtcNow;
                    cont++;
                    db.SaveChanges();
                }
                mensaje += "Se cerrarón " + cont + " Grupos del ciclo 2017. ";
                var datos = new
                {
                    correcto = true,
                    mensaje = mensaje
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var datos = new
                {
                    correcto = false,
                    mensaje = "ha ocurrido un error"
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize(Roles = "Distribuidor")]
        public ActionResult Directores(int? page, Guid? EscuelasDis, int? Nivelescolar)
        {
            ViewBag.NivelEscolar = Extensions.ToSelectListDisplayName(typeof(EnumNivelEscolar), Nivelescolar.ToString());
            ViewBag.EscuelasDis = db.Schools.Where(x => x.DistribuidorID == userid).ToList().Select(y =>
              new SelectListItem
              {
                  Value = y.SchoolID.ToString(),
                  Text = y.Name,
                  Selected = (y.SchoolID == EscuelasDis)
              }).OrderBy(x => x.Text);

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Schools.Where(x => x.DistribuidorID == userid).AsQueryable();
            List<Director> listdir = new List<Director>();

            if (Nivelescolar.HasValue)
            {
                v = v.Where(x => x.Scholar_Level == Nivelescolar);
            }
            if (EscuelasDis.HasValue)
            {
                v = v.Where(x => x.SchoolID == EscuelasDis);
            }

            foreach (var dir in v)
            {
                var director = db.Directors.Where(x => x.DirectorID == dir.IdDirector).FirstOrDefault();
                if (director != null)
                {
                    listdir.Add(director);
                }
            }
            if (listdir.Count() != 0)
            {
                return View(listdir.ToList().ToPagedList(pageNumber, pageSize));
            }
            else
            {
                return View(listdir.ToList().ToPagedList(pageNumber, pageSize));
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarDirector()
        {
            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.DistribuidorID == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdDirector == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasDirector = listaEscuela.ToList();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarDirector(RegisterModelDirector model)
        {
            IMembershipService MembershipService = new AccountMembershipService();
            if (true)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 10)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                var usuariotemporal = "usuariotemporal" + result;
                MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    try
                    {
                        Roles.AddUserToRole(usuariotemporal, "Director");

                        MembershipUser user = Membership.GetUser(usuariotemporal, false);
                        Guid userid = (Guid)user.ProviderUserKey;


                        DateTime fecha = DateTime.MinValue;
                        fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                        aspnet_Users us = db.aspnet_Users.Find(userid);
                        us.UserExtra = new UserExtra();
                        us.UserExtra.Birthday = fecha;
                        us.UserExtra.Name = model.Name;
                        us.UserExtra.LastNameP = model.LastNameP;
                        us.UserExtra.LastNameM = model.LastNameM;
                        us.UserExtra.Password = model.Password;
                        us.UserExtra.SexoCode = (int)model.Sexo;
                        us.UserExtra.Correo = model.Email;
                        us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                        Director nuevoDirec = new Director()
                        {
                            DirectorID = Guid.NewGuid(),
                            UserID = userid,

                        };


                        var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var random1 = new Random();
                        bool nipvalido = false;
                        String nip = "DIR";
                        var length = 5;
                        var loop = 100000;
                        var cont = 0;
                        var result1 = new string(
                            Enumerable.Repeat(chars1, length)
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                        while (!nipvalido)
                        {
                            cont++;
                            if (cont > loop)
                            {
                                result1 = new string(
                                Enumerable.Repeat(chars1, length + (cont / loop))
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                            }
                            nip = "DIR" + result1;
                            if (db.ATPs.Count(x => x.NIP == nip) == 0)
                                nipvalido = true;
                        }
                        nuevoDirec.Nip = nip;

                        db.Directors.Add(nuevoDirec);
                        db.SaveChanges();

                        var school = db.Schools.Find(model.SchoolID);
                        school.IdDirector = nuevoDirec.DirectorID;
                        Licencia checa = db.Licencias.Where(x => x.IdEscuela == school.SchoolID).OrderByDescending(y => y.FechaRegistro).FirstOrDefault();
                        if (checa != null)
                        {
                            LicenciaUsuario newlic = new LicenciaUsuario()
                            {
                                Id = Guid.NewGuid(),
                                IdUsuario = userid,
                                IdLicencia = checa.Id,
                                FechaRegistro = DateTime.Now,
                                IsPrueba = checa.IsPrueba
                            };
                            db.LicenciaUsuarios.Add(newlic);
                            db.SaveChanges();
                        }

                        string nuevousername;
                        var indice = us.UserExtra.Indice;
                        if (indice > 9999)
                        {

                            nuevousername = "D" + indice.ToString();
                        }
                        else
                        {
                            nuevousername = "D" + String.Format("{0:D4}", indice);
                        }
                        ///
                        var usuarioMembership = Membership.GetUser(usuariotemporal);
                        us.UserName = nuevousername;
                        us.LoweredUserName = nuevousername.ToLower();
                        us.UserExtra.Password = "123456";
                        db.SaveChanges();

                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }

                    return RedirectToAction("Directores", "Distribuidor");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.DistribuidorID == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdDirector == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasDirector = listaEscuela.ToList();

            return View(model);
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _DesligarDirector(Guid s, Guid d)
        {
            var director = db.Directors.Where(x => x.DirectorID == d).FirstOrDefault();
            var school = db.Schools.Find(s);
            director.Schools.Remove(school);
            
            director.SchoolID = null;
            school.IdDirector = null;

            try
            {
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult Asesores(int? page, string nombre)
        {
            ViewBag.NombreFilter = nombre;

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Distribuidors.Where(x=>x.IdDistribuidor == userid).AsQueryable();

            if (!string.IsNullOrEmpty(nombre))
            {
                v = v.Where(x => x.aspnet_Users.UserExtra.Name.Contains(nombre) || x.aspnet_Users.UserExtra.LastNameP.Contains(nombre));
            }

            return View(v.OrderBy(x => x.aspnet_Users.UserExtra.LastNameP).ThenBy(x=>x.aspnet_Users.UserExtra.Name).ToList().ToPagedList(pageNumber, pageSize));
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarAsesor()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarAsesor(RegisterModelDistribuidor model)
        {
            var distribuidor = db.Distribuidors.Find(userid);
            if (true)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 10)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                var usuariotemporal = "usuariotemporal" + result;

                var contrasena = "123456";
                MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, contrasena, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    try
                    {
                        Roles.AddUserToRole(usuariotemporal, "Asesor");

                        MembershipUser user = Membership.GetUser(usuariotemporal, false);
                        Guid userids = (Guid)user.ProviderUserKey;


                        DateTime fecha = DateTime.MinValue;
                        fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                        aspnet_Users us = db.aspnet_Users.Find(userids);
                        us.UserExtra = new UserExtra();
                        us.UserExtra.Birthday = fecha;
                        us.UserExtra.Name = model.Name;
                        us.UserExtra.LastNameP = model.LastNameP;
                        us.UserExtra.LastNameM = model.LastNameM;
                        us.UserExtra.Password = contrasena;
                        us.UserExtra.SexoCode = (int)model.Sexo;
                        us.UserExtra.Correo = model.Email;
                        us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Distribuidor;

                        Distribuidor nuevoDistri = new Distribuidor
                        {
                            Id = userids,
                            IdDistribuidor = distribuidor.Id,
                            Estado = distribuidor.Estado,
                            RazonSocial = distribuidor.RazonSocial,
                            RFC = distribuidor.RFC,
                            Calle = distribuidor.Calle,
                            NumInt = distribuidor.NumInt,
                            NumExt = distribuidor.NumExt,
                            Colonia = distribuidor.Colonia,
                            Referencia = distribuidor.Referencia,
                            Localidad = distribuidor.Localidad,
                            Pais = distribuidor.Pais,
                            CP = distribuidor.CP,
                            IdPais = distribuidor.IdPais,
                            IdEstado = distribuidor.IdEstado
                        };

                        var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var random2 = new Random();

                        bool nipvalido = false;
                        String nip = "AS";
                        var length = 5;
                        var loop = 100000;
                        var cont = 0;
                        var result2 = new string(
                            Enumerable.Repeat(chars2, length)
                                      .Select(s => s[random2.Next(s.Length)])
                                      .ToArray());
                        while (!nipvalido)
                        {
                            cont++;
                            if (cont > loop)
                            {
                                result = new string(
                                Enumerable.Repeat(chars2, length + (cont / loop))
                                      .Select(s => s[random2.Next(s.Length)])
                                      .ToArray());
                            }
                            nip = "AS" + result2;
                            if (db.Distribuidors.Count(x => x.NIP == nip) == 0)
                                nipvalido = true;
                        }
                        nuevoDistri.NIP = result2;

                        db.Distribuidors.Add(nuevoDistri);
                        db.SaveChanges();

                        string nuevousername;
                        var indice = us.UserExtra.Indice;
                        if (indice > 9999)
                        {

                            nuevousername = "AS" + indice.ToString();
                        }
                        else
                        {
                            nuevousername = "AS" + String.Format("{0:D4}", indice);
                        }
                        var usuarioMembership = Membership.GetUser(usuariotemporal);
                        us.UserName = nuevousername;
                        us.LoweredUserName = nuevousername.ToLower();
                        db.SaveChanges();
                        return RedirectToAction("Asesores", "Distribuidor");
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }

            }

            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult Coordinadores(int? page, string nombre)
        {
            ViewBag.NombreFilter = nombre;

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Coordinadors.Where(x=>x.IdDistribuidor == userid).AsQueryable();

            if (!string.IsNullOrEmpty(nombre))
            {
                v = v.Where(x => x.aspnet_Users.UserExtra.Name.Contains(nombre) || x.aspnet_Users.UserExtra.LastNameP.Contains(nombre));
            }

            return View(v.OrderBy(x => x.aspnet_Users.UserExtra.LastNameP).ThenBy(x => x.aspnet_Users.UserExtra.Name).ToList().ToPagedList(pageNumber, pageSize));
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult _ChangeSchool()
        {
            var coordinadores = db.Coordinadors.Where(x=>x.IdDistribuidor == userid).Count();
            var escuelas = db.Schools.Where(x => x.DistribuidorID == userid).Count();
            
            if (escuelas >= 0)
            {
                var escuelaList = db.Schools.Where(x => x.DistribuidorID == userid && x.IdCoordinador == null).ToList();
                ViewBag.SchoolList = escuelaList.OrderBy(x=>x.Name).ToList();
            }
            else
            {
                ViewBag.SchoolList = null;
            }

            if (coordinadores >= 0)
            {
                return PartialView(db.Coordinadors.Where(x => x.IdDistribuidor == userid).ToList());
            }
            else
            {
                return PartialView();
            }

        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _ChangeSchools(Guid IdCoordinador, Guid SchoolID)
        {
            School sc = db.Schools.Find(SchoolID);
            var coodinador = db.Coordinadors.Find(IdCoordinador);

            if (sc != null && coodinador != null)
            {
                sc.IdCoordinador = coodinador.Id;
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _GetSchoolCoor(Guid id)
        {
            if (db.Schools.Where(x=>x.IdCoordinador == id).Count() == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var schools = db.Schools.Where(x => x.IdCoordinador == id).Select(x => new
                {
                    Id = x.SchoolID,
                    Nombre = x.Name,
                    Clave = x.ClaveOficial,
                    Dependencia = ((EnumDependenciaEscuela)x.Dependencia).ToString(),
                    Escolaridad = ((EnumNivelEscolar)x.Scholar_Level).ToString(),
                    IdCoor = x.IdCoordinador
                });
                return Json(schools, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public JsonResult _DeleteSchoolCoordinador(Guid Id)
        {
            School sp = db.Schools.Find(Id);
            sp.IdCoordinador = null;
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarCoodinador()
        {
            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.DistribuidorID == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdCoordinador == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasCoor = listaEscuela.ToList();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult AgregarCoodinador(RegisterModelCoordinador model)
        {
            var distribuidor = db.Distribuidors.FirstOrDefault(x => x.Id == userid);
            IMembershipService MembershipService = new AccountMembershipService();
            if (true)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 10)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                var usuariotemporal = "usuariotemporal" + result;
                MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    try
                    {
                        Roles.AddUserToRole(usuariotemporal, "Coordinador");

                        MembershipUser user = Membership.GetUser(usuariotemporal, false);
                        Guid userid = (Guid)user.ProviderUserKey;


                        DateTime fecha = DateTime.MinValue;
                        fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                        aspnet_Users us = db.aspnet_Users.Find(userid);
                        us.UserExtra = new UserExtra();
                        us.UserExtra.Birthday = fecha;
                        us.UserExtra.Name = model.Name;
                        us.UserExtra.LastNameP = model.LastNameP;
                        us.UserExtra.LastNameM = model.LastNameM;
                        us.UserExtra.Password = model.Password;
                        us.UserExtra.SexoCode = (int)model.Sexo;
                        us.UserExtra.Correo = model.Email;
                        us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                        Coordinador nuevocoor = new Coordinador()
                        {
                            Id = userid,
                            EstadoCode = 1,
                            IdDistribuidor = distribuidor.Id
                        };
                        
                        db.Coordinadors.Add(nuevocoor);
                        db.SaveChanges();

                        var school = db.Schools.Find(model.SchoolID);
                        school.IdCoordinador = nuevocoor.Id;
                        
                        string nuevousername;
                        var indice = us.UserExtra.Indice;
                        if (indice > 9999)
                        {

                            nuevousername = "C" + indice.ToString();
                        }
                        else
                        {
                            nuevousername = "C" + String.Format("{0:D4}", indice);
                        }
                        ///
                        var usuarioMembership = Membership.GetUser(usuariotemporal);
                        us.UserName = nuevousername;
                        us.LoweredUserName = nuevousername.ToLower();
                        us.UserExtra.Password = "123456";
                        db.SaveChanges();

                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }

                    return RedirectToAction("Coordinadores", "Distribuidor");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.DistribuidorID == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdCoordinador == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasDirector = listaEscuela.ToList();

            return View(model);
        }

        [Authorize(Roles = "Distribuidor")]
        //public JsonResult _updateschool2()
        //{
        //    var estado = db.Estadoes.Find(new Guid("C4262843-AACD-48BD-929F-7D594D738CB9"));
        //    var sinprocesar = db.xxxxDirectorPrimarias.Where(x=>x.Procesado == false).OrderBy(x=>x.zonaescolar).ToList();
        //    var contando = 0;

        //    foreach(var item in sinprocesar)
        //    {
        //        var sectorss = Int32.Parse(item.zonaescolar);

        //        var sector = db.Sectors.Where(x => x.IdEstado == estado.ID && x.Sector1 == sectorss).FirstOrDefault();
        //        if(sector != null)
        //        {
        //            var zonass = Int32.Parse(item.sector);
        //            Zona newzona = new Zona()
        //            {
        //                Id = Guid.NewGuid(),
        //                Zona1 = zonass,
        //                IdSector = sector.Id,
        //                Clave = "28FIZ",
        //                Tipo = 2,
        //                Nivel = 2,
        //                EstadoCode = estado.Codigo,
        //                IdEstado = estado.ID
        //            };
        //            db.Zonas.Add(newzona);
        //            db.SaveChanges();

        //            var escuela = db.Schools.Where(x => x.ClaveOficial == item.cct).FirstOrDefault();
        //            if (escuela != null)
        //            {
        //                escuela.IdZona = newzona.Id;
        //                db.SaveChanges();
        //            }
        //            //var modificar = db.xxxxDirectorPrimarias.SingleOrDefault(x=>x.ID == item.ID);
        //            //modificar.Procesado = true;
        //            //db.SaveChanges();
        //            contando++;
        //        }

        //    }
        //    var datos = new
        //    {
        //        correcto = true,
        //        mensaje = "Se agregarón " + contando + "Zonas"
        //    };
        //    return Json(datos, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult _updateschool2()
        //{
        //    var iddistribuidor = new Guid("52e826bb-ff22-493c-8c8a-bc8d5f197c01");

        //    //var idescuelas = db.Licencias.Where(x => x.IdEscuela != null && x.FechaFinal == new DateTime(2019, 08, 31)).Select(x => x.IdEscuela).ToList();
        //    Guid[] IdEscuelas = new Guid[] { new Guid("86C0FDEF-E94A-41D9-AE6F-037422CF49A1") };
        //    var mensaje = "";

        //    foreach (var idescuela in IdEscuelas)
        //    {
        //        var escuela = db.Schools.Find(idescuela);
        //        //var licencia = db.Licencias.Where(x => x.IdEscuela == escuela.SchoolID).FirstOrDefault();

        //        try
        //        {

        //            var plan = db.Plans.Find(/*licencia.IdPlan*/ new Guid("CCD5E13A-640C-4E07-BEA9-EE2D17E2BBE4") /*new Guid("c433e314-9998-43f8-97d5-427f2df17491")*/);
        //            var subtotal = /*licencia.NumAlumnos*/  190 * plan.Costo;
        //            var total = subtotal * 1.16;
        //            var iva = total - subtotal;

        //            Licencia lic = new Licencia()
        //            {
        //                Id = Guid.NewGuid(),
        //                IdEscuela = escuela.SchoolID,
        //                IdPlan = plan.IdPlan,
        //                NumAlumnos = /*licencia.NumAlumnos*/ 190,
        //                FechaRegistro = DateTime.Now,
        //                FechaFinal = new DateTime(2019, 07, 31),
        //                FechaAprobacion = DateTime.UtcNow,
        //                FechaInicio = DateTime.UtcNow,
        //                IsAprobado = true,
        //                IsUsing = true,
        //                IsPrueba = false,
        //                StandBy = false,
        //                IsActivo = true,
        //                CostoAlumno = plan.Costo,
        //                CostoAlumnoPlan = plan.Costo,
        //                DuracionPlan = plan.Duracion,
        //                NombrePlan = plan.Nombre,
        //                DescripcionPlan = plan.Descripcion,
        //                CicloEscolar = 2018
        //            };
        //            db.Licencias.Add(lic);
        //            db.SaveChanges();

        //            //OrdenCompra
        //            OrdenCompra ordnew = new OrdenCompra()
        //            {
        //                Id = Guid.NewGuid(),
        //                IdEscuela = escuela.SchoolID,
        //                IdDistribuidor = userid,
        //                FechaRegistro = DateTime.Now,
        //                IdLicencia = lic.Id,
        //                SubTotal = subtotal,
        //                IVA = iva,
        //                Total = total,
        //                Estatus = 1,
        //                Facturado = false,
        //                ReqFactura = false
        //                //Estatus = 1
        //            };
        //            db.OrdenCompras.Add(ordnew);
        //            db.SaveChanges();

        //            //OdenCompraProducto
        //            OrdenCompraProducto prodnew = new OrdenCompraProducto()
        //            {
        //                Id = Guid.NewGuid(),
        //                IdOrdenCompra = ordnew.Id,
        //                IdPlan = plan.IdPlan,
        //                Cantidad = /*licencia.NumAlumnos.Value*/90,
        //                CostoUnitario = plan.Costo,
        //                Concepto = plan.Nombre,
        //                Total = subtotal,
        //                Unidad = "Licencia",
        //                FechaRegistro = DateTime.Now
        //            };
        //            db.OrdenCompraProductoes.Add(prodnew);

        //            db.SaveChanges();

        //            //db.sofia_licencia_AgregarLicenciaDirectorEscuela(lic.Id, escuela.SchoolID);
        //            //db.sofia_licencia_AgregarLicenciaProfesoresEscuela(lic.Id, escuela.SchoolID);
        //            //db.sofia_licencia_AgregarLicenciaProfesoresGpoActivos(lic.Id, escuela.SchoolID);
        //            //db.sofia_licencia_AgregarLicenciaAlumnosGpoActivos(lic.Id, escuela.SchoolID);

        //            mensaje += "se agregó, correctamente la licencia del ciclo 2018 para la escuela " + escuela.Name;

        //        }
        //        catch (Exception er)
        //        {
        //            mensaje = "ha ocurrido un error en la escuela " + er.Message;


        //        }
        //    }
        //    var datos = new
        //    {
        //        correcto = true,
        //        mensaje = mensaje
        //    };
        //    return Json(datos, JsonRequestBehavior.AllowGet);

        //}
        //public JsonResult _updateschool2()
        //{
        //    var iddistribuidor = new Guid("36d80362-ddf1-413f-93ed-169330fdbb5a");

        //    var escuela = db.Schools.Where(x => x.DistribuidorID == iddistribuidor).ToList();
        //    var i = 0;
        //    foreach(var item in escuela)
        //    {
        //        var lic = db.Licencias.Where(x => x.IdEscuela == item.SchoolID && x.FechaFinal > DateTime.Now).FirstOrDefault();
        //        var nolic = db.LicenciaUsuarios.Where(x => x.IdLicencia == lic.Id && x.aspnet_Users.UserExtra.TipoUsuario == 3).Count();

        //        try
        //        {
        //            lic.NumAlumnos = nolic;
        //            db.SaveChanges();
        //            i++;
        //        }
        //        catch
        //        {


        //        }

        //    }

        //    var datos = new
        //    {
        //        correcto = true,
        //        mensaje = "se actualizaron " + i + " licencias"
        //    };
        //    return Json(datos, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult _updateschool2()
        {
            var escuelas = db.Schools.ToList();
            var i = 0;

            foreach(var item in escuelas)
            {
                var distribuidor = db.Distribuidors.Where(x=>x.Id == item.IdUser).FirstOrDefault();

                if(distribuidor != null)
                {
                    item.DistribuidorID = distribuidor.Id;
                    i++;
                    db.SaveChanges();
                }
            }

            var datos = new
            {
                correcto = true,
                mensaje = "se actualizaron " + i + " escuelas"
            };
            return Json(datos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Excel_Asignadas1x1()
        {
            var fechainicio = new DateTime(2018, 12, 01, 00, 00, 01);
            var fechafinal = new DateTime(2018, 12, 31, 23, 59, 59);
            //var fechainicio = new DateTime(2018, 09, 01, 00, 00, 01);
            //var fechafinal = DateTime.UtcNow;
            List<RetornoActividad1x1> regresa = new List<RetornoActividad1x1>();
            //var IdEscuelas = db.Schools.Where(x => x.DistribuidorID == new Guid("36d80362-ddf1-413f-93ed-169330fdbb5a") && x.City.Contains("VICTORIA")).Select(x => x.SchoolID).ToList();
            Guid[] IdEscuelas = new Guid[] { new Guid("991e6aa0-4869-475e-b468-143ec930c81b"), new Guid("cd939828-4f2a-463b-a6f6-e7712d952411"), new Guid("b8320993-bd3b-4b16-b75d-3c7217955017"),
            new Guid("13324f41-623b-4225-896e-49529803e38b"), new Guid("b1678bb7-cd8d-462b-a502-545ea2b1dcbc"), new Guid("617daa5e-1ebf-4639-b948-9a759ea864eb"),
            new Guid("87f2bb61-7bfd-4e72-9304-f78a2512ccbc"), new Guid("8b194779-3d2e-44ec-824e-019e98339acb"), new Guid("e1587cc9-a523-46c0-a03a-388a5ed2653e"),
            new Guid("ce7440f8-f615-4228-af89-ca6c6968a4f6"), new Guid("607ee0e9-4382-4886-91d7-e001cc22cc96"), new Guid("fe70bb42-333f-42ba-86ed-ec5271f59cfa")};

            foreach (var id in IdEscuelas)
            {
                var escprim = db.Schools.Find(id);
                var grupos = escprim.Groups.Where(x => x.IsActivo == true && x.Activo == true && x.TeacherID != null);
                foreach (var item in grupos)
                {
                    var NombreProfesor = item.Teacher.aspnet_Users.UserExtra.Name + " " + item.Teacher.aspnet_Users.UserExtra.LastNameP + " " + item.Teacher.aspnet_Users.UserExtra.LastNameM;
                    var NombreGrupo = item.NivelEscolar.Grado + "" + item.Description + " ";
                    var NombreEscuela = escprim.Name;
                    var asignadas = item.Assigments.Where(x => x.TeacherID == item.TeacherID && x.CreationDate >= fechainicio && x.CreationDate <= fechafinal && x.IdGroup == item.GroupID).ToList();

                    foreach (var actividad in asignadas)
                    {
                        RetornoActividad1x1 nuevo = new RetornoActividad1x1()
                        {
                            NombreEscuela = NombreEscuela,
                            NombreProfesor = NombreProfesor,
                            NombreGrupo = NombreGrupo,
                            NombreActividad = actividad.Name,
                            NumeroEjercicios = actividad.Exercises.Count()
                        };
                        regresa.Add(nuevo);
                    }
                }
            }

            var p = regresa.OrderBy(x => x.NombreEscuela).ThenByDescending(x => x.NombreProfesor);
            GridView gv = new GridView();
            gv.DataSource = p.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + "AsignadasPeru_" + DateTime.Now.ToShortDateString() + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return View();
        }

        public ActionResult Excel_Creadas()
        {
            var fechainicio = new DateTime(2018, 12, 01, 00, 00, 01);
            var fechafinal = new DateTime(2018, 12, 31, 23, 59, 59);
            List<retorno> regresa = new List<retorno>();
            Guid[] IdEscuelas = new Guid[] { new Guid("991e6aa0-4869-475e-b468-143ec930c81b"), new Guid("cd939828-4f2a-463b-a6f6-e7712d952411"), new Guid("b8320993-bd3b-4b16-b75d-3c7217955017"),
            new Guid("13324f41-623b-4225-896e-49529803e38b"), new Guid("b1678bb7-cd8d-462b-a502-545ea2b1dcbc"), new Guid("617daa5e-1ebf-4639-b948-9a759ea864eb"),
            new Guid("87f2bb61-7bfd-4e72-9304-f78a2512ccbc"), new Guid("8b194779-3d2e-44ec-824e-019e98339acb"), new Guid("e1587cc9-a523-46c0-a03a-388a5ed2653e"),
            new Guid("ce7440f8-f615-4228-af89-ca6c6968a4f6"), new Guid("607ee0e9-4382-4886-91d7-e001cc22cc96"), new Guid("fe70bb42-333f-42ba-86ed-ec5271f59cfa")};

            foreach (var id in IdEscuelas)
            {
                var escprim = db.Schools.Find(id);
                var grupos = escprim.Groups.Where(x => x.IsActivo == true && x.Activo == true && x.TeacherID != null);
                foreach (var item in grupos)
                {
                    var NombreProfesor = item.Teacher.aspnet_Users.UserExtra.Name + " " + item.Teacher.aspnet_Users.UserExtra.LastNameP + " " + item.Teacher.aspnet_Users.UserExtra.LastNameM;
                    var NombreGrupo = item.NivelEscolar.Grado + "" + item.Description + " ";
                    var NombreEscuela = escprim.Name;
                    var creadas = db.Exercises.Where(x => x.UserID == item.Teacher.UserId && x.NivelEscolar.Grado == item.NivelEscolar.Grado).Count();

                    retorno nuevo = new retorno()
                    {
                        NombreEscuela = NombreEscuela,
                        NombreProfesor = NombreProfesor,
                        NombreGrupo = NombreGrupo,
                        ActividadesAsignadas = creadas,
                    };
                    regresa.Add(nuevo);
                }
            }

            var p = regresa.OrderBy(x => x.NombreEscuela).ThenByDescending(x => x.NombreProfesor);
            GridView gv = new GridView();
            gv.DataSource = p.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + "CreadasPeru_" + DateTime.Now.ToShortDateString() + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return View();
        }

        public ActionResult Excel_AlumnosPuntosPromedio()
        {
            Guid[] IdEscuelas = new Guid[] { new Guid("991e6aa0-4869-475e-b468-143ec930c81b"), new Guid("cd939828-4f2a-463b-a6f6-e7712d952411"), new Guid("b8320993-bd3b-4b16-b75d-3c7217955017"),
            new Guid("13324f41-623b-4225-896e-49529803e38b"), new Guid("b1678bb7-cd8d-462b-a502-545ea2b1dcbc"), new Guid("617daa5e-1ebf-4639-b948-9a759ea864eb"),
            new Guid("87f2bb61-7bfd-4e72-9304-f78a2512ccbc"), new Guid("8b194779-3d2e-44ec-824e-019e98339acb"), new Guid("e1587cc9-a523-46c0-a03a-388a5ed2653e"),
            new Guid("ce7440f8-f615-4228-af89-ca6c6968a4f6"), new Guid("607ee0e9-4382-4886-91d7-e001cc22cc96"), new Guid("fe70bb42-333f-42ba-86ed-ec5271f59cfa")};
            List<RetornoGrupo> regresa = new List<RetornoGrupo>();
            var fechainicio = new DateTime(2018, 11, 01, 00, 00, 01);
            var fechafinal = new DateTime(2018, 11, 30, 23, 59, 59);
            //var fechainicio = new DateTime(2018, 09, 01, 00, 00, 01);
            //var fechafinal = DateTime.UtcNow;
            //var IdEscuelas = db.Schools.Where(x => x.DistribuidorID == new Guid("36d80362-ddf1-413f-93ed-169330fdbb5a") && x.City.Contains("VICTORIA")).Select(x => x.SchoolID).ToList();

            foreach (var id in IdEscuelas)
            {
                var escprim = db.Schools.Find(id);
                var grupos = escprim.Groups.Where(x => x.IsActivo == true && x.Activo == true && x.TeacherID != null);
                foreach (var item in grupos)
                {

                    var NombreProfesor = item.Teacher.aspnet_Users.UserExtra.Name + " " + item.Teacher.aspnet_Users.UserExtra.LastNameP + " " + item.Teacher.aspnet_Users.UserExtra.LastNameM;
                    var NombreGrupo = item.NivelEscolar.Grado + "" + item.Description + " ";
                    var estudiantes = item.Students;
                    RetornoGrupo newret = new RetornoGrupo()
                    {
                        NombreEscuela = escprim.Name,
                        NombreProfesor = NombreProfesor,
                        NombreGrupo = NombreGrupo,
                        EjerciciosAsignados = 0,
                        EjerciciosLibres = 0,
                        EjerciciosTotales = 0,
                        EjerciciosTask = 0,
                        Puntos = 0
                    };

                    foreach (var estu in estudiantes)
                    {
                        var datos = db.Estadistica_diaria_estudiantes_ejercicios.Where(x => x.IdEstudiante == estu.StudentID && x.FechaInicio >= fechainicio && x.FechaFinal <= fechafinal).ToList();
                        newret.EjerciciosAsignados += (datos.Select(x => (int?)x.TotalActividad).Sum() ?? 0);
                        newret.EjerciciosLibres += (datos.Select(x => (int?)x.TotalLibres).Sum() ?? 0);
                        newret.EjerciciosTotales += (datos.Select(x => (int?)x.TotalEjercicios).Sum() ?? 0);
                        newret.EjerciciosTask += (datos.Select(x => (int?)x.TotalTask).Sum() ?? 0);
                        newret.Puntos += (datos.Select(x => (int?)x.TotalPuntos).Sum() ?? 0);
                    }
                    regresa.Add(newret);
                }
            }

            var p = regresa.OrderBy(x => x.NombreEscuela).ThenByDescending(x => x.NombreGrupo);
            GridView gv = new GridView();
            gv.DataSource = p.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + "Excel_UsoGrupos_Peru_" + DateTime.Now.ToShortDateString() + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return View();
        }

        //---------------- CONF. USUARIO ---------------//

        [Authorize(Roles = "Distribuidor")]
        public ActionResult MiPerfil()
        {
            MembershipUser user = Membership.GetUser(User.Identity.Name);
            var ue = db.UserExtras.Where(q => q.UserID == userid).FirstOrDefault();
            var distribuidor = db.Distribuidors.Find(userid);

            IEnumerable<SelectListItem> Months = System.Globalization.DateTimeFormatInfo
               .CurrentInfo
               .MonthNames
               .Where(x => !string.IsNullOrEmpty(x))
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName.ToUpper()
               });

            var Days = new List<SelectListItem>();
            for (var i = 1; i <= 31; i++)
            {
                Days.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            var Ano = new List<SelectListItem>();
            for (var i = DateTime.Today.Year; i >= DateTime.Today.Year - 80; i--)
            {
                Ano.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }


            var Model = new DistribucionSofia.Models.ViewModel_EditUserExtra_D()
            {
                Id = ue.UserID,
                Name = ue.Name,
                LastNameP = ue.LastNameP,
                LastNameM = ue.LastNameM,
                Ano = (ue.Birthday.HasValue) ? ue.Birthday.Value.Year : 2015,
                Dia = (ue.Birthday.HasValue) ? ue.Birthday.Value.Day : 0,
                Mes = (ue.Birthday.HasValue) ? ue.Birthday.Value.Month : 0,
                FechaNacimiento = (ue.Birthday.HasValue) ? ue.Birthday.Value : DateTime.Now,
                Email = ue.aspnet_Users.aspnet_Membership.Email,
                Telefono = ue.Telefono,
                Username = Membership.GetUser().UserName,
                IsFacebook = (ue.IsFacebook.HasValue) ? ue.IsFacebook.Value : false,
                RazonSocial = distribuidor.RazonSocial,
                RFC = distribuidor.RFC,
                Calle = distribuidor.Calle,
                NumInt = distribuidor.NumInt,
                NumExt = distribuidor.NumExt,
                Colonia = distribuidor.Colonia,
                Localidad = distribuidor.Localidad,
                Referencia = distribuidor.Referencia,
                DELEGACION = distribuidor.DELEGACION,
                Estado = distribuidor.Estado,
                Pais = distribuidor.Pais,
                CP = distribuidor.CP
            };

            ViewBag.EstadosMX = db.EstadoMXes.ToList();
            ViewBag.Ano = new SelectList(Ano, "Value", "Text", Model.Ano);
            ViewBag.Dia = new SelectList(Days, "Value", "Text", Model.Dia);
            ViewBag.Mes = new SelectList(Months, "Value", "Text", Model.Mes);

            return View(Model);
        }

        [HttpPost]
        [Authorize(Roles = "Distribuidor")]
        public ActionResult MiPerfil(DistribucionSofia.Models.ViewModel_EditUserExtra_D Model)
        {
            if (ModelState.IsValid)
            {
                var distribuidor = db.Distribuidors.Find(userid);
                var ue = db.UserExtras.First(x => x.UserID == Model.Id);
                ue.Birthday = new DateTime(Model.Ano, Model.Mes, Model.Dia);
                ue.Telefono = Model.Telefono;
                ue.Name = Model.Name;
                ue.LastNameP = Model.LastNameP;
                ue.LastNameM = Model.LastNameM;
                distribuidor.RFC = Model.RFC;
                distribuidor.RazonSocial = Model.RazonSocial;
                distribuidor.Calle = Model.Calle;
                distribuidor.NumInt = Model.NumInt;
                distribuidor.NumExt = Model.NumExt;
                distribuidor.Colonia = Model.Colonia;
                distribuidor.Localidad = Model.Localidad;
                distribuidor.Referencia = Model.Referencia;
                distribuidor.DELEGACION = Model.DELEGACION;
                distribuidor.Estado = (Model.Estado.HasValue ? Model.Estado.Value : 1);
                distribuidor.Pais = Model.Pais;
                distribuidor.CP = Model.CP;
                db.SaveChanges();

                //Email
                var user = Membership.GetUser(Model.Id);
                user.Email = Model.Email;
                Membership.UpdateUser(user);

                //Username, cambiar si no es de fb y si son diferentes usernames
                var currentUserName = ue.aspnet_Users.UserName;
                var newUserName = Model.Username;
                if (Model.IsFacebook == false && currentUserName.ToLower() != newUserName.ToLower())
                {
                    using (var context = new Entities())
                    {
                        // Get the membership record from the database
                        var currentUserNameLowered = currentUserName.ToLower();
                        var membershipUser = context.aspnet_Users
                            .Where(u => u.LoweredUserName == currentUserNameLowered)
                            .FirstOrDefault();

                        if (membershipUser != null)
                        {
                            // Ensure that the new user name is not already being used
                            string newUserNameLowered = newUserName.ToLower();
                            if (!context.aspnet_Users.Any(u => u.LoweredUserName == newUserNameLowered))
                            {
                                membershipUser.UserName = newUserName;
                                membershipUser.LoweredUserName = newUserNameLowered;
                                context.SaveChanges();

                                //Cambiar sesion
                                var membershipService = new AccountMembershipService();
                                if (membershipService.ValidateUser(newUserName, ue.Password))
                                {
                                    Session["Usuario"] = newUserName;
                                    FormsAuthentication.SetAuthCookie(newUserName, false);
                                }

                            }

                        }
                    }
                }
                return RedirectToAction("MiPerfil", "Distribuidor");
            }

            IEnumerable<SelectListItem> Months = System.Globalization.DateTimeFormatInfo
               .CurrentInfo
               .MonthNames
               .Where(x => !string.IsNullOrEmpty(x))
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName.ToUpper()
               });

            var Days = new List<SelectListItem>();
            for (var i = 1; i <= 31; i++)
            {
                Days.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            var Ano = new List<SelectListItem>();
            for (var i = DateTime.Today.Year; i >= DateTime.Today.Year - 80; i--)
            {
                Ano.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            ViewBag.Ano = new SelectList(Ano, "Value", "Text", Model.Ano);
            ViewBag.Dia = new SelectList(Days, "Value", "Text", Model.Dia);
            ViewBag.Mes = new SelectList(Months, "Value", "Text", Model.Mes);
            return View(Model);
        }

        public ActionResult PagoTarjeta(Guid id)
        {
            var orden = db.OrdenCompras.Find(id);
            var escuela = db.Schools.Find(orden.IdEscuela);
            ViewBag.NombreEscuela = escuela.Name;
            ViewBag.IdOrden = orden;

            var model = new ViewModel_Pagar(){
                 Id = orden.Id,
                 IdEscuela = orden.IdEscuela,
                 IdProfesor = orden.IdProfesor,
                 IdDistribuidor = orden.IdDistribuidor,
                 IdAsesor = orden.IdAsesor,
                 FechaRegistro = orden.FechaRegistro,
                 SubTotal = orden.SubTotal,
                 IVA = orden.IVA,
                 Total = orden.Total,
                 TotalPagado = orden.TotalPagado,
                 Estatus = orden.Estatus,
                 Facturado = orden.Facturado
            };
            
                        
            return PartialView(model);            
        }

        public ActionResult PagoTarjeta(ViewModel_Pagar model)
        {
            var distribuidor = db.Distribuidors.Find(userid);
            var orden = db.OrdenCompras.Find(model.Id);
            Conekta.ApiKey = "key_q9P9vwKzEgQbjoizTBnyzw";

            var token_id = Request.Form["conektaTokenId"];


            try
            {
                if (distribuidor.IdCustomer == "" && token_id != "")
                {
                    string nombre = distribuidor.aspnet_Users.UserExtra.Name + " " + distribuidor.aspnet_Users.UserExtra.LastNameP + " " + distribuidor.aspnet_Users.UserExtra.LastNameM;
                    string email = distribuidor.aspnet_Users.UserExtra.Correo;
                    string telefono = "+52" + distribuidor.aspnet_Users.UserExtra.Telefono;

                    JObject newcustomer = JObject.Parse("{'name':'" + nombre + "'," +
                                            "'email':'" + email + "'," +
                                            "'phone':'" + telefono + "'}");

                    Customer addcus = Customer.create(newcustomer);

                    distribuidor.IdCustomer = addcus.id.ToString();

                    db.SaveChanges();

                    JObject valid_payment_method = JObject.Parse("{'description':'Sofia Premium'," +
                                             "'reference_id':'" + distribuidor.IdCustomer + "'," +
                                             "'amount':"+model.Total+"," +
                                             "'currency':'MXN'," +
                                             "'card':'"+token_id+"'}");

                    Charge charge = Charge.create(valid_payment_method);
                }
                else if (token_id != "") 
                {
                    JObject valid_payment_method = JObject.Parse("{'description':'Sofia Premium'," +
                                             "'reference_id':'" + distribuidor.IdCustomer + "'," +
                                             "'amount':" + model.Total + "," +
                                             "'currency':'MXN'," +
                                             "'card':'" + token_id + "'}");

                    Charge charge = Charge.create(valid_payment_method);
                }

                return PartialView();
            }
            catch (Exception ex)
            {
                return PartialView();
            }
            
        }
                
        public JsonResult _crearCliente()
        {
            var distribuidor = db.Distribuidors.Where(x=>x.IdDistribuidor == null);
            Conekta.ApiKey = "key_q9P9vwKzEgQbjoizTBnyzw";
            //Conekta.Version = "2.0.0";

            try
            {
                foreach (var item in distribuidor)
                {
                    Customer newcustomer = new Customer();
                    newcustomer.name = item.aspnet_Users.UserExtra.Name + " " + item.aspnet_Users.UserExtra.LastNameP + " " + item.aspnet_Users.UserExtra.LastNameM;
                    newcustomer.email = item.aspnet_Users.UserExtra.Correo;
                    newcustomer.phone = "+52" + item.aspnet_Users.UserExtra.Telefono;
                                       
                    //Customer addcus = new Customer().create(newcustomer.ToString());

                    //item.IdCustomer = addcus.id.ToString();

                    db.SaveChanges();
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************
        [Authorize]
        [DistribucionSofia.Utils.Audit]
        public ActionResult ChangePassword()
        {
            var idUser = (Guid)Membership.GetUser().ProviderUserKey;
            var userextra = db.UserExtras.FirstOrDefault(x => x.UserID == idUser);
            
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);
                if (user.ChangePassword(model.OldPassword.ToLower(), model.NewPassword.ToLower()) /*MembershipService.ChangePassword(User.Identity.Name, model.OldPassword.ToLower(), model.NewPassword.ToLower())*/)
                {
                    ////EN CASO DE ALMACENAR CONTRASEÑA
                    //MembershipUser user = Membership.GetUser(User.Identity.Name, false);
                    Guid userid = (Guid)user.ProviderUserKey;
                    db.UserExtras.Where(e => e.UserID == userid).First().Password = model.NewPassword.ToLower();
                    db.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "La contraseña actual es incorrecta o la nueva contraseña invalida");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }
        
    }
}