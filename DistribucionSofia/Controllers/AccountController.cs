﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using DistribucionSofia.Models;
using System.Net;
using System.Data.Entity.Validation;
using System.Diagnostics;
using EncryptStringSample;
using System.Net.Mail;
using System.Globalization;
using System.Text;
using System.IO;
using System.Dynamic;
using DistribucionSofia.Models.Enum;
using Newtonsoft.Json;
using WebMatrix.WebData;

namespace DistribucionSofia.Controllers
{
    
    public class AccountController : Controller
    {
        private Entities db = new Entities();

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {

            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************
        public JsonResult IsEmail_Disponible(string Email)
        {
            return Json(db.aspnet_Membership.Count(x => x.Email == Email) == 0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogOn()
        {
            var config = db.Configuracions.First();

            return View();
        }

        public JsonResult LogOnJson(string user, string pass)
        {
            LogOnModel model = new LogOnModel();
            model.UserName = user;
            model.Password = pass;
            model.RememberMe = true;

            if (MembershipService.ValidateUser(user, pass))
            {
                var caca = Roles.GetRolesForUser(user);
                FormsService.SignIn(model.UserName, model.RememberMe);
                return Json(caca, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [DistribucionSofia.Utils.Audit]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Membership.GetUser(model.UserName);
                if (MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    Session["Usuario"] = model.UserName;

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    //FormsService.SignIn(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }

                else if (MembershipService.ValidateUser(model.UserName, model.Password.ToLower()))
                {
                    Session["Usuario"] = model.UserName;
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    //FormsService.SignIn(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Usuario o contraseña incorrecta.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            FormsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        public JsonResult _LogOut()
        {
            FormsService.SignOut();

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult _IsUserPasswordSafe(string NewPassword, string Password)
        {
            NewPassword = string.IsNullOrEmpty(NewPassword) ? Password : NewPassword;
            try
            {
                var malasContras = new List<string>();
                var jsonfilepath = Path.Combine(DistribucionSofia.Utils.FileUtils.GetDir() + "JSONs\\", Path.GetFileName("DiccionarioContrasenasMalas.json"));
                using (StreamReader r = new StreamReader(jsonfilepath))
                {
                    string json = r.ReadToEnd();
                    malasContras = JsonConvert.DeserializeObject<List<string>>(json);
                }

                if (malasContras.Contains(NewPassword.ToLower()))
                    return Json("La contraseña que escribiste es muy fácil de adivinar.", JsonRequestBehavior.AllowGet);

                /*
                if(User.Identity.IsAuthenticated){
                    var userid = (Guid)Membership.GetUser().ProviderUserKey;
                    var userextra = db.UserExtras.FirstOrDefault(x => x.UserID == userid);
                    if(userextra != null)
                    {
                        if (!string.IsNullOrEmpty(userextra.Name))
                        {
                            var name = System.Text.RegularExpressions.Regex.Replace(userextra.Name, @"\s+", "").ToLower();
                            if (NewPassword.ToLower().Contains(name))
                                return Json("Tu nombre no puede ser parte de la contraseña.", JsonRequestBehavior.AllowGet);

                            var apellidos = (userextra.LastNameP + " " + userextra.LastNameM).ToLower().Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var app in apellidos)
                            {
                                if (NewPassword.ToLower().Contains(app))
                                    return Json("Ningún apellido puede ser parte de la contraseña.", JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                }*/


                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception err)
            {
                return Json("Error al validar contraseña: " + err.Message, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
