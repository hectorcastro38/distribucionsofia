﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Web.Routing;
using DistribucionSofia.Models;
using DistribucionSofia.Models.Enum;
using PagedList;

namespace DistribucionSofia.Controllers
{
    public class AsesorController : Controller
    {
        private Entities db = new Entities();
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        Guid userid = (Guid)Membership.GetUser().ProviderUserKey;
        private aspnet_Users user = new aspnet_Users();

        protected override void Initialize(RequestContext requestContext)
        {

            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        //------------------------------- INTERFAZ ---------------------------//
        [Authorize(Roles = "Asesor")]
        public ActionResult Index()
        {
            ViewBag.EscuelasDis = db.Schools.Where(x => x.IdAsesor == userid).Count();
            ViewBag.OrdenCompraDis = db.OrdenCompras.Where(x => x.IdAsesor == userid && x.Estatus != 2).Count();
            ViewBag.OrdenAdesaprobadaDis = db.OrdenCompras.Where(x => x.IdAsesor == userid && x.Estatus == 0).Count();

            ViewBag.EscuelasExpira = db.Schools.Where(x => x.IdAsesor == userid).SelectMany(y => y.Licencias).Where(r => r.FechaFinal.Value.Month == (DateTime.Now.Month + 1) && r.FechaFinal.Value.Year == DateTime.Now.Year).Select(u => u.IdEscuela).Distinct().Count();

            return View();
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult NuevaOrden(int tipo, Guid? EscuelaID)
        {
            using (var db = new Entities())
            {
                var distribuidor = db.Distribuidors.Find(userid);
                var distribuidorG = distribuidor.Distribuidor2;
                ViewBag.Escuelas = db.Schools.Where(x => x.IdAsesor == distribuidor.Id).ToList().Select(x => new
                {
                    Text = x.Name + " (" + x.ClaveOficial + ")",
                    Value = x.SchoolID
                });

                ViewBag.Dependencia = Extensions.ToSelectListDisplayName(typeof(EnumDependenciaEscuela), "");

                ViewBag.Paises = db.Pais.Where(x => x.Id == distribuidorG.IdPais).ToList();
                ViewBag.Estados = db.Estadoes.Where(x => x.IdPais == distribuidorG.IdPais).OrderBy(x => x.Codigo).ToList();
                if (tipo == 1)
                {
                    ViewBag.Tipo = "Nueva orden de compra";
                }
                else if (tipo == 2)
                {
                    ViewBag.Tipo = "Agregar escuela nueva";
                }
                else
                {
                    ViewBag.Tipo = "Renovar/Nueva Licencia";
                }
                ViewBag.TipoNum = tipo;
                var cultura = distribuidorG.Pai.LenguajeCodigoes.FirstOrDefault();
                ViewBag.Planes = db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0 && x.IdCultura == cultura.Id).Select(y => new ViewModel_CheckBoxPlan { IdPlan = y.IdPlan, Nombre = y.Nombre, Descripcion = y.Descripcion, Costo = y.Costo, Duracion = y.Duracion, IsChecked = false }).ToList();
                var model = new DistribucionSofia.Models.ViewModel_d_OrdenCompra();
                
                model.PeriodoPrueba = false;
                ViewBag.HasDirector = 0;

                if (EscuelaID.HasValue)
                {
                    var escuela = db.Schools.Find(EscuelaID.Value);

                    model.SchoolID = escuela.SchoolID;
                    model.Name = escuela.Name;
                    model.ClaveOficial = escuela.ClaveOficial;
                    model.Type = escuela.Type;
                    model.Dependencia = escuela.Dependencia;
                    model.Scholar_Level = escuela.Scholar_Level;
                    model.Address = escuela.Address;
                    model.Telephone = escuela.Telephone;
                    model.EstadoCode = escuela.EstadoCode;
                    model.City = escuela.City;
                    model.Province = escuela.Province;
                    model.Country = escuela.Country;
                    model.Timezone = escuela.Timezone;
                    model.Detalle = escuela.Detalle;

                    if (escuela.IdDirector.HasValue)
                    {
                        ViewBag.HasDirector = 1;
                    }
                    else
                    {
                        ViewBag.HasDirector = 0;
                    }


                    model.IsPrueba = escuela.IsPrueba;

                }
                return View(model);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Asesor")]
        public ActionResult NuevaOrden(ViewModel_d_OrdenCompra model, FormCollection fc)
        {
            var valido = ModelState.IsValid;
            Guid idusuario = Guid.Empty;
            try
            {
                var distribuidor = db.Distribuidors.FirstOrDefault(x => x.Id == userid);

                if (distribuidor != null)
                {
                    model.Distribuidor = distribuidor;
                }
                var existe = db.Schools.FirstOrDefault(x => x.SchoolID == model.SchoolID);
                if (existe == null)
                {
                    //step 1
                    School escuelanew = new School();
                    var estado = db.EstadoMXes.Where(x => x.ID == model.EstadoCode).FirstOrDefault();

                    escuelanew.SchoolID = Guid.NewGuid();
                    escuelanew.IdUser = userid;
                    escuelanew.DistribuidorID = distribuidor.IdDistribuidor;
                    escuelanew.IdAsesor = userid;
                    escuelanew.FechaRegistro = DateTime.Now;
                    if (model.Timezone != null)
                    {
                        escuelanew.TimezoneOffset = TimeZoneInfo.FindSystemTimeZoneById(model.Timezone).BaseUtcOffset.Hours;
                    }
                    else
                    {
                        escuelanew.Timezone = db.Configuracions.First().Timezone;
                        escuelanew.TimezoneOffset = db.Configuracions.First().TimezoneOffset;
                    }
                    var estadoli = db.Estadoes.Find(model.IdEstado);

                    escuelanew.Name = model.Name;
                    escuelanew.Type = model.Type;
                    escuelanew.Scholar_Level = model.Scholar_Level;
                    escuelanew.City = model.City;
                    escuelanew.Address = model.Address;
                    escuelanew.Telephone = model.Telephone;
                    escuelanew.ClaveOficial = model.ClaveOficial;
                    escuelanew.IsActive = true;
                    escuelanew.Dependencia = model.Dependencia;
                    escuelanew.Detalle = model.Detalle;
                    escuelanew.Localidad = model.Localidad;
                    escuelanew.EstadoCode = estadoli.Codigo;
                    escuelanew.Province = estadoli.Nombre;
                    escuelanew.Country = estadoli.Pai.Nombre; 
                    escuelanew.IdEstado = model.IdEstado;
                    escuelanew.IdPais = model.IdPais;
                    escuelanew.IsDistribuidor = false;

                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    bool nipvalido = false;
                    String nip = "E";
                    var length = 5;
                    var loop = 100000;
                    var cont = 0;
                    var result = new string(
                        Enumerable.Repeat(chars, length)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                    while (!nipvalido)
                    {
                        cont++;
                        if (cont > loop)
                        {
                            result = new string(
                            Enumerable.Repeat(chars, length + (cont / loop))
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());
                        }
                        nip = "E" + result;
                        if (db.Schools.Count(x => x.Nip == nip) == 0)
                            nipvalido = true;
                    }
                    escuelanew.Nip = nip;

                    if (!model.omiteContacto)
                    {
                        //registro director 
                        IMembershipService MembershipService = new AccountMembershipService();
                        if (true)
                        {
                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var random2 = new Random();
                            var result2 = new string(
                                Enumerable.Repeat(chars2, 10)
                                          .Select(s => s[random2.Next(s.Length)])
                                          .ToArray());
                            var usuariotemporal = "usuariotemporal" + result2;
                            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.correoDir);

                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    Roles.AddUserToRole(usuariotemporal, "Director");

                                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                                    Guid useridDir = (Guid)user.ProviderUserKey;

                                    DateTime fecha = DateTime.MinValue;
                                    fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                                    aspnet_Users us = db.aspnet_Users.Find(useridDir);
                                    us.UserExtra = new UserExtra();
                                    us.UserExtra.Birthday = fecha;
                                    us.UserExtra.Name = model.nombreDir;
                                    us.UserExtra.LastNameP = model.apellidoP;
                                    us.UserExtra.LastNameM = model.apellidoM;
                                    us.UserExtra.Password = "123456";
                                    us.UserExtra.SexoCode = (int)model.sexo;
                                    us.UserExtra.Correo = model.correoDir;
                                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                                    Director nuevoDirec = new Director()
                                    {
                                        DirectorID = Guid.NewGuid(),
                                        UserID = useridDir,

                                    };


                                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var random1 = new Random();
                                    bool nipvalido3 = false;
                                    String nip3 = "DIR";
                                    var length3 = 5;
                                    var loop3 = 100000;
                                    var cont3 = 0;
                                    var result1 = new string(
                                        Enumerable.Repeat(chars1, length3)
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                    while (!nipvalido3)
                                    {
                                        cont3++;
                                        if (cont3 > loop3)
                                        {
                                            result1 = new string(
                                            Enumerable.Repeat(chars1, length3 + (cont3 / loop3))
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                        }
                                        nip3 = "DIR" + result1;
                                        if (db.Directors.Count(x => x.Nip == nip3) == 0)
                                            nipvalido3 = true;
                                    }
                                    nuevoDirec.Nip = nip3;

                                    db.Directors.Add(nuevoDirec);
                                    db.SaveChanges();

                                    idusuario = nuevoDirec.UserID;

                                    string nuevousername;
                                    var indice = us.UserExtra.Indice;
                                    if (indice > 9999)
                                    {

                                        nuevousername = "D" + indice.ToString();
                                    }
                                    else
                                    {
                                        nuevousername = "D" + String.Format("{0:D4}", indice);
                                    }
                                    ///
                                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = "123456";
                                    db.SaveChanges();

                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                            }
                        }
                    }

                    var tipo = "";
                    //step 2
                    if (model.PeriodoPrueba)
                    {
                        if (model.Scholar_Level == 2)//Primaria
                        {
                            model.IdPlan = new Guid("ccd5e13a-640c-4e07-bea9-ee2d17e2bbe4");
                        }
                        else //Secundaria
                        {
                            model.IdPlan = new Guid("BE7207F6-0915-4439-A07C-9BDB305A4C53");
                        }
                        model.periodo = 6;
                        model.Cantidad = model.Cantidad2;
                        tipo = "Licencia Prueba";
                        escuelanew.IsPrueba = true;
                    }
                    else
                    {
                        model.periodo = 12;
                        tipo = "Licencia";
                    }
                    var plan = db.Plans.Find(model.IdPlan);
                    var subtotal = model.Cantidad * plan.Costo;
                    var total = (subtotal * 1.16);
                    var iva = total - subtotal;

                    if (model.PeriodoPrueba)
                    {
                        subtotal = 0;
                        total = 0;
                        iva = 0;
                    }

                    Licencia lic = new Licencia()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdPlan = plan.IdPlan,
                        NumAlumnos = model.Cantidad,
                        FechaRegistro = DateTime.Now,
                        FechaFinal = DateTime.Now.AddMonths(model.periodo),
                        IsAprobado = false,
                        IsUsing = false,
                        IsPrueba = model.IsPrueba,
                        StandBy = false,
                        IsActivo = false,
                        CostoAlumno = plan.Costo,
                        CostoAlumnoPlan = plan.Costo,
                        DuracionPlan = plan.Duracion,
                        NombrePlan = plan.Nombre,
                        DescripcionPlan = plan.Descripcion
                    };
                    db.Licencias.Add(lic);
                    db.SaveChanges();

                    if (!model.omiteContacto)
                    {
                        if (idusuario != Guid.Empty)
                        {
                            LicenciaUsuario newlic = new LicenciaUsuario()
                            {
                                Id = Guid.NewGuid(),
                                IdUsuario = idusuario,
                                IdLicencia = lic.Id,
                                FechaRegistro = DateTime.Now,
                                IsPrueba = lic.IsPrueba
                            };
                            db.LicenciaUsuarios.Add(newlic);
                            db.SaveChanges();
                        }
                    }

                    //OrdenCompra
                    OrdenCompra ordnew = new OrdenCompra()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdDistribuidor = distribuidor.Distribuidor2.Id,
                        IdAsesor = userid,
                        FechaRegistro = DateTime.Now,
                        IdLicencia = lic.Id,
                        SubTotal = subtotal,
                        IVA = iva,
                        Total = total,
                        Estatus = 0
                    };
                    if (model.aplicaFactura)
                    {
                        ordnew.Facturado = false;
                        ordnew.ReqFactura = true;
                    }
                    else
                    {
                        ordnew.ReqFactura = false;
                    }
                    if (model.PeriodoPrueba)
                    {
                        ordnew.Estatus = 1;
                    }
                    db.OrdenCompras.Add(ordnew);
                    db.SaveChanges();

                    //OdenCompraProducto
                    OrdenCompraProducto prodnew = new OrdenCompraProducto()
                    {
                        Id = Guid.NewGuid(),
                        IdOrdenCompra = ordnew.Id,
                        IdPlan = plan.IdPlan,
                        Cantidad = model.Cantidad,
                        CostoUnitario = plan.Costo,
                        Concepto = plan.Nombre,
                        Total = subtotal,
                        Unidad = tipo,
                        FechaRegistro = DateTime.Now
                    };
                    db.OrdenCompraProductoes.Add(prodnew);

                    db.SaveChanges();

                    return RedirectToAction("Orden", "Asesor", new { idorden = ordnew.Id });
                }
                else
                {
                    //step 1
                    School escuelanew = db.Schools.Find(model.SchoolID);

                    escuelanew.IsPrueba = model.IsPrueba;

                    if (!model.omiteContacto)
                    {
                        //registro director 
                        IMembershipService MembershipService = new AccountMembershipService();
                        if (true)
                        {
                            var chars2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var random2 = new Random();
                            var result2 = new string(
                                Enumerable.Repeat(chars2, 10)
                                          .Select(s => s[random2.Next(s.Length)])
                                          .ToArray());
                            var usuariotemporal = "usuariotemporal" + result2;
                            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.correoDir);

                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                try
                                {
                                    Roles.AddUserToRole(usuariotemporal, "Director");

                                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                                    Guid useridDir = (Guid)user.ProviderUserKey;

                                    DateTime fecha = DateTime.MinValue;
                                    fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                                    aspnet_Users us = db.aspnet_Users.Find(useridDir);
                                    us.UserExtra = new UserExtra();
                                    us.UserExtra.Birthday = fecha;
                                    us.UserExtra.Name = model.nombreDir;
                                    us.UserExtra.LastNameP = model.apellidoP;
                                    us.UserExtra.LastNameM = model.apellidoM;
                                    us.UserExtra.Password = "123456";
                                    us.UserExtra.SexoCode = (int)model.sexo;
                                    us.UserExtra.Correo = model.correoDir;
                                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                                    Director nuevoDirec = new Director()
                                    {
                                        DirectorID = Guid.NewGuid(),
                                        UserID = useridDir,

                                    };


                                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var random1 = new Random();
                                    bool nipvalido3 = false;
                                    String nip3 = "DIR";
                                    var length3 = 5;
                                    var loop3 = 100000;
                                    var cont3 = 0;
                                    var result1 = new string(
                                        Enumerable.Repeat(chars1, length3)
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                    while (!nipvalido3)
                                    {
                                        cont3++;
                                        if (cont3 > loop3)
                                        {
                                            result1 = new string(
                                            Enumerable.Repeat(chars1, length3 + (cont3 / loop3))
                                                  .Select(s => s[random1.Next(s.Length)])
                                                  .ToArray());
                                        }
                                        nip3 = "DIR" + result1;
                                        if (db.Directors.Count(x => x.Nip == nip3) == 0)
                                            nipvalido3 = true;
                                    }
                                    nuevoDirec.Nip = nip3;

                                    db.Directors.Add(nuevoDirec);
                                    db.SaveChanges();

                                    string nuevousername;
                                    var indice = us.UserExtra.Indice;
                                    if (indice > 9999)
                                    {

                                        nuevousername = "D" + indice.ToString();
                                    }
                                    else
                                    {
                                        nuevousername = "D" + String.Format("{0:D4}", indice);
                                    }
                                    ///
                                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                                    us.UserName = nuevousername;
                                    us.LoweredUserName = nuevousername.ToLower();
                                    us.UserExtra.Password = "123456";

                                    escuelanew.IdDirector = nuevoDirec.DirectorID;

                                    db.SaveChanges();

                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                            }
                        }
                    }

                    //step 2

                    var tipo = "";
                    if (model.PeriodoPrueba)
                    {
                        if (model.Scholar_Level == 2)//Primaria
                        {
                            model.IdPlan = new Guid("ccd5e13a-640c-4e07-bea9-ee2d17e2bbe4");
                        }
                        else //Secundaria
                        {
                            model.IdPlan = new Guid("BE7207F6-0915-4439-A07C-9BDB305A4C53");
                        }
                        model.periodo = 6;
                        model.Cantidad = model.Cantidad2;
                        tipo = "Licencia Prueba";
                        escuelanew.IsPrueba = true;
                    }
                    else
                    {
                        model.periodo = 12;
                        tipo = "Licencia";
                    }
                    var plan = db.Plans.Find(model.IdPlan);
                    var subtotal = model.Cantidad * plan.Costo;
                    var total = subtotal * 1.16;
                    var iva = total - subtotal;

                    if (model.PeriodoPrueba)
                    {
                        subtotal = 0;
                        total = 0;
                        iva = 0;
                    }

                    Licencia lic = new Licencia()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdPlan = plan.IdPlan,
                        NumAlumnos = model.Cantidad,
                        FechaRegistro = DateTime.Now,
                        FechaFinal = DateTime.Now.AddMonths(model.periodo),
                        IsAprobado = false,
                        IsUsing = false,
                        IsPrueba = model.IsPrueba,
                        StandBy = false,
                        IsActivo = false,
                        CostoAlumno = plan.Costo,
                        CostoAlumnoPlan = plan.Costo,
                        DuracionPlan = plan.Duracion,
                        NombrePlan = plan.Nombre,
                        DescripcionPlan = plan.Descripcion
                    };
                    if (model.PeriodoPrueba)
                    {
                        lic.IsAprobado = true;
                        lic.FechaAprobacion = DateTime.Now;
                        lic.FechaInicio = DateTime.Now;
                    }
                    db.Licencias.Add(lic);
                    db.SaveChanges();


                    //OrdenCompra
                    OrdenCompra ordnew = new OrdenCompra()
                    {
                        Id = Guid.NewGuid(),
                        IdEscuela = escuelanew.SchoolID,
                        IdDistribuidor = distribuidor.Distribuidor2.Id,
                        IdAsesor = userid,
                        FechaRegistro = DateTime.Now,
                        IdLicencia = lic.Id,
                        SubTotal = subtotal,
                        IVA = iva,
                        Total = total,
                        Estatus = 0
                    };
                    if (model.aplicaFactura)
                    {
                        ordnew.Facturado = false;
                        ordnew.ReqFactura = true;
                    }
                    else
                    {
                        ordnew.ReqFactura = false;
                    }
                    if (model.PeriodoPrueba)
                    {
                        ordnew.Estatus = 1;
                    }
                    db.OrdenCompras.Add(ordnew);
                    db.SaveChanges();

                    //OdenCompraProducto
                    OrdenCompraProducto prodnew = new OrdenCompraProducto()
                    {
                        Id = Guid.NewGuid(),
                        IdOrdenCompra = ordnew.Id,
                        IdPlan = plan.IdPlan,
                        Cantidad = model.Cantidad,
                        CostoUnitario = plan.Costo,
                        Concepto = plan.Nombre,
                        Total = subtotal,
                        Unidad = tipo,
                        FechaRegistro = DateTime.Now
                    };
                    db.OrdenCompraProductoes.Add(prodnew);

                    if (escuelanew.IdDirector.HasValue)
                    {
                        var director = db.Directors.Where(x => x.DirectorID == escuelanew.IdDirector).FirstOrDefault();
                        LicenciaUsuario newlicuser = new LicenciaUsuario()
                        {
                            Id = Guid.NewGuid(),
                            IdUsuario = director.UserID,
                            IdLicencia = lic.Id,
                            FechaRegistro = DateTime.Now,
                            IsActivo = true,
                            IsPrueba = lic.IsPrueba
                        };
                        db.LicenciaUsuarios.Add(newlicuser);
                        db.SaveChanges();
                        db.sofia_licencia_AgregarLicenciaEscuelaUsuarios(lic.Id, escuelanew.SchoolID, lic.IsPrueba);
                    }

                    return RedirectToAction("Orden", "Asesor", new { idorden = ordnew.Id });
                }
            }
            catch (DbEntityValidationException e)
            {
                var errores = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        errores.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                //throw;
                ViewBag.Errores = errores;

                return View(model);
            }
        }

        public JsonResult _GetJsonCity(int EstadoName)
        {
            var city = db.MunicipioMXes.Where(x => x.EstadoMX.ID == EstadoName).Select(x => new
            {
                Nombre = x.Nombre + " (" + x.cve_mun + ")"
            });
            return Json(city.OrderBy(x => x.Nombre), JsonRequestBehavior.AllowGet);
        }

        public JsonResult _GetSchool(Guid SchoolID)
        {
            using (var dbe = new Entities())
            {
                var escuela = dbe.Schools.Find(SchoolID);

                var licenciaValida = dbe.Licencias.Where(x => x.IdEscuela == escuela.SchoolID).OrderByDescending(y => y.FechaFinal).FirstOrDefault();
                var licenciaPrueba = dbe.Licencias.Where(x => x.IdEscuela == escuela.SchoolID && x.IsPrueba == true).Count();
                var mensaje = "";
                var mensajetipo = 0;

                if (licenciaValida != null)
                {
                    if (licenciaValida.FechaFinal > DateTime.Now)
                    {
                        mensaje = "La escuela cuenta con licencia hasta " + licenciaValida.FechaFinal;
                        mensajetipo = 0;
                    }
                    else
                    {
                        if (licenciaPrueba != 0)
                        {

                        }
                        mensaje = "La licencia caduco " + licenciaValida.FechaFinal;
                        mensajetipo = 1;
                    }
                }
                else
                {
                    mensaje = "La escuela no tiene licencia";
                    mensajetipo = 2;
                }

                var datos = new
                {
                    Id = escuela.SchoolID,
                    Nombre = escuela.Name,
                    Clave = escuela.ClaveOficial,
                    Tipo = escuela.Type,
                    Escolaridad = escuela.Scholar_Level,
                    Direccion = escuela.Address,
                    Telefono = escuela.Telephone,
                    Estado = escuela.IdEstado,
                    Ciudad = escuela.City,
                    Pais = escuela.IdPais,
                    Horario = escuela.Timezone,
                    Comentarios = escuela.Detalle,
                    Mensaje = mensaje,
                    TipoMensaje = mensajetipo,
                    Prueba = escuela.IsPrueba
                };
                return Json(datos, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult OrdenesRealizadas()
        {
            var distribuidor = db.Distribuidors.Find(userid);

            return View(db.OrdenCompras.Where(x => x.IdAsesor == distribuidor.Id).ToList());
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult _DetalleCompra(Guid id)
        {
            return PartialView(db.OrdenCompras.Find(id));
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult ConsultarPlanes()
        {
            using (var db = new Entities())
            {
                return View(db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0).ToList());
            }
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult Escuela(int? page, int? Dependencia, int? Nivelescolar, string Clave, string Nombre)
        {
            ViewBag.DependenciaFilter = Dependencia;
            ViewBag.NivelescolarFilter = Nivelescolar;
            ViewBag.ClaveFilter = Clave;
            ViewBag.NombreFilter = Nombre;
            ViewBag.Dependencia = Extensions.ToSelectListDisplayName(typeof(EnumDependenciaEscuela), Dependencia.ToString());
            ViewBag.NivelEscolar = Extensions.ToSelectListDisplayName(typeof(EnumNivelEscolar), Nivelescolar.ToString());

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Schools.Where(x => x.IdAsesor == userid).AsQueryable();

            if (Dependencia.HasValue)
            {
                v = v.Where(x => x.Dependencia == Dependencia);
            }
            if (Nivelescolar.HasValue)
            {
                v = v.Where(x => x.Scholar_Level == Nivelescolar);
            }

            if (!string.IsNullOrEmpty(Nombre))
            {
                v = v.Where(x => x.Name.Contains(Nombre));
            }

            if (!string.IsNullOrEmpty(Clave))
            {
                v = v.Where(x => x.ClaveOficial.Contains(Clave));
            }
            var escuelas = db.Schools.Where(x => x.IdAsesor == userid).ToList();
            if (escuelas.Where(x => x.IsDistribuidor == true).Count() > 0)
            {
                ViewBag.AddSchool = true;
            }
            else
            {
                ViewBag.AddSchool = false;
            }
            return View(v.OrderBy(x => x.Name).ThenBy(x => x.ClaveOficial).ThenBy(x => x.Dependencia).ThenBy(x => x.Scholar_Level).ToList().ToPagedList(pageNumber, pageSize));
        }

        [Authorize(Roles = "Asesor")]
        public JsonResult _CreateDirectorDistri(string Name, string LastNameP, string LastNameM, string Email, DateTime Birthday, int Sexo, Guid SchoolID)
        {
            IMembershipService MembershipService = new AccountMembershipService();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            var usuariotemporal = "usuariotemporal" + result;
            MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", Email);

            if (createStatus == MembershipCreateStatus.Success)
            {
                try
                {
                    Roles.AddUserToRole(usuariotemporal, "Director");

                    MembershipUser user = Membership.GetUser(usuariotemporal, false);
                    Guid userid = (Guid)user.ProviderUserKey;

                    aspnet_Users us = db.aspnet_Users.Find(userid);
                    us.UserExtra = new UserExtra();
                    us.UserExtra.Birthday = DateTime.Parse(Birthday.ToString("dd/MM/yyyy"));
                    us.UserExtra.Name = Name;
                    us.UserExtra.LastNameP = LastNameP;
                    us.UserExtra.LastNameM = LastNameM;
                    us.UserExtra.Password = "123456";
                    us.UserExtra.SexoCode = (int)Sexo;
                    us.UserExtra.Correo = Email;
                    us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                    Director nuevoDirec = new Director()
                    {
                        DirectorID = Guid.NewGuid(),
                        UserID = userid,

                    };

                    var school = db.Schools.Find(SchoolID);
                    school.IdDirector = nuevoDirec.DirectorID;

                    Licencia checa = db.Licencias.Where(x => x.IdEscuela == school.SchoolID).OrderByDescending(y => y.FechaRegistro).FirstOrDefault();
                    if (checa != null)
                    {
                        LicenciaUsuario newlic = new LicenciaUsuario()
                        {
                            Id = Guid.NewGuid(),
                            IdUsuario = userid,
                            IdLicencia = checa.Id,
                            FechaRegistro = DateTime.Now
                        };
                        db.LicenciaUsuarios.Add(newlic);
                    }

                    var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random1 = new Random();
                    bool nipvalido = false;
                    String nip = "DIR";
                    var length = 5;
                    var loop = 100000;
                    var cont = 0;
                    var result1 = new string(
                        Enumerable.Repeat(chars1, length)
                                  .Select(s => s[random1.Next(s.Length)])
                                  .ToArray());
                    while (!nipvalido)
                    {
                        cont++;
                        if (cont > loop)
                        {
                            result1 = new string(
                            Enumerable.Repeat(chars1, length + (cont / loop))
                                  .Select(s => s[random1.Next(s.Length)])
                                  .ToArray());
                        }
                        nip = "DIR" + result1;
                        if (db.ATPs.Count(x => x.NIP == nip) == 0)
                            nipvalido = true;
                    }
                    nuevoDirec.Nip = nip;

                    db.Directors.Add(nuevoDirec);
                    db.SaveChanges();

                    string nuevousername;
                    var indice = us.UserExtra.Indice;
                    if (indice > 9999)
                    {

                        nuevousername = "D" + indice.ToString();
                    }
                    else
                    {
                        nuevousername = "D" + String.Format("{0:D4}", indice);
                    }
                    ///
                    var usuarioMembership = Membership.GetUser(usuariotemporal);
                    us.UserName = nuevousername;
                    us.LoweredUserName = nuevousername.ToLower();
                    us.UserExtra.Password = "123456";
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);

                }
                catch (DbEntityValidationException dbEx)
                {

                    return Json(false, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize(Roles = "Asesor")]
        public ActionResult _FacturaSchool(Guid id)
        {
            ViewBag.Estados = db.EstadoMXes.ToList();
            return PartialView(db.Schools.Find(id));
        }

        [HttpPost]
        [Authorize(Roles = "Asesor")]
        public ActionResult _FacturaSchool(School school)
        {
            ViewBag.FacEstadoCode = db.EstadoMXes.ToList();

            if (ModelState.IsValid)
            {
                var schoolup = db.Schools.Find(school.SchoolID);
                schoolup.FacRFC = school.FacRFC;
                schoolup.FacRazonSocial = school.FacRazonSocial;
                schoolup.FacCalle = school.FacCalle;
                schoolup.FacNumExt = school.FacNumExt;
                schoolup.FacNumInt = school.FacNumInt;
                schoolup.FacColonia = school.FacColonia;
                schoolup.FacLocalidad = school.FacLocalidad;
                schoolup.FacReferencia = school.FacReferencia;
                schoolup.FacDelegacion = school.FacDelegacion;
                schoolup.FacEstadoCode = school.FacEstadoCode;
                schoolup.FacPais = school.FacPais;
                schoolup.FacCodigoPostal = school.FacCodigoPostal;

                db.SaveChanges();

                return RedirectToAction("Escuela", "Asesor");
            }
            return PartialView(school);
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult _LicenciaSchool(Guid id)
        {
            ViewBag.idSchool = id;
            var licencia = db.Licencias.AsQueryable();
            ViewBag.Planes = db.Plans.Where(x => x.IsActivo == true && x.IsInterno == false && x.Tipo == 0).ToList();
            if (db.Licencias.Where(x => x.IdEscuela == id).Count() > 0)
            {
                licencia = licencia.Where(x => x.IdEscuela == id);
            }
            else
            {
                licencia = null;
            }
            return PartialView(licencia);
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult _ContactoSchool(Guid id)
        {
            var contacto = db.SchoolContactoes.AsQueryable();
            if (db.SchoolContactoes.Where(x => x.IdSchool == id).Count() > 0)
            {
                contacto = contacto.Where(x => x.IdSchool == id);
            }
            else
            {
                contacto = null;
            }
            ViewBag.SchoolID = id;
            return PartialView(contacto);
        }

        [Authorize(Roles = "Asesor")]
        public JsonResult _deleteContacto(Guid SchoolID, Guid idContacto)
        {
            var Escuela = db.Schools.Find(SchoolID);
            var contacto = db.SchoolContactoes.Find(idContacto);
            Escuela.SchoolContactoes.Remove(contacto);
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Asesor")]
        public JsonResult _addContacto(Guid SchoolID, string Nombre, string Telefono, string Correo)
        {
            SchoolContacto newcontacto = new SchoolContacto()
            {
                IdSchoolContacto = Guid.NewGuid(),
                Nombre = Nombre,
                Correo = Correo,
                Telefono = Telefono,
                IdSchool = SchoolID
            };
            db.SchoolContactoes.Add(newcontacto);
            try
            {
                db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult _DetalleEscuela(Guid id)
        {
            var teacherSchool = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Teachers).ToList();

            var studentSchool = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Groups).SelectMany(y => y.Students).ToList();
            var tareaScho = db.Schools.Where(x => x.SchoolID == id).SelectMany(x => x.Groups).SelectMany(t => t.GroupMaterias).SelectMany(x => x.Assigments).Count();
            ViewBag.TareasSchool = tareaScho;

            var totalEx = 0;
            var alumnos = studentSchool.Where(x => x.EjerciciosCompletados != 0).Distinct().Count();
            ViewBag.AlumnoCom = alumnos;
            foreach (var algo in studentSchool)
            {
                totalEx += algo.EjerciciosCompletados;
            }
            ViewBag.ExcersiceTotal = totalEx;

            return PartialView(db.Schools.Find(id));
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult Directores(int? page, Guid? EscuelasDis, int? Nivelescolar)
        {
            ViewBag.NivelEscolar = Extensions.ToSelectListDisplayName(typeof(EnumNivelEscolar), Nivelescolar.ToString());
            ViewBag.EscuelasDis = db.Schools.Where(x => x.IdAsesor == userid).ToList().Select(y =>
            new SelectListItem
            {
                Value = y.SchoolID.ToString(),
                Text = y.Name,
                Selected = (y.SchoolID == EscuelasDis)
            }).OrderBy(x=>x.Text);

            var rows = 10;
            if (!page.HasValue)
                page = 1;
            int pageSize = rows;
            int pageNumber = (page ?? 1);
            ViewBag.page = page;

            var v = db.Schools.Where(x => x.IdAsesor == userid).AsQueryable();
            List<Director> listdir = new List<Director>();

            if (Nivelescolar.HasValue)
            {
                v = v.Where(x => x.Scholar_Level == Nivelescolar);
            }
            if (EscuelasDis.HasValue)
            {
                v = v.Where(x => x.SchoolID == EscuelasDis);
            }

            foreach (var dir in v)
            {
                var director = db.Directors.Where(x => x.DirectorID == dir.IdDirector).FirstOrDefault();
                if (director != null)
                {
                    listdir.Add(director);
                }
            }
            if (listdir.Count() != 0)
            {
                return View(listdir.ToList().ToPagedList(pageNumber, pageSize));
            }
            else
            {
                return View(listdir.ToList().ToPagedList(pageNumber, pageSize));
            }
        }

        [Authorize(Roles = "Asesor")]
        public ActionResult AgregarDirector()
        {
            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.IdAsesor == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdDirector == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasDirector = listaEscuela.ToList();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Asesor")]
        public ActionResult AgregarDirector(RegisterModelDirector model)
        {
            IMembershipService MembershipService = new AccountMembershipService();
            if (true)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 10)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                var usuariotemporal = "usuariotemporal" + result;
                MembershipCreateStatus createStatus = MembershipService.CreateUser(usuariotemporal, "123456", model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    try
                    {
                        Roles.AddUserToRole(usuariotemporal, "Director");

                        MembershipUser user = Membership.GetUser(usuariotemporal, false);
                        Guid userid = (Guid)user.ProviderUserKey;


                        DateTime fecha = DateTime.MinValue;
                        fecha = DateTime.Parse(model.Dia + "/" + model.Mes + "/" + model.Ano + "");

                        aspnet_Users us = db.aspnet_Users.Find(userid);
                        us.UserExtra = new UserExtra();
                        us.UserExtra.Birthday = fecha;
                        us.UserExtra.Name = model.Name;
                        us.UserExtra.LastNameP = model.LastNameP;
                        us.UserExtra.LastNameM = model.LastNameM;
                        us.UserExtra.Password = model.Password;
                        us.UserExtra.SexoCode = (int)model.Sexo;
                        us.UserExtra.Correo = model.Email;
                        us.UserExtra.TipoUsuario = (int)EnumTipoUsuarioPreregistro.Director;

                        Director nuevoDirec = new Director()
                        {
                            DirectorID = Guid.NewGuid(),
                            UserID = userid,

                        };

                        var school = db.Schools.Find(model.SchoolID);
                        school.IdDirector = nuevoDirec.DirectorID;

                        Licencia checa = db.Licencias.Where(x => x.IdEscuela == school.SchoolID).OrderByDescending(y => y.FechaRegistro).FirstOrDefault();
                        if (checa != null)
                        {
                            LicenciaUsuario newlic = new LicenciaUsuario()
                            {
                                Id = Guid.NewGuid(),
                                IdUsuario = userid,
                                IdLicencia = checa.Id,
                                FechaRegistro = DateTime.Now,
                                IsPrueba = checa.IsPrueba
                            };
                            db.LicenciaUsuarios.Add(newlic);
                            db.SaveChanges();
                        }

                        var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var random1 = new Random();
                        bool nipvalido = false;
                        String nip = "DIR";
                        var length = 5;
                        var loop = 100000;
                        var cont = 0;
                        var result1 = new string(
                            Enumerable.Repeat(chars1, length)
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                        while (!nipvalido)
                        {
                            cont++;
                            if (cont > loop)
                            {
                                result1 = new string(
                                Enumerable.Repeat(chars1, length + (cont / loop))
                                      .Select(s => s[random1.Next(s.Length)])
                                      .ToArray());
                            }
                            nip = "DIR" + result1;
                            if (db.ATPs.Count(x => x.NIP == nip) == 0)
                                nipvalido = true;
                        }
                        nuevoDirec.Nip = nip;

                        db.Directors.Add(nuevoDirec);
                        db.SaveChanges();

                        string nuevousername;
                        var indice = us.UserExtra.Indice;
                        if (indice > 9999)
                        {

                            nuevousername = "D" + indice.ToString();
                        }
                        else
                        {
                            nuevousername = "D" + String.Format("{0:D4}", indice);
                        }
                        ///
                        var usuarioMembership = Membership.GetUser(usuariotemporal);
                        us.UserName = nuevousername;
                        us.LoweredUserName = nuevousername.ToLower();
                        us.UserExtra.Password = "123456";
                        db.SaveChanges();

                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }

                    return RedirectToAction("Directores", "Distribuidor");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            List<School> listaEscuela = new List<School>();
            foreach (var escuela in db.Schools.Where(x => x.DistribuidorID == userid).OrderBy(s => s.Name).ToList())
            {
                if (escuela.IdDirector == null)
                {
                    listaEscuela.Add(escuela);
                }
            }
            ViewBag.EscuelasDirector = listaEscuela.ToList();

            return View(model);
        }


        //---------------- CONF. USUARIO ---------------//

        [Authorize(Roles = "Distribuidor")]
        public ActionResult MiPerfil()
        {
            MembershipUser user = Membership.GetUser(User.Identity.Name);
            var ue = db.UserExtras.Where(q => q.UserID == userid).FirstOrDefault();

            IEnumerable<SelectListItem> Months = System.Globalization.DateTimeFormatInfo
               .CurrentInfo
               .MonthNames
               .Where(x => !string.IsNullOrEmpty(x))
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName.ToUpper()
               });

            var Days = new List<SelectListItem>();
            for (var i = 1; i <= 31; i++)
            {
                Days.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            var Ano = new List<SelectListItem>();
            for (var i = DateTime.Today.Year; i >= DateTime.Today.Year - 80; i--)
            {
                Ano.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }


            var Model = new DistribucionSofia.Models.ViewModel_EditUserExtra_D()
            {
                Id = ue.UserID,
                Name = ue.Name,
                LastNameP = ue.LastNameP,
                LastNameM = ue.LastNameM,
                Ano = (ue.Birthday.HasValue) ? ue.Birthday.Value.Year : 2015,
                Dia = (ue.Birthday.HasValue) ? ue.Birthday.Value.Day : 0,
                Mes = (ue.Birthday.HasValue) ? ue.Birthday.Value.Month : 0,
                FechaNacimiento = (ue.Birthday.HasValue) ? ue.Birthday.Value : DateTime.Now,
                Email = ue.aspnet_Users.aspnet_Membership.Email,
                Telefono = ue.Telefono,
                Username = Membership.GetUser().UserName,
                IsFacebook = (ue.IsFacebook.HasValue) ? ue.IsFacebook.Value : false
            };


            ViewBag.Ano = new SelectList(Ano, "Value", "Text", Model.Ano);
            ViewBag.Dia = new SelectList(Days, "Value", "Text", Model.Dia);
            ViewBag.Mes = new SelectList(Months, "Value", "Text", Model.Mes);

            return View(Model);
        }

        [HttpPost]
        [Authorize(Roles = "Asesor")]
        public ActionResult MiPerfil(DistribucionSofia.Models.ViewModel_EditUserExtra_D Model)
        {
            if (ModelState.IsValid)
            {

                var ue = db.UserExtras.First(x => x.UserID == Model.Id);
                ue.Birthday = new DateTime(Model.Ano, Model.Mes, Model.Dia);
                ue.Telefono = Model.Telefono;
                ue.Name = Model.Name;
                ue.LastNameP = Model.LastNameP;
                ue.LastNameM = Model.LastNameM;
                db.SaveChanges();

                //Email
                var user = Membership.GetUser(Model.Id);
                user.Email = Model.Email;
                Membership.UpdateUser(user);

                //Username, cambiar si no es de fb y si son diferentes usernames
                var currentUserName = ue.aspnet_Users.UserName;
                var newUserName = Model.Username;
                if (Model.IsFacebook == false && currentUserName.ToLower() != newUserName.ToLower())
                {
                    using (var context = new Entities())
                    {
                        // Get the membership record from the database
                        var currentUserNameLowered = currentUserName.ToLower();
                        var membershipUser = context.aspnet_Users
                            .Where(u => u.LoweredUserName == currentUserNameLowered)
                            .FirstOrDefault();

                        if (membershipUser != null)
                        {
                            // Ensure that the new user name is not already being used
                            string newUserNameLowered = newUserName.ToLower();
                            if (!context.aspnet_Users.Any(u => u.LoweredUserName == newUserNameLowered))
                            {
                                membershipUser.UserName = newUserName;
                                membershipUser.LoweredUserName = newUserNameLowered;
                                context.SaveChanges();

                                //Cambiar sesion
                                var membershipService = new AccountMembershipService();
                                if (membershipService.ValidateUser(newUserName, ue.Password))
                                {
                                    Session["Usuario"] = newUserName;
                                    FormsAuthentication.SetAuthCookie(newUserName, false);
                                }

                            }

                        }
                    }
                }
                return RedirectToAction("Index", "Home");
            }

            IEnumerable<SelectListItem> Months = System.Globalization.DateTimeFormatInfo
               .CurrentInfo
               .MonthNames
               .Where(x => !string.IsNullOrEmpty(x))
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName.ToUpper()
               });

            var Days = new List<SelectListItem>();
            for (var i = 1; i <= 31; i++)
            {
                Days.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            var Ano = new List<SelectListItem>();
            for (var i = DateTime.Today.Year; i >= DateTime.Today.Year - 80; i--)
            {
                Ano.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            ViewBag.Ano = new SelectList(Ano, "Value", "Text", Model.Ano);
            ViewBag.Dia = new SelectList(Days, "Value", "Text", Model.Dia);
            ViewBag.Mes = new SelectList(Months, "Value", "Text", Model.Mes);
            return View(Model);
        }

        [Authorize]
        [DistribucionSofia.Utils.Audit]
        public ActionResult ChangePassword()
        {
            var idUser = (Guid)Membership.GetUser().ProviderUserKey;
            var userextra = db.UserExtras.FirstOrDefault(x => x.UserID == idUser);

            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);
                if (user.ChangePassword(model.OldPassword.ToLower(), model.NewPassword.ToLower()) /*MembershipService.ChangePassword(User.Identity.Name, model.OldPassword.ToLower(), model.NewPassword.ToLower())*/)
                {
                    ////EN CASO DE ALMACENAR CONTRASEÑA
                    //MembershipUser user = Membership.GetUser(User.Identity.Name, false);
                    Guid userid = (Guid)user.ProviderUserKey;
                    db.UserExtras.Where(e => e.UserID == userid).First().Password = model.NewPassword.ToLower();
                    db.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "La contraseña actual es incorrecta o la nueva contraseña invalida");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}