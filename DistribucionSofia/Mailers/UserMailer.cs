using Mvc.Mailer;
using DistribucionSofia.Models;

namespace DistribucionSofia.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer 	
	{
		public UserMailer()
		{
			MasterName="_Layout";
		}
		
		public virtual MvcMailMessage Welcome()
		{
            
			//ViewBag.Data = someObject;
			return Populate(x =>
			{
				x.Subject = "Welcome";
				x.ViewName = "Welcome";
				x.To.Add("some-email@example.com");
			});
		}

        public virtual MvcMailMessage Prueba()
        {
            return Populate(x =>
            {
                x.Subject = "Bienvenido";
                x.ViewName = "Prueba";
            });
        }


        public virtual MvcMailMessage Default(string titulo, string contenido)
        {
            ViewBag.Contenido = contenido;
            ViewBag.Titulo = titulo;
            return Populate(x =>
            {
                x.Subject = titulo;
                x.ViewName = "Default";
            });
        }

        public virtual MvcMailMessage NotificacionUsuario(string subject, string email, string usuario)
        {
            ViewBag.Usuario = usuario;
            ViewBag.Subject = subject;
            return Populate(x =>
            {
                x.Subject = "Orden de Compra Sof�a XT";
                x.Body = subject;
                x.ViewName = "Orden";
                x.To.Add(email);
            });
        }

        public virtual MvcMailMessage NotificacionManager(string subject, string email)
        {
            ViewBag.Subject = subject;
            return Populate(x =>
            {
                x.Subject = "Nueva orden";
                x.Body = subject;
                x.ViewName = "Orden";
                x.To.Add(email);
            });
        }

        public virtual MvcMailMessage GoodBye()
        {
            //ViewBag.Data = someObject;
            return Populate(x =>
            {
                x.Subject = "GoodBye";
                x.ViewName = "GoodBye";
                x.To.Add("julio.ibarrae@gmail.com");
            });
        }
    }
}