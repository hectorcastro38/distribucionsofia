using Mvc.Mailer;

namespace DistribucionSofia.Mailers
{ 
    public interface IUserMailer
    {
			MvcMailMessage Welcome();
			MvcMailMessage GoodBye();
	}
}