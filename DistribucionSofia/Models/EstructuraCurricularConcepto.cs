//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EstructuraCurricularConcepto
    {
        public System.Guid Id { get; set; }
        public System.Guid IdEstructuraCurricularDefinicion { get; set; }
        public System.Guid IdDiagnosticoConcepto { get; set; }
    
        public virtual DiagnosticoConcepto DiagnosticoConcepto { get; set; }
        public virtual EstructuraCurricularDefinicion EstructuraCurricularDefinicion { get; set; }
    }
}
