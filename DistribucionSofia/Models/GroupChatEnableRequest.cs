//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GroupChatEnableRequest
    {
        public System.Guid Id { get; set; }
        public System.Guid IdGrupo { get; set; }
        public Nullable<System.Guid> IdProfesorComputacion { get; set; }
        public Nullable<System.Guid> IdProfesor { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public Nullable<System.Guid> IdDirector { get; set; }
        public Nullable<System.DateTime> FechaRespuesta { get; set; }
        public Nullable<bool> IsAprovado { get; set; }
    
        public virtual Director Director { get; set; }
        public virtual Group Group { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual TeacherComputacion TeacherComputacion { get; set; }
    }
}
