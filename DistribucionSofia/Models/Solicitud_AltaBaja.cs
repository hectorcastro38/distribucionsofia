//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Solicitud_AltaBaja
    {
        public System.Guid Id { get; set; }
        public System.Guid Id_Escuela { get; set; }
        public System.Guid Id_Grupo_Origen { get; set; }
        public Nullable<System.Guid> Id_Grupo_Destino { get; set; }
        public System.Guid Id_Estudiante { get; set; }
        public Nullable<System.Guid> Id_Usuario_Aprobo { get; set; }
        public int Tipo { get; set; }
        public bool Estatus { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public Nullable<System.DateTime> FechaCambio { get; set; }
    
        public virtual Group Group { get; set; }
        public virtual Group Group1 { get; set; }
        public virtual School School { get; set; }
        public virtual Secretario Secretario { get; set; }
        public virtual Student Student { get; set; }
    }
}
