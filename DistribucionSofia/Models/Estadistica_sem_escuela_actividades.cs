//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Estadistica_sem_escuela_actividades
    {
        public System.Guid Id { get; set; }
        public System.DateTime Semana { get; set; }
        public int TareasAsignadas { get; set; }
        public int Profesores { get; set; }
        public int ProfesoresActivos { get; set; }
        public int Grupos { get; set; }
        public int GruposActivos { get; set; }
        public double PorcentajeProfesoresActivos { get; set; }
        public double PorcentajeGruposActivos { get; set; }
        public double PromedioTareasProfesor { get; set; }
        public string NombreEstatus { get; set; }
        public int Estatus { get; set; }
        public Nullable<bool> IsActiva { get; set; }
        public string NombreEscuela { get; set; }
        public string NombreDependencia { get; set; }
        public string NombreEstado { get; set; }
        public Nullable<int> NombreZona { get; set; }
        public string ClaveOficial { get; set; }
        public Nullable<int> Dependencia { get; set; }
        public int EstadoCode { get; set; }
        public System.Guid IdEscuela { get; set; }
        public Nullable<System.Guid> IdZona { get; set; }
        public Nullable<int> EstudiantesActivos { get; set; }
        public int TotalEjercicios { get; set; }
        public System.DateTime FechaRegistro { get; set; }
    
        public virtual School School { get; set; }
        public virtual Zona Zona { get; set; }
    }
}
