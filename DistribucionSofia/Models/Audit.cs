//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Audit
    {
        public System.Guid Id { get; set; }
        public string Username { get; set; }
        public Nullable<System.Guid> IdUser { get; set; }
        public string URL { get; set; }
        public System.DateTime Fecha { get; set; }
        public string IPAddress { get; set; }
        public string Controller { get; set; }
        public string Area { get; set; }
        public string Action { get; set; }
        public string Params { get; set; }
        public string Method { get; set; }
        public string Host { get; set; }
        public string ClientComputerName { get; set; }
    
        public virtual aspnet_Membership aspnet_Membership { get; set; }
    }
}
