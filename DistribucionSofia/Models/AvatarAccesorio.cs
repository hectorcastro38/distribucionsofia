//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AvatarAccesorio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AvatarAccesorio()
        {
            this.AvatarComprasEstudiantes = new HashSet<AvatarComprasEstudiante>();
            this.AvatarUsuarioAccesorios = new HashSet<AvatarUsuarioAccesorio>();
            this.Avatars = new HashSet<Avatar>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid IdAvatar { get; set; }
        public System.Guid IdImagen { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public System.Guid IdCategoriaAvatar { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public Nullable<System.DateTime> FechaAbierto { get; set; }
        public Nullable<System.DateTime> FechaCerrado { get; set; }
        public Nullable<System.Guid> IdNivelPunjate { get; set; }
        public int Creditos { get; set; }
        public bool IsExclusivoMaraton { get; set; }
    
        public virtual Avatar Avatar { get; set; }
        public virtual NivelPuntaje NivelPuntaje { get; set; }
        public virtual AvatarCategoria AvatarCategoria { get; set; }
        public virtual Imagene Imagene { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AvatarComprasEstudiante> AvatarComprasEstudiantes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AvatarUsuarioAccesorio> AvatarUsuarioAccesorios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Avatar> Avatars { get; set; }
    }
}
