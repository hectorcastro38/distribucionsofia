//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Recurso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Recurso()
        {
            this.Comentarios = new HashSet<Comentario>();
            this.RecursoVotoes = new HashSet<RecursoVoto>();
            this.Temas = new HashSet<Tema>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public string Url { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string MIMEtype { get; set; }
        public string NombreOriginal { get; set; }
        public int PuntosTotales { get; set; }
        public int VotosTotales { get; set; }
        public double Calificacion { get; set; }
        public bool IsActivo { get; set; }
        public int NVersion { get; set; }
        public int Tipo { get; set; }
        public string Descripcion { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comentario> Comentarios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecursoVoto> RecursoVotoes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tema> Temas { get; set; }
    }
}
