//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Evaluacione
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Evaluacione()
        {
            this.Assigments = new HashSet<Assigment>();
            this.EvaluacionSubs = new HashSet<EvaluacionSub>();
            this.GroupMateriaParcialesEvaluacions = new HashSet<GroupMateriaParcialesEvaluacion>();
            this.Frases = new HashSet<Frase>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public bool Manual { get; set; }
        public Nullable<bool> hasSubEvaluaciones { get; set; }
        public Nullable<System.Guid> IdTeacher { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Assigment> Assigments { get; set; }
        public virtual Teacher Teacher { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EvaluacionSub> EvaluacionSubs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupMateriaParcialesEvaluacion> GroupMateriaParcialesEvaluacions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Frase> Frases { get; set; }
    }
}
