//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TasksRetroUser
    {
        public System.Guid Id { get; set; }
        public int IdTask { get; set; }
        public int IdTask_Complete { get; set; }
        public System.Guid IdUser { get; set; }
        public System.Guid IdOptionRetro { get; set; }
        public System.DateTime Fecha { get; set; }
    
        public virtual aspnet_Membership aspnet_Membership { get; set; }
        public virtual ExerciseRetroOption ExerciseRetroOption { get; set; }
        public virtual Task Task { get; set; }
        public virtual Tasks_Complete Tasks_Complete { get; set; }
    }
}
