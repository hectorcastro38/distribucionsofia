//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityRating
    {
        public System.Guid Id { get; set; }
        public Nullable<int> IdTask { get; set; }
        public Nullable<System.Guid> IdAssigment { get; set; }
        public int Rating { get; set; }
        public string Comentario { get; set; }
        public System.Guid EstudianteOrigen { get; set; }
    
        public virtual Assigment Assigment { get; set; }
        public virtual Student Student { get; set; }
        public virtual Task Task { get; set; }
    }
}
