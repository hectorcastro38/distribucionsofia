//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cupon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cupon()
        {
            this.Licencias = new HashSet<Licencia>();
        }
    
        public System.Guid Id { get; set; }
        public int TipoCupon { get; set; }
        public int TipoValidacionCupon { get; set; }
        public System.DateTime FechaInicioInscripcion { get; set; }
        public System.DateTime FechaFinalIncripcion { get; set; }
        public int LimiteUso { get; set; }
        public int NumeroLicencias { get; set; }
        public Nullable<int> Validacion_Dias { get; set; }
        public Nullable<System.DateTime> Validacion_FechaInicio { get; set; }
        public Nullable<System.DateTime> Validacion_FechaFinal { get; set; }
        public string Nip { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Licencia> Licencias { get; set; }
    }
}
