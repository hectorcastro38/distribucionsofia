//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Logro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User_Logro()
        {
            this.Feeds = new HashSet<Feed>();
            this.UsuarioDiagnosticoConceptoes = new HashSet<UsuarioDiagnosticoConcepto>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid IdUser { get; set; }
        public System.Guid IdLogro { get; set; }
        public System.DateTime Fecha { get; set; }
        public Nullable<System.Guid> IdAssigmentComplete { get; set; }
        public Nullable<System.Guid> IdTempActivity { get; set; }
        public Nullable<int> IdTaskComplete { get; set; }
        public Nullable<int> IdQuickTestComplete { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual Assigment_Complete Assigment_Complete { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feed> Feeds { get; set; }
        public virtual Logro Logro { get; set; }
        public virtual QuickTest_Complete QuickTest_Complete { get; set; }
        public virtual Tasks_Complete Tasks_Complete { get; set; }
        public virtual Temp_Activity Temp_Activity { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsuarioDiagnosticoConcepto> UsuarioDiagnosticoConceptoes { get; set; }
    }
}
