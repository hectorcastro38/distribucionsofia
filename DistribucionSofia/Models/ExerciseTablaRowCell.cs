//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExerciseTablaRowCell
    {
        public System.Guid Id { get; set; }
        public System.Guid IdColumn { get; set; }
        public System.Guid IdRow { get; set; }
        public int CellTypeCode { get; set; }
        public string Value { get; set; }
        public Nullable<bool> IsCorrect { get; set; }
        public string Options { get; set; }
        public System.Guid IdTabla { get; set; }
    
        public virtual ExerciseTabla ExerciseTabla { get; set; }
        public virtual ExerciseTablaColumn ExerciseTablaColumn { get; set; }
        public virtual ExerciseTablaRow ExerciseTablaRow { get; set; }
    }
}
