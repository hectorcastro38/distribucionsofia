//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inbox
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inbox()
        {
            this.InboxReportes = new HashSet<InboxReporte>();
            this.InboxUserReads = new HashSet<InboxUserRead>();
        }
    
        public System.Guid Id { get; set; }
        public System.DateTime Fecha { get; set; }
        public string TextoMensaje { get; set; }
        public System.Guid IdUserSend { get; set; }
        public System.Guid IdConversacion { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual InboxConversation InboxConversation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InboxReporte> InboxReportes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InboxUserRead> InboxUserReads { get; set; }
    }
}
