//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Traduccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Traduccion()
        {
            this.TraduccionPropuestas = new HashSet<TraduccionPropuesta>();
        }
    
        public System.Guid Id { get; set; }
        public string Tabla { get; set; }
        public string IdTabla { get; set; }
        public System.Guid IdLenguaje { get; set; }
        public string Campo { get; set; }
        public string Traduccion1 { get; set; }
        public System.Guid IdUserCreado { get; set; }
        public System.DateTime FechaRegistrado { get; set; }
        public Nullable<System.Guid> IdUserModificado { get; set; }
        public Nullable<System.DateTime> FechaModificado { get; set; }
        public string Autor { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> FechaEliminado { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual aspnet_Users aspnet_Users1 { get; set; }
        public virtual LenguajeCodigo LenguajeCodigo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TraduccionPropuesta> TraduccionPropuestas { get; set; }
    }
}
