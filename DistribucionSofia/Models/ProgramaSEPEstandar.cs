//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProgramaSEPEstandar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProgramaSEPEstandar()
        {
            this.Planeacions = new HashSet<Planeacion>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public System.Guid IdProgramaSEP_C_Eje { get; set; }
        public System.Guid IdProgramaSEP_C_Tema { get; set; }
        public int IdNivelEscolar { get; set; }
        public int Orden { get; set; }
    
        public virtual NivelEscolar NivelEscolar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Planeacion> Planeacions { get; set; }
        public virtual ProgramaSEP_C_Eje ProgramaSEP_C_Eje { get; set; }
        public virtual ProgramaSEP_C_Tema ProgramaSEP_C_Tema { get; set; }
    }
}
