﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Threading;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using EncryptStringSample;
using System.Text;
using Mvc.Mailer;
using DistribucionSofia.Mailers;

namespace DistribucionSofia.Models
{
    #region Models

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Contraseña actual requerida")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña actual")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Nueva contraseña requerida")]
        [ValidatePasswordLength]
        [System.Web.Mvc.Remote("_IsUserPasswordSafe", "Account")]
        [RegularExpression("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$", ErrorMessage = "La contraseña debe tener entre 6 a 20 caracteres, puedes usar letras y números.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña")]
        public string NewPassword { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirma nueva contraseña")]
        public string ConfirmPassword { get; set; }
    }


    public class ChangeQuestionAnswerModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña actual")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Pregunta")]
        public string Pregunta { get; set; }

        [Required]
        [Display(Name = "Respuesta")]
        public string Respuesta { get; set; }
    }


    public class UpdateAccountInfoModel
    {
        [Required(ErrorMessage = "El nuevo nombre de usuario es requerido")]
        [Display(Name = "Usuario")]
        //[System.Web.Mvc.Remote("_IsUserDisponibleR", "Account")]
        [System.Web.Mvc.Remote("_IsUsernameSafe", "Account")]
        [RegularExpression("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$", ErrorMessage = "El nombre de usuario debe tener entre 6 a 20 caracteres, puedes usar letras, números y el símbolo '_' (guión bajo).")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "La nueva contraseña es requerida")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        [System.Web.Mvc.Remote("_IsUserPasswordSafe", "Account")]
        [RegularExpression("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$", ErrorMessage = "La contraseña debe tener entre 6 a 20 caracteres, puedes usar letras y números.")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirma nueva contraseña")]
        public string ConfirmPassword { get; set; }

        [System.Web.Mvc.Remote("_IsValidEmailPapa", "Account")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Dirección de correo de tu papá")]
        public string EmailPapa { get; set; }

        [System.Web.Mvc.Remote("_IsValidEmailMama", "Account")]
        [Display(Name = "Dirección de correo de tu mamá")]
        [DataType(DataType.EmailAddress)]
        public string EmailMama { get; set; }

    }

    public class LogOnModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Recordarme?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        Random rand = new Random(200);

        int random = new Random(150).Next(0, 9);

        int random1 = new Random(200).Next(0, 9);
        int random2 = new Random(5435).Next(0, 9);
        int random3 = new Random(4354).Next(0, 9);
        int random4 = new Random(54645).Next(0, 9);

        [Required(ErrorMessage = "*")]
        [Display(Name = "Usuario")]
        [System.Web.Mvc.Remote("_IsUserDisponibleR", "Account")]
        public string UserName { get; set; }


        [Required(ErrorMessage = "*")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Dirección de correo")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme contraseña")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Nombres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Apellido Paterno")]
        public string LastNameP { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Apellido Materno")]
        public string LastNameM { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Fecha de Nacimiento")]
        public System.DateTime Birthday { get; set; }

        [Required(ErrorMessage = "*")]
        public int Sexo { get; set; }
    }

    public class RegisterModelDistribuidor : RegisterModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Estado")]
        public int Estado { get; set; }

        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Calle")]
        public string Calle { get; set; }

        [Display(Name = "Número Interior")]
        public string NumInt { get; set; }

        [Display(Name = "Número Exterior")]
        public string NumExt { get; set; }

        [Display(Name = "Colonia")]
        public string Colonia { get; set; }

        [Display(Name = "País")]
        public string Pais { get; set; }

        [Display(Name = "Código Postal")]
        public string CP { get; set; }


        [Required(ErrorMessage = "*")]
        [Display(Name = "Día")]
        public int Dia { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Mes")]
        public int Mes { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Año")]
        public int Ano { get; set; }
    }

    public class RegisterModelDirector : RegisterModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Escuela")]
        public Guid SchoolID { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Día")]
        public int Dia { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Mes")]
        public int Mes { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Año")]
        public int Ano { get; set; }
    }

    public class RegisterModelCoordinador : RegisterModel
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Estado")]
        public int Estado { get; set; }
        public Guid IdDistribuidor { get; set; }
        public Guid SchoolID { get; set;}
        [Required(ErrorMessage = "*")]
        [Display(Name = "Día")]
        public int Dia { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Mes")]
        public int Mes { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Año")]
        public int Ano { get; set; }

    }

    #endregion

    #region Services
    // The FormsAuthentication type is sealed and contains static members, so it is difficult to
    // unit test code that calls its members. The interface and helper class below demonstrate
    // how to create an abstract wrapper around such a type in order to make the AccountController
    // code unit testable.

    public interface IMembershipService
    {
        int MinPasswordLength { get; }

        bool ValidateUser(string userName, string password);
        MembershipCreateStatus CreateUser(string userName, string password, string email);
        MembershipCreateStatus CreateUser(string userName, string password, string email, bool aprobado);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
    }

    public class AccountMembershipService : IMembershipService
    {
        private readonly MembershipProvider _provider;

        public AccountMembershipService()
            : this(null)
        {
        }

        public AccountMembershipService(MembershipProvider provider)
        {
            _provider = provider ?? Membership.Provider;
        }

        public int MinPasswordLength
        {
            get
            {
                return _provider.MinRequiredPasswordLength;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

            return _provider.ValidateUser(userName, password);
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

            MembershipCreateStatus status;
            _provider.CreateUser(userName, password, email, "passwordQuestion", "passwordAnswer", true, null, out status);
            return status;
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email, bool aprobado)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

            MembershipCreateStatus status;
            _provider.CreateUser(userName, password, email, "passwordQuestion", "passwordAnswer", aprobado, null, out status);
            return status;
        }

        //public MembershipCreateStatus CreateUserRetrieval(string userName, string password, string email)
        //{
        //    if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
        //    if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
        //    if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

        //    MembershipCreateStatus status;
        //    _provider.CreateUser(userName, password, email, String.Empty, String.Empty, true, null, out status);
        //    return status;
        //}

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
                return currentUser.ChangePasswordQuestionAndAnswer(oldPassword, newPassword, "passwordAnswer");
                //return currentUser.ChangePassword(oldPassword, newPassword);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

    }

    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(string userName, bool createPersistentCookie)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");

            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
    #endregion

    #region Validation
    public static class AccountValidation
    {
        public static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Usuario existente. Intente con un usuario diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Algun usuario para ese correo ya existe. Ingrese un correo diferente";
                //return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Contraseña inválida";
                //return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Correo inválido";
                //return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    //return "The user name provided is invalid. Please check the value and try again.";
                    return "Usuario inválido";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "Ha ocurrido un error interno, verifique e intente de nuevo. Si el problema persiste, contacte al administrado del sistema.";
                //return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class ValidatePasswordLengthAttribute : ValidationAttribute, IClientValidatable
    {
        private const string _defaultErrorMessage = "'{0}' deben ser al menos {1} caracteres.";
        private readonly int _minCharacters = Membership.Provider.MinRequiredPasswordLength;

        public ValidatePasswordLengthAttribute()
            : base(_defaultErrorMessage)
        {
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString,
                name, _minCharacters);
        }

        public override bool IsValid(object value)
        {
            string valueAsString = value as string;
            return (valueAsString != null && valueAsString.Length >= _minCharacters);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[]{
                new ModelClientValidationStringLengthRule(FormatErrorMessage(metadata.GetDisplayName()), _minCharacters, int.MaxValue)
            };
        }
    }
    #endregion
}
