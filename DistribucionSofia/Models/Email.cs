﻿using EncryptStringSample;
using DistribucionSofia.Mailers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Security;

namespace DistribucionSofia.Models
{
    public static class Email
    {
        public static void EnviarNotificacionUsuario(Guid iddistribuidor, Guid escuela)
        {
            Entities db = new Entities();
            var con = db.Configuracions.First();
            var school = db.Schools.Find(escuela);
            var distribuidor = db.Distribuidors.Find(iddistribuidor);

            var shost = con.Smtp_host;
            var sfrom = con.Smtp_email_from;
            var spass = con.Smtp_pass;
            var sto = distribuidor.aspnet_Users.UserExtra.Correo;
            var sport = con.Smtp_port;
            var ssl = con.Smtp_enableSsl;
            
            using (var cSmtp = new SmtpClient(shost, sport))
            {
                cSmtp.EnableSsl = ssl;
                cSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                cSmtp.Credentials = new NetworkCredential(sfrom, spass);
                var ms = new UserMailer().NotificacionUsuario("Se ha creado la orden la escuela: " + school.Name + ". Por el usuario " + distribuidor.aspnet_Users.UserName + ", el día " + DateTime.Now.ToShortDateString() +
                ", si la orden de compra es en modo prueba, se habilitara en menos de 24 horas, de lo contrario se activara, hasta que se refleje el pago. ", sto, distribuidor.aspnet_Users.UserName);
                ms.From = new MailAddress(sfrom, "SofiaXT");
                cSmtp.Send(ms);
            }
        }

        public static void EnviarNotificacionUsuarioAprobada(Guid idDistribuidor, Guid idEscuela)
        {
            Entities db = new Entities();
            var con = db.Configuracions.First();
            var school = db.Schools.Find(idEscuela);
            var distribuidor = db.Distribuidors.Find(idDistribuidor);

            var shost = con.Smtp_host;
            var sfrom = con.Smtp_email_from;
            var spass = con.Smtp_pass;
            var sto = distribuidor.aspnet_Users.UserExtra.Correo;
            var sport = con.Smtp_port;
            var ssl = con.Smtp_enableSsl;

            using (var cSmtp = new SmtpClient(shost, sport))
            {
                cSmtp.EnableSsl = ssl;
                cSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                cSmtp.Credentials = new NetworkCredential(sfrom, spass);
                var ms = new UserMailer().NotificacionUsuario("Se ha aprobado la orden de la escuela: " + school.Name + ", el día " + DateTime.Now.ToShortDateString(), sto, distribuidor.aspnet_Users.UserName);
                ms.From = new MailAddress(sfrom, "SofiaXT");
                cSmtp.Send(ms);
            }
        }

        public static void EnviarNotificacionNuevaPrueba(Guid idEscuela)
        {
            Entities db = new Entities();
            var con = db.Configuracions.First();
            var school = db.Schools.Find(idEscuela);

            var shost = con.Smtp_host;
            var sfrom = con.Smtp_email_from;
            var spass = con.Smtp_pass;
            var sto = "daniel.valenzuela@sofiaxt.com";
            var sport = con.Smtp_port;
            var ssl = con.Smtp_enableSsl;

            using (var cSmtp = new SmtpClient(shost, sport))
            {
                cSmtp.EnableSsl = ssl;
                cSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                cSmtp.Credentials = new NetworkCredential(sfrom, spass);
                var ms = new UserMailer().NotificacionManager("Se ha crado la orden de prueba: " + school.Name + ", el día " + DateTime.Now.ToShortDateString(), sto);
                ms.From = new MailAddress(sfrom, "SofiaXT");
                cSmtp.Send(ms);
            }
        }

        public static void EnviarNotificacionNueva(Guid idEscuela)
        {
            Entities db = new Entities();
            var con = db.Configuracions.First();
            var school = db.Schools.Find(idEscuela);

            var shost = con.Smtp_host;
            var sfrom = con.Smtp_email_from;
            var spass = con.Smtp_pass;
            var sto = "daniel.valenzuela@sofiaxt.com";
            var sport = con.Smtp_port;
            var ssl = con.Smtp_enableSsl;

            using (var cSmtp = new SmtpClient(shost, sport))
            {
                cSmtp.EnableSsl = ssl;
                cSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                cSmtp.Credentials = new NetworkCredential(sfrom, spass);
                var ms = new UserMailer().NotificacionManager("Se ha orden la escuela: " + school.Name + ", el día " + DateTime.Now.ToShortDateString(), sto);
                ms.From = new MailAddress(sfrom, "SofiaXT");
                cSmtp.Send(ms);
            }
        }

        private static string GetPublicUrl()
        {
            var request = HttpContext.Current.Request;

            var uriBuilder = new UriBuilder
            {
                Host = request.Url.Host,
                Path = "/",
                Port = 80,
                Scheme = "http",
            };

            if (request.IsLocal)
            {
                uriBuilder.Port = request.Url.Port;
            }

            return new Uri(uriBuilder.Uri.ToString()).AbsoluteUri;
        }

        public static void EnviarEmailPrueba()
        {
            Entities db = new Entities();
            var con = db.Configuracions.First();

            var shost = con.Smtp_host;
            var sfrom = con.Smtp_email_from;
            var spass = con.Smtp_pass;
            var sto = "julio.ibarrae@gmail.com";
            var sport = con.Smtp_port;
            var ssl = con.Smtp_enableSsl;

            using (var cSmtp = new SmtpClient(shost, sport))
            {
                cSmtp.EnableSsl = ssl;
                cSmtp.Credentials = new NetworkCredential(sfrom, spass);
                var ms = new UserMailer().Prueba();
                ms.From = new MailAddress(sfrom, "SofiaXT - Usuarios");
                ms.Bcc.Add("lugolmara@sofiaxt.mx");
                ms.To.Add(sto);
                ms.To.Add("jcesar_ibarra@hotmail.com");
                //ms.To.Add("general@sofiaxt.com");
                cSmtp.Send(ms);
            }
        }
    }
}