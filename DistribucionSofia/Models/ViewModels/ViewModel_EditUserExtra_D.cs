﻿using DistribucionSofia.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DistribucionSofia.Models
{
    public class ViewModel_EditUserExtra_D
    {
        [Required(ErrorMessage = "*")]
        [Display(Name = "Usuario")]
        [RemoteArea("isUsernameValid", "Validation", "")]
        [RegularExpression("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$", ErrorMessage = "El nombre de usuario debe tener entre 6 a 20 caracteres, puedes usar letras, números y el símbolo '_' (guión bajo).")]
        public string Username { get; set; }

        [Required(ErrorMessage="Requerido")]
        public string Name { get; set; }
        public string LastNameP { get; set; }
        public string LastNameM { get; set; }

        public bool IsFacebook { get; set; }

        public Guid Id { get; set; }

        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Dirección inválida")]
        [StringLength(100, ErrorMessage = "La {0} debe ser de al menos {2} caracteres.", MinimumLength = 2)]
        [RemoteArea("_IsEmailDisponible", "Account", "")]
        public string Email { get; set; }

        //[Required(ErrorMessage="Celular obligatorio para recuperación de contraseña")]
        [StringLength(10, ErrorMessage = "El teléfono debe tener 10 caracteres", MinimumLength = 0)]
        public string Telefono { get; set; }

        public string RazonSocial { get; set; }
        public string RFC { get; set; }
        public string Calle { get; set; }
        public string NumInt { get; set; }
        public string NumExt { get; set; }
        public string Colonia { get; set; }
        public string Localidad { get; set; }
        public string Referencia { get; set; }
        public string DELEGACION { get; set; }
        public string Pais { get; set; }
        public string CP { get; set; }
        public Nullable<int> Estado { get; set; }
    }
}