﻿using System;

namespace DistribucionSofia.Models.ViewModels
{
    public class ViewModel_d_ReporteEstadistico_Escuela
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public string Ciudad { get; set; }
        public string NombreEscuela { get; set; }
        public string CCT { get; set; }
        public string Escolaridad { get; set; }
        public string Turno { get; set; }
        public int Actividades_Asignadas { get; set; }
        public int TotalEjercicios { get; set; }
        public int EjerciciosActividades { get; set; }
        public int EjerciciosLibres { get; set; }
        public int EjerciciosOtros { get; set; }
        public int Profesores_con_Grupo { get; set; }
        public int Estudiantes_Grupo { get; set; }
        public int LicenciaAlumno { get; set; }
    }

    public class ViewModel_d_ReporteEstadistico_Individual
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public string Ciudad { get; set; }
        public string NombreEscuela { get; set; }
        public string CCT { get; set; }
        public string Escolaridad { get; set; }
        public string Turno { get; set; }
        public string Grupo { get; set; }
        public string Profesor { get; set; }
        public int Activo { get; set; }
        public int ActividadesAsignadas { get; set; }
        public int EstudiantesActivos { get; set; }
        public int EstudiantesGrupo { get; set; }
        public int LicenciasAlumno { get; set; }
        public int TotalEjercicios { get; set; }
        public int EjerciciosActividades { get; set; }
        public int EjerciciosLibres { get; set; }
        public int EjerciciosOtros { get; set; }
        public double TotalCalificacion { get; set; }
    }

    public class ViewModel_d_ReporteSemaforo
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public string NombreEscuela { get; set; }
        public string CCT { get; set; }
        public string Escolaridad { get; set; }
        public string Turno { get; set; }
        public string Grupo { get; set; }
        public string Profesor { get; set; }
        public int Actividades_Asignadas { get; set; }
        public int LicenciaAlumno { get; set; }
        public int EjerciciosActividades { get; set; }
    }
}