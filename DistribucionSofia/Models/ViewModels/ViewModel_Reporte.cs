﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.ViewModels
{
    public class ViewModel_Reporte
    {
        public Guid? IdEstudiante { get; set; }
        public string NombreAlumno { get; set; }
        public string Grupo { get; set; }
        public string NombreEscuela { get; set; }
        public Guid? IdSchool { get; set; }
        public Guid? IdAssigment { get; set; }
        public double Diagnostico { get; set; }
        public string Paquete1 { get; set; }
        public string Paquete2 { get; set; }
        public string Paquete3 { get; set; }
        public string Paquete4 { get; set; }
        public string Paquete5 { get; set; }
        public string Paquete6 { get; set; }
        public string Paquete7 { get; set; }
        public  string UltimoPaqueteFecha { get; set; }
        public int UltimoPaquete { get; set; }
    }


}