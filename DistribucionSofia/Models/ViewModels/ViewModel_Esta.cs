﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.ViewModels
{
    public class retorno
    {
        public string NombreProfesor { get; set; }
        public string NombreEscuela { get; set; }
        public int ActividadesAsignadas { get; set; }
        public string NombreGrupo { get; set; }
    }

    public class RetornoActividad1x1
    {
        public string NombreEscuela { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public string NombreActividad { get; set; }
        public int NumeroEjercicios { get; set; }
        public string Fecha { get; set; }
    }

    public class RetornoAlumnos
    {
        public string NombreEscuela { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public string NombreAlumno { get; set; }
        public int Puntos { get; set; }
        public double Promedio { get; set; }
    }

    public class RetornoNumeroAlumnos
    {
        public string NombreEscuela { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public int NumeroAlumnos { get; set; }
    }

    public class RetornoAlumnos2
    {
        public string NombreEscuela { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public string NombreAlumno { get; set; }
        public int EjerciciosTotales { get; set; }
        public int EjerciciosLibres { get; set; }
        public int EjerciciosAsignados { get; set; }
        public int Puntos { get; set; }

    }

    public class RetornoGrupo
    {
        public string NombreEscuela { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public int EjerciciosTotales { get; set; }
        public int EjerciciosLibres { get; set; }
        public int EjerciciosAsignados { get; set; }
        public int EjerciciosTask { get; set; }
        public int Puntos { get; set; }

    }

    public class Licenias
    {
        public string Escuela { get; set; }
        public string Nivel { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int NoLicenciasAlumno { get; set; }
        public int NoLicenciaProfesor { get; set; }
    }
}