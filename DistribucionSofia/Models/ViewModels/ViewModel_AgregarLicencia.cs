﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DistribucionSofia.Models
{
    public class ViewModel_LicenciaTamaulipas
    {
        
        //Escuela
        public string Ciudad { get; set; }
        public string NombreEscuela { get; set; }
        public string CCT { get; set; }
        public string Escolaridad { get; set; }
        public int LicAlumnos { get; set; }
        public int LicProGrupoActivo { get; set; }
        public int LicProfEscuela { get; set; }
        public int LicDirector { get; set; }
        public DateTime FechaFinal { get; set; }
        public int TotalLic { get; set; }
        public int Agregados { get; set; }
    }
}