﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DistribucionSofia.Models
{
    public class ViewModel_excel_Maestros : School
    {
        
        //Maestr
        public string Nombre { get; set; }
        public string ApellidoM { get; set; }
        public string ApellidoP { get; set; }
        public string Grado { get; set; }
        public string Grupo { get; set; }
        public string NombreEscuela { get; set; }
        public string ClaveOficial { get; set; }
        public string Usuario { get; set; }
        public string Contraseña { get; set; }

    }
}