﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DistribucionSofia.Models
{
    public class ViewModel_excel_EscuelaNueva : School
    {
        
        //Director
        public string NombreDir { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string CorreoDir { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int Sexo { get; set; }
        public string UsuarioDirector { get; set; }
        public string ContraseñaDirector { get; set; }

        public int Cantidad { get; set; }
        public Guid IdPlan { get; set; }
        public int Periodo { get; set; }
        public int Conteo { get; set; }
    }
}