﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DistribucionSofia.Models.Enum;

namespace DistribucionSofia.Models
{
    public class ViewModel_d_RegistroDirector : School
    {
        [Required(ErrorMessage = "{0} requerido")]
        [Display(Name = "UserName")]
        //[StringLength(100, ErrorMessage = "El  {0} debe ser de al menos {2} caracteres.", MinimumLength = 4)]
        [RegularExpression("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$", ErrorMessage = "El nombre de usuario debe tener entre 6 a 20 caracteres, puedes usar letras, números y el símbolo '_' (guión bajo).")]
        [System.Web.Mvc.Remote("_IsUserDisponibleR", "Account")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "{0} requerido")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Dirección inválida")]
        [StringLength(100, ErrorMessage = "El campo {0} debe ser de al menos {2} caracteres.", MinimumLength = 2)]
        [System.Web.Mvc.Remote("_IsEmailDisponible", "Account")]
        [Display(Name = "Dirección de correo")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*")]
        public string NameDir { get; set; }
        [Required(ErrorMessage = "*")]
        public string LastNameP { get; set; }
        public string LastNameM { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Día")]
        public int Dia { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Mes")]
        public int Mes { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Año")]
        public int Ano { get; set; }
        [Required(ErrorMessage = "*")]
        public int Sexo { get; set; }
        public string Telefono { get; set;}
    }
}