﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.ViewModels
{
    public class ViewModel_VinculacionStudent
    {
        public Guid GrupoViejoId { get; set; }
        public Guid GrupoNuevoId { get; set; }
        public Guid EstudianteId { get; set; }
        public bool Procesado { get; set; }

    }
}