﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DistribucionSofia.Models
{
    public class ViewModel_AlumnosDiagnostico : School
    {

        public string IdEscuela { get; set; }
        public string IdEstudiante { get; set; }
        public string IdGrupo { get; set; }
        public string ciudad { get; set; }
        public int TaskID { get; set; }
        public int IdNivelEscolar { get; set; }
        public int Grado { get; set; }
        public string idEstado { get; set; }
        public string idPais { get; set; }
        public bool IsProcesado { get; set; }


     
    }
}