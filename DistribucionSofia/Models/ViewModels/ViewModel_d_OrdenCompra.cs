﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DistribucionSofia.Models
{
    public class ViewModel_d_OrdenCompra : School
    {
        //Director
        public string nombreDir { get; set; }
        public string apellidoP { get; set; }
        public string apellidoM { get; set; }
        public string correoDir { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int sexo { get; set; }

        public int Cantidad { get; set; }
        public int Cantidad2 { get; set; }
        public Guid IdPlan { get; set; }
        public string nombrePlan { get; set; }
        public bool PeriodoPrueba { get; set;}

        public bool aplicaFactura { get; set;}
        public bool omiteContacto { get; set;}
        public int periodo {get; set;}
        public DateTime FechaInicio { get; set;}
    }
}