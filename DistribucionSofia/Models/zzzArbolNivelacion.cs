//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class zzzArbolNivelacion
    {
        public double Id { get; set; }
        public Nullable<double> GRADO { get; set; }
        public Nullable<double> EJE { get; set; }
        public Nullable<double> OTRO { get; set; }
        public string NOMENCLATURA { get; set; }
        public string CONCEPTO { get; set; }
        public string HIJOS { get; set; }
        public int IsProcesada { get; set; }
    }
}
