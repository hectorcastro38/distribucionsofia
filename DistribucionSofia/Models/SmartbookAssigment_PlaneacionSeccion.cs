//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SmartbookAssigment_PlaneacionSeccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SmartbookAssigment_PlaneacionSeccion()
        {
            this.Assigments = new HashSet<Assigment>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid IdSmartbookAssigment { get; set; }
        public int IdPlaneacion { get; set; }
        public int IdSeccion { get; set; }
        public int Tipo { get; set; }
        public bool IsActivo { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Assigment> Assigments { get; set; }
        public virtual SmartbookAssigment SmartbookAssigment { get; set; }
    }
}
