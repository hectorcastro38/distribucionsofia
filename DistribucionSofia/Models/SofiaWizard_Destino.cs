//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SofiaWizard_Destino
    {
        public System.Guid Id { get; set; }
        public System.Guid IdSofiaWizard { get; set; }
        public int TipoDesitno { get; set; }
        public Nullable<System.Guid> IdEstudiante { get; set; }
        public Nullable<System.Guid> IdUsuario { get; set; }
        public Nullable<System.Guid> IdDirector { get; set; }
        public Nullable<System.Guid> IdEscuela { get; set; }
        public Nullable<System.Guid> IdProfesor { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual Director Director { get; set; }
        public virtual School School { get; set; }
        public virtual SofiaWizard SofiaWizard { get; set; }
        public virtual Student Student { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}
