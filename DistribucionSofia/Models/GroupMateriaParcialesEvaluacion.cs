//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GroupMateriaParcialesEvaluacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GroupMateriaParcialesEvaluacion()
        {
            this.GroupMateriaParcialesCalificaciones = new HashSet<GroupMateriaParcialesCalificacione>();
            this.GroupMateriaParcialesEvaluacionSubs = new HashSet<GroupMateriaParcialesEvaluacionSub>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid IdGroupMateriaParcial { get; set; }
        public System.Guid IdEvaluacion { get; set; }
        public int Porcentaje { get; set; }
        public bool hasSubevaluaciones { get; set; }
        public Nullable<int> CountSubevaluaciones { get; set; }
    
        public virtual Evaluacione Evaluacione { get; set; }
        public virtual GroupMateriaParciale GroupMateriaParciale { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupMateriaParcialesCalificacione> GroupMateriaParcialesCalificaciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupMateriaParcialesEvaluacionSub> GroupMateriaParcialesEvaluacionSubs { get; set; }
    }
}
