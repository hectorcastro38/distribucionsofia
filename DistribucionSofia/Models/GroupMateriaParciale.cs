//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GroupMateriaParciale
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GroupMateriaParciale()
        {
            this.Assigments = new HashSet<Assigment>();
            this.GroupMateriaParcialesAlumnos = new HashSet<GroupMateriaParcialesAlumno>();
            this.GroupMateriaParcialesEvaluacions = new HashSet<GroupMateriaParcialesEvaluacion>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid IdGrupoMateria { get; set; }
        public bool IsCerrado { get; set; }
        public Nullable<System.DateTime> FechaCerrado { get; set; }
        public int Numero { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Assigment> Assigments { get; set; }
        public virtual GroupMateria GroupMateria { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupMateriaParcialesAlumno> GroupMateriaParcialesAlumnos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupMateriaParcialesEvaluacion> GroupMateriaParcialesEvaluacions { get; set; }
    }
}
