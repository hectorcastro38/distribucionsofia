//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Feed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Feed()
        {
            this.FeedComentarios = new HashSet<FeedComentario>();
            this.FeedStudents = new HashSet<FeedStudent>();
            this.FeedUsers = new HashSet<FeedUser>();
        }
    
        public System.Guid Id { get; set; }
        public System.DateTime Fecha { get; set; }
        public int Likes { get; set; }
        public string TextoFeed { get; set; }
        public Nullable<System.Guid> IdStudent { get; set; }
        public bool IsPublic { get; set; }
        public int TipoType { get; set; }
        public Nullable<System.Guid> IdAvatarUsuario { get; set; }
        public Nullable<int> IdQTCompleto { get; set; }
        public Nullable<int> IdTaskComplete { get; set; }
        public Nullable<System.Guid> IdAssigComplete { get; set; }
        public Nullable<System.Guid> IdTIComplete { get; set; }
        public Nullable<System.Guid> IdLogroUsuario { get; set; }
        public Nullable<System.Guid> IdJuego { get; set; }
        public Nullable<System.Guid> IdImagen { get; set; }
        public Nullable<System.Guid> IdProductoCompra { get; set; }
        public Nullable<System.Guid> IdTeacher { get; set; }
        public Nullable<System.Guid> IdTutor { get; set; }
        public Nullable<System.Guid> IdGroup { get; set; }
        public Nullable<System.Guid> IdSchool { get; set; }
        public Nullable<System.Guid> IdDistribuidor { get; set; }
        public Nullable<System.Guid> IdDirector { get; set; }
        public Nullable<System.Guid> IdTeacherComputacion { get; set; }
        public Nullable<bool> IsComentable { get; set; }
        public Nullable<System.Guid> IdAAUser { get; set; }
        public string HTML { get; set; }
        public Nullable<System.Guid> IdRecursoXT { get; set; }
    
        public virtual AAEstudiante AAEstudiante { get; set; }
        public virtual Assigment_Complete Assigment_Complete { get; set; }
        public virtual Director Director { get; set; }
        public virtual Distribuidor Distribuidor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedComentario> FeedComentarios { get; set; }
        public virtual Group Group { get; set; }
        public virtual Imagene Imagene { get; set; }
        public virtual JuegoEstudianteScore JuegoEstudianteScore { get; set; }
        public virtual ProductoEstudiante ProductoEstudiante { get; set; }
        public virtual QuickTest_Complete QuickTest_Complete { get; set; }
        public virtual School School { get; set; }
        public virtual Student Student { get; set; }
        public virtual Tasks_Complete Tasks_Complete { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual TeacherComputacion TeacherComputacion { get; set; }
        public virtual TestInteligencia_Complete TestInteligencia_Complete { get; set; }
        public virtual Tutor Tutor { get; set; }
        public virtual User_Logro User_Logro { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedStudent> FeedStudents { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedUser> FeedUsers { get; set; }
    }
}
