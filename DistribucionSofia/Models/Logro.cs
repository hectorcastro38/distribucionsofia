//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Logro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Logro()
        {
            this.AccionesGuardadas = new HashSet<AccionesGuardada>();
            this.User_Logro = new HashSet<User_Logro>();
            this.Logro1 = new HashSet<Logro>();
            this.Logroes = new HashSet<Logro>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public Nullable<int> Cantidad { get; set; }
        public Nullable<double> Cantidad2 { get; set; }
        public int CreditosExtra { get; set; }
        public Nullable<System.Guid> IdImagen { get; set; }
        public System.Guid IdTipoLogro { get; set; }
        public Nullable<int> IdNivelEscolar { get; set; }
        public Nullable<System.Guid> IdDiagnosticoArbolJefe { get; set; }
        public Nullable<System.Guid> IdDiagnosticoArbol { get; set; }
        public Nullable<System.Guid> IdDiagnosticoConcepto { get; set; }
        public bool IsActivo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccionesGuardada> AccionesGuardadas { get; set; }
        public virtual DiagnosticoArbol DiagnosticoArbol { get; set; }
        public virtual DiagnosticoArbolJefe DiagnosticoArbolJefe { get; set; }
        public virtual DiagnosticoConcepto DiagnosticoConcepto { get; set; }
        public virtual Imagene Imagene { get; set; }
        public virtual Logro_Tipo Logro_Tipo { get; set; }
        public virtual NivelEscolar NivelEscolar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User_Logro> User_Logro { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Logro> Logro1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Logro> Logroes { get; set; }
    }
}
