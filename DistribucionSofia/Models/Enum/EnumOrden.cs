﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.Enum
{
    public enum EnumOrden
    {
        Espera = 0,
        Aprobado = 1,
        Cancelado = 2,
    }
}