﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.Enum
{

    public enum EnumDependenciaEscuela
    {
        Federal = 2,
        Estatal = 1,
        Privada = 3,

    }
}