﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.Enum
{

    public enum EnumOrdenLista
    {
        Semana = 0,
        Tareas = 1,
        Ejercicios = 5
    }
}