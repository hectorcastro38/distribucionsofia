﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.Enum
{
    public enum EnumNivelEscolar
    {
        
        Primaria = 2,
        Secundaria = 3,
        Preparatoria = 4
        
    }
}