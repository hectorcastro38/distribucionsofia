﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models
{
    public enum EnumTipoUsuarioPreregistro
    {
        [Display(Name = "Alumno")]
        Alumno = 3,

        [Display(Name = "Profesor/Tutor")]
        Profesor = 2,

        [Display(Name = "Padre")]
        Tutor = 1,

        [Display(Name = "Director")]
        Director = 4,

        [Display(Name = "ATP")]
        ATP = 5,

        [Display(Name = "Supervisor")]
        Supervisor = 6,

        [Display(Name = "Jefe de Sector")]
        JefeSector = 7,

        [Display(Name = "XT-Admin")]
        Admin = 8,

        [Display(Name = "Coordinador")]
        Coordinador = 9,

        [Display(Name = "Profesor de computación")]
        Computacion = 10,

        [Display(Name = "Distribuidor")]
        Distribuidor = 11,

        [Display(Name = "XT-Capturador")]
        Capturador = 12,

        [Display(Name = "XT-Estadística")]
        Estadistica = 13,

        [Display(Name = "Registro")]
        Registro = 14,

        
    }
}