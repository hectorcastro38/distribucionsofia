﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DistribucionSofia.Models.Enum
{
    public enum EnumSexo
    {
        [Display(Name = "Hombre")]
        Hombre = 1,

        [Display(Name = "Mujer")]
        Mujer = 2,
        
    }
}