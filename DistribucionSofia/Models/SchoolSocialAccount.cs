//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SchoolSocialAccount
    {
        public System.Guid IdSchool { get; set; }
        public string InstaUserid { get; set; }
        public string InstaAccessToken { get; set; }
        public Nullable<System.DateTime> InstaUpdateDate { get; set; }
        public string InstaPageNameOrPageID { get; set; }
        public string FbUserid { get; set; }
        public string FbAccessToken { get; set; }
        public Nullable<System.DateTime> FbUpdateDate { get; set; }
        public string FbPageNameOrPageID { get; set; }
    
        public virtual School School { get; set; }
    }
}
