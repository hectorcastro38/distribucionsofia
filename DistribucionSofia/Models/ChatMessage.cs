//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChatMessage
    {
        public System.Guid Id { get; set; }
        public string Mensaje { get; set; }
        public System.DateTime FechaCreado { get; set; }
        public Nullable<System.DateTime> FechaLeido { get; set; }
        public System.Guid IdUserOrigen { get; set; }
        public System.Guid IdUserDestino { get; set; }
        public string UsernameOrigen { get; set; }
        public string UsernameDestino { get; set; }
    }
}
