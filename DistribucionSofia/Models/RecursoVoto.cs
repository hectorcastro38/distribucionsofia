//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RecursoVoto
    {
        public System.Guid Id { get; set; }
        public System.Guid IdRecurso { get; set; }
        public System.Guid IdUser { get; set; }
        public int Puntos { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual Recurso Recurso { get; set; }
    }
}
