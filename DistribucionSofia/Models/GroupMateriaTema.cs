//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GroupMateriaTema
    {
        public System.Guid Id { get; set; }
        public System.Guid IdGrupoMateria { get; set; }
        public System.Guid IdTema { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaTermino { get; set; }
        public bool IsVisto { get; set; }
        public Nullable<System.DateTime> FechaVisto { get; set; }
        public int Mes { get; set; }
    
        public virtual GroupMateria GroupMateria { get; set; }
        public virtual Tema Tema { get; set; }
    }
}
