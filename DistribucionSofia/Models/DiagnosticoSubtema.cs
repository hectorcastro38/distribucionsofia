//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DiagnosticoSubtema
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DiagnosticoSubtema()
        {
            this.DiagnosticoConceptoes = new HashSet<DiagnosticoConcepto>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public System.Guid IdTema { get; set; }
        public string Clave { get; set; }
        public string Orden { get; set; }
        public bool IsActivo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiagnosticoConcepto> DiagnosticoConceptoes { get; set; }
        public virtual DiagnosticoTema DiagnosticoTema { get; set; }
    }
}
