//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DistribucionSofia.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SofiaWizard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SofiaWizard()
        {
            this.SofiaWizard_Destino = new HashSet<SofiaWizard_Destino>();
            this.SofiaWizard_Pasos = new HashSet<SofiaWizard_Pasos>();
            this.SofiaWizard_Usuario = new HashSet<SofiaWizard_Usuario>();
        }
    
        public System.Guid IdWizard { get; set; }
        public string Nombre { get; set; }
        public bool IsActivo { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaFinal { get; set; }
        public int Paso { get; set; }
        public bool IsObligatorio { get; set; }
        public string Descripcion { get; set; }
        public int TipoDestino { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SofiaWizard_Destino> SofiaWizard_Destino { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SofiaWizard_Pasos> SofiaWizard_Pasos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SofiaWizard_Usuario> SofiaWizard_Usuario { get; set; }
    }
}
